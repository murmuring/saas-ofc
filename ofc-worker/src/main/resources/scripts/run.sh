#!/bin/bash

APP="ofc-worker"
PHOME=$(dirname `readlink -f "$0"`)
PHOME=$(dirname $PHOME)
CLASS_PATH=${PHOME}/conf:${PHOME}/lib/*
#:${wumart.api.classpath}

LOG_DIR=${PHOME}/../logs/worker

if [ ! -d "$LOG_DIR" ]; then
    mkdir -p "$LOG_DIR"
fi

APP=${APP}-$1

#JMX_PORT=`expr 8500`
JAVA_OPTS="-server -Xms1024m -Xmx1024m -Xmn384m -XX:MetaspaceSize=128m -XX:MaxMetaspaceSize=512m \
-Xss256k -XX:+UseConcMarkSweepGC \
-XX:+UseParNewGC \
-XX:+PrintGC -Xloggc:${LOG_DIR}/gc.log \
-Dlogs.dir=$LOG_DIR \
-Duser.timezone=GMT+08 \
-Dsnowflake.node=12"

pid=`ps -eo pid,args | grep ${APP} | grep java | grep -v grep | awk '{print $1}'`
echo "-----------------${APP} pid is ${pid}-----------------"

if [ -n "$pid" ]
then
    kill -3 ${pid}
    kill ${pid} && sleep 3
    if [  -n "`ps -eo pid | grep $pid`" ]
    then
        kill -9 ${pid}
    fi
    echo "-----------------kill pid: ${pid}-----------------"
fi

if [  -n "`ps -eo pid | grep $pid`" ]
then
    echo "-----------------kill failure!-----------------"
fi

echo "start ...."
java $JAVA_OPTS -cp $CLASS_PATH com.lsh.ofc.worker.bootstrap.WorkerMain ${APP}
