package com.lsh.ofc.worker.task;

import com.alibaba.fastjson.JSON;
import com.dangdang.ddframe.job.api.JobExecutionMultipleShardingContext;
import com.dangdang.ddframe.job.plugin.job.type.simple.AbstractSimpleElasticJob;
import com.lsh.base.common.exception.BusinessException;
import com.lsh.ofc.core.constant.Constants;
import com.lsh.ofc.core.redis.RedisTemplate;
import com.lsh.ofc.proxy.model.CreateWgSoRespHead;
import com.lsh.ofc.proxy.model.CreateWgStoReqHead;
import com.lsh.ofc.proxy.service.ScServiceProxy;
import com.lsh.ofc.proxy.service.WgServiceProxy;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Project Name: lsh-ofc
 * @author fuhao
 * Date: 18/6/19
 * Time: 18/6/19.
 * 北京链商电子商务有限公司
 * Package name:com.lsh.ofc.worker.task.
 * desc:定时任务，汇总sto
 */
@Slf4j
@Component
public class SendStoJob extends AbstractSimpleElasticJob {

    @Autowired
    private WgServiceProxy wgServiceProxy;

    @Autowired
    private RedisTemplate redisTemplate;

    @Autowired
    private ScServiceProxy scServiceProxy;

    @Override
    public void process(JobExecutionMultipleShardingContext jobContext) {
        try {
            log.info("ofc_task sto task create 开始...");
            long start = System.currentTimeMillis();
            int pushNum = 0;
            while (pushNum < 10) {
                CreateWgStoReqHead createWgStoReqHead = this.getStoHead();
                if (createWgStoReqHead == null) {
                    log.info("ofc_task sto task  is empty");
                    break;
                }
                this.dealSto(createWgStoReqHead);
                pushNum = pushNum +1;
            }
            long end = System.currentTimeMillis();
            log.info("ofc_task sto task create 完成...耗时：" + (end - start) + " ,处理sto数量:" + pushNum);

        } catch (BusinessException e) {
            log.error("ofc_task sto task create 异常... " + e.getMessage(), e);
        }
    }

    private void dealSto(CreateWgStoReqHead createWgStoReqHead) throws BusinessException{

        try{
            String summaryType = createWgStoReqHead.getSummaryType();

            CreateWgSoRespHead createSoResp;

            switch (summaryType) {
                case Constants.TYPE_SUMMARY_PSI_STO:
                    createSoResp = wgServiceProxy.createSto(createWgStoReqHead, createWgStoReqHead.getWgPath());
                    break;
                case Constants.TYPE_SUMMARY_SC_IBD:
                    scServiceProxy.sendIbd(createWgStoReqHead);
                default:
                    createSoResp = new CreateWgSoRespHead();
            }

            log.info("*****CreateWgStoRespHead createSoResp is " + JSON.toJSONString(createSoResp));

            //推送成功，给redis存入数据(时间)

            SimpleDateFormat spdf = new SimpleDateFormat("yyyy-MM-dd");

            String orderStoKey = MessageFormat.format(Constants.OFC_STO_PUST_STATUS, createWgStoReqHead.getSendWarehouseCode(), createWgStoReqHead.getReceiveWarehouseCode(),spdf.format(new Date()));
            //一天的时间有效期
            redisTemplate.set(orderStoKey, createSoResp.getSoId() ==null ? createWgStoReqHead.getOrderId() :  createSoResp.getSoId(), 60 * 60 * 24);
        }catch (Exception e){
            //推送失败,继续推送
            String orderKey = MessageFormat.format(Constants.OFC_STO_PUST_PSI_STATUS,createWgStoReqHead.getOrderId());

            log.error("deal error push to queue:"+orderKey);

            redisTemplate.sadd(Constants.OFC_STO_PUST_PSI_ORDERS,orderKey);


            log.error(e.getMessage(),e);
        }

    }

    /**
     * 查询需要推送的订单
     * @return             汇总sto指定大小
     */
    private CreateWgStoReqHead getStoHead() {
        String key = redisTemplate.spop(Constants.OFC_STO_PUST_PSI_ORDERS);
        if(!StringUtils.isBlank(key)){
                String value = redisTemplate.get(key);
                return JSON.parseObject(value, CreateWgStoReqHead.class);
        }
        return null;
    }

}
