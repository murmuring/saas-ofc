package com.lsh.ofc.worker.task;

import com.lsh.base.common.exception.BusinessException;
import com.lsh.ofc.core.entity.OfcTask;
import com.lsh.ofc.core.enums.OfcTaskStatus;
import com.lsh.ofc.core.enums.OfcTaskType;
import com.lsh.ofc.core.service.OfcRoCreateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Set;

/**
 * 创建RO任务
 *
 * @author huangdong
 * @date 16/9/5
 */
@Component
public class OfcRoCreateJob extends AbstractOfcTaskJob {

    @Autowired
    private OfcRoCreateService ofcRoCreateService;

    @Override
    protected boolean filterFlag(OfcTask task) {
        return false;
    }

    @Override
    protected OfcTaskType getFetchTaskType() {
        return OfcTaskType.RO_CREATE;
    }

    @Override
    protected int processTask(OfcTask task) throws BusinessException {

        return this.ofcRoCreateService.process(task.getRefId()) ? 1 : -1;
    }

    @Override
    protected List<OfcTask> fetchTasks(OfcTaskType type, Set<OfcTaskStatus> statuses, int shardingCount, List<Integer> shardingItems, int fetchSize) {
        return super.defaultFetchTasks(type, statuses, shardingCount, shardingItems, fetchSize);
    }
}
