package com.lsh.ofc.worker.task;

import com.lsh.base.common.exception.BusinessException;
import com.lsh.ofc.core.entity.OfcSoDetail;
import com.lsh.ofc.core.entity.OfcSoHead;
import com.lsh.ofc.core.entity.OfcTask;
import com.lsh.ofc.core.enums.OfcTaskStatus;
import com.lsh.ofc.core.enums.OfcTaskType;
import com.lsh.ofc.core.proxy.service.WumartBasicService;
import com.lsh.ofc.core.service.OfcSoService;
import com.lsh.ofc.proxy.context.WumartBasicContext;
import com.lsh.ofc.proxy.model.CreatePreWgPoReqDetail;
import com.lsh.ofc.proxy.model.CreatePreWgPoReqHead;
import com.lsh.ofc.proxy.service.WgServiceProxy;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * 创建SO任务
 *
 * @author peter
 * @date 19/3/29
 */
@Component
@Slf4j
public class OfcSo2PsiPoJob extends AbstractOfcTaskJob {

    @Autowired
    private OfcSoService ofcSoService;

    @Autowired
    private WgServiceProxy wgServiceProxy;

    @Autowired
    private WumartBasicService wumartBasicService;


    @Override
    protected OfcTaskType getFetchTaskType() {
        return OfcTaskType.SO_PO_PSI;
    }

    /**
     * 包含 DC10 汇总so的 obd 的po 过账
     *
     * @param task
     * @return
     * @throws BusinessException
     */
    @Override
    protected int processTask(OfcTask task) throws BusinessException {
        boolean ret;

        try {
            Long orderCode = task.getRefId();

            OfcSoHead filter = new OfcSoHead();
            filter.setOrderCode(orderCode);
            List<OfcSoHead> soHeadList = ofcSoService.findList(filter, true);
            if (CollectionUtils.isEmpty(soHeadList)) {
                log.info("SO单不存在，订单号：{}", orderCode);
                return 0;
            }

            for (OfcSoHead ofcSoHead : soHeadList) {

                String wgPath = this.wumartBasicService.getWmsPath(WumartBasicContext.buildContext(ofcSoHead.getRegionCode(), ofcSoHead.getSupplierCode(), ofcSoHead.getSupplierOrg(), ofcSoHead.getSupplierId(), ofcSoHead.getSupplierGroup()));
                String sourceDC = this.wumartBasicService.getSourceDC(WumartBasicContext.buildContext(ofcSoHead.getRegionCode(), ofcSoHead.getSupplierCode(), ofcSoHead.getSupplierOrg(), ofcSoHead.getSupplierId(), ofcSoHead.getSupplierGroup()));
                //调用po 过账接口 TODO
                Integer time = (int) (System.currentTimeMillis() / 1000);
                CreatePreWgPoReqHead poOrder = new CreatePreWgPoReqHead();
                poOrder.setFromWarehouseCode(ofcSoHead.getSupplierCode());
                poOrder.setToWarehouseCode(sourceDC);
                poOrder.setSoId(ofcSoHead.getSoBillCode());
                poOrder.setSummaryTime(time);
                poOrder.setMisVenderId(ofcSoHead.getVenderId());
                List<CreatePreWgPoReqDetail> details = new ArrayList<>();
                for (OfcSoDetail soDetail : ofcSoHead.getDetails()) {
                    CreatePreWgPoReqDetail reqDetail = new CreatePreWgPoReqDetail();
                    reqDetail.setGoodsId(soDetail.getSkuSupplyCode());
                    reqDetail.setGoodsName(soDetail.getGoodsName());
                    reqDetail.setLineNo(soDetail.getItemNo());
                    reqDetail.setQty(soDetail.getSkuOrderQty());

                    details.add(reqDetail);
                }

                poOrder.setDetails(details);
                Boolean flag = wgServiceProxy.sendPrePo(poOrder, wgPath);
                logger.info("物美汇总推送进销存 flag " + flag);
            }

            ret = true;
        } catch (BusinessException e) {
            logger.error("请求进销存，创建PO任务处理失败", e);
            ret = false;
        }

        return ret == true ? 1 : -1;
    }


    @Override
    protected boolean filterFlag(OfcTask task) {

        return false;
    }

    @Override
    protected List<OfcTask> fetchTasks(OfcTaskType type, Set<OfcTaskStatus> statuses, int shardingCount, List<Integer> shardingItems, int fetchSize) {
        return super.defaultFetchTasks(type, statuses, shardingCount, shardingItems, fetchSize);
    }
}
