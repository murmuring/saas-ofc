package com.lsh.ofc.worker.task;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.lsh.base.common.exception.BusinessException;
import com.lsh.ofc.core.constant.Constants;
import com.lsh.ofc.core.entity.OfcObdHead;
import com.lsh.ofc.core.entity.OfcTask;
import com.lsh.ofc.core.enums.OfcTaskStatus;
import com.lsh.ofc.core.enums.OfcTaskType;
import com.lsh.ofc.core.service.OfcObdService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Set;

/**
 * 创建SO任务
 *
 * @author huangdong
 * @date 16/9/5
 */
@Component
public class OfcObdScanBoxPushJob extends AbstractOfcTaskJob {

    @Autowired
    private OfcObdService ofcObdService;


    @Override
    protected OfcTaskType getFetchTaskType() {
        return OfcTaskType.SO_PUSH_DMG;
    }

    @Override
    protected int processTask(OfcTask task) throws BusinessException {

        boolean ret = false;
        try {
            Long soBillCode = task.getRefId();
            OfcObdHead filter = new OfcObdHead();
            filter.setSoBillCode(String.valueOf(soBillCode));
            OfcObdHead obdHead = ofcObdService.findOne(filter, false);

            if (null == obdHead) {
                return 0;
            }

            String ext = obdHead.getExt();

            if (StringUtils.isBlank(ext)) {
                return 0;
            }

            JSONObject extJson = JSON.parseObject(ext);
            String scanPackageCodes = extJson.getString(Constants.OBD_D_OBD_SCAN_PACKAGE_CODES);
            String collectionLocation = extJson.getString(Constants.OBD_COLLECTION_LOCATION);

            obdHead.getOrderCode();
            if(StringUtils.isBlank(collectionLocation)){
                collectionLocation = "";
            }
            if(StringUtils.isBlank(scanPackageCodes)){
                return 0;
            }


            // 推送商城proxy

        } catch (BusinessException e) {
            logger.error("任务处理失败", e);
            ret = true;
        }

        return ret == true ? 1 : 0;
    }




    @Override
    protected boolean filterFlag(OfcTask task) {

        return false;
    }

    @Override
    protected List<OfcTask> fetchTasks(OfcTaskType type, Set<OfcTaskStatus> statuses, int shardingCount, List<Integer> shardingItems, int fetchSize) {
        return super.defaultFetchTasks(type, statuses, shardingCount, shardingItems, fetchSize);
    }

}
