package com.lsh.ofc.worker.task;

import com.dangdang.ddframe.job.api.JobExecutionSingleShardingContext;
import com.lsh.ofc.core.base.SessionId;
import com.lsh.ofc.core.entity.OfcRoHead;
import com.lsh.ofc.core.enums.FulfillChannel;
import com.lsh.ofc.core.enums.RoStatus;
import com.lsh.ofc.core.mail.EmailHandler;
import com.lsh.ofc.core.service.OfcRoService;
import com.lsh.ofc.worker.base.AbstractSequenceDataFlowJob;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.*;
import java.util.concurrent.atomic.AtomicLong;

/**
 * 查询RO信息，刷新RO单状态
 *
 * @author panxudong
 * @date 16/11/30
 */
@Component
public class OfcRoQuery20Job extends AbstractSequenceDataFlowJob<OfcRoHead> {

    protected final Logger logger = Logger.getLogger(this.getClass());
    private final AtomicLong offset = new AtomicLong(0);
    @Autowired
    private OfcRoService ofcRoService;
    @Autowired
    private EmailHandler emailHandler;
    @Value("${ro.query.delay.time}")
    private Integer queryInterval;

    private Set<RoStatus> getFetchRoStatus() {
        return new HashSet<>(Arrays.asList(RoStatus.CREATED));
    }

    @Override
    public List<OfcRoHead> fetchTasks(JobExecutionSingleShardingContext context) {
        int size = 800;

        Set<RoStatus> statuses = this.getFetchRoStatus();
        String message = "20查询RO信息，刷新RO单状态 fetchData(" + statuses + ", " + queryInterval + ", " + offset + ", " + size + ")";
        logger.info(message + " start...");
        try {
            List<OfcRoHead> list = ofcRoService.fetchRoByStatusAndTimeStamp(statuses, queryInterval.intValue(), 10000, size);
            if(null == list || list.size() == 0){
                return Collections.EMPTY_LIST;
            }
            Iterator<OfcRoHead> roHeadIterator = list.iterator();
            while (roHeadIterator.hasNext()){
                OfcRoHead roHead = roHeadIterator.next();

                if(roHead.getSupplierDc().equals("DC10") && roHead.getFulfillChannel().equals(FulfillChannel.WUMART_OFC.getValue())){
                    continue;
                }

                roHeadIterator.remove();
            }

            logger.info(message + " ed... data_size=" + list.size());
            return list;
        } catch (Throwable e) {
            logger.error(message + " error... " + e.getMessage(), e);
            throw e;
        }
    }

    @Override
    public boolean processTask(JobExecutionSingleShardingContext context, OfcRoHead ofcRoHead) {
        if (ofcRoHead == null) {
            return true;
        }

        Long returnCode = ofcRoHead.getReturnCode();
        String billCode = ofcRoHead.getRoBillCode();
        String prefix = "20 返仓单【" + returnCode + "】【" + billCode + "】查询RO信息，刷新RO单状态";
        logger.info(prefix + "处理开始！");
        Integer roStatus = ofcRoHead.getRoStatus();
        if (!this.getFetchRoStatus().contains(RoStatus.valueOf(roStatus))) {
            logger.warn(prefix + " process ignore...");
            return true;
        }
        try {
            Integer ret = ofcRoService.refreshRoStatus(ofcRoHead);
            if (RoStatus.CREATED.getValue().equals(ret) || RoStatus.COMPLETED.getValue().equals(ret)) {
                logger.info(prefix + "20处理成功！");
            } else {
                logger.warn(prefix + "20未处理成功！");
                this.sendMail(returnCode, billCode, "查询RO信息，刷新RO单状态未处理成功！");
            }
            return true;
        } catch (Throwable e) {
            logger.error(prefix + "处理异常！" + e.getMessage(), e);
            this.sendMail(returnCode, billCode, "查询RO信息，刷新RO单状态处理异常！");
            return false;
        }
    }

    private void sendMail(Long returnCode, String billCode, String message) {
        StringBuilder builder = new StringBuilder(message);
        builder.append("20返仓单号：").append(returnCode).append("\n");
        builder.append("业务单号：").append(billCode).append("\n");
        builder.append("日志ID：").append(SessionId.get()).append("\n");
        builder.append("返仓履约结果超过" + this.queryInterval / 60 + "分钟未回传！").append("\n");
        this.emailHandler.buildEmail(EmailHandler.EmailTopic.RO_QUERY, builder.toString());
    }
}