package com.lsh.ofc.worker.task;

import com.dangdang.ddframe.job.api.JobExecutionMultipleShardingContext;
import com.lsh.base.common.exception.BusinessException;
import com.lsh.ofc.core.enums.OfcTaskStatus;
import com.lsh.ofc.core.service.OfcTaskService;
import com.lsh.ofc.worker.base.AbstractSimpleJob;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * OFC任务历史Job
 *
 * @author huangdong
 * @date 16/9/20
 */
@Component
public class OfcPmsSoMergeJob extends AbstractSimpleJob {

    @Autowired
    private OfcTaskService ofcTaskService;

    @Override
    public void execute(JobExecutionMultipleShardingContext context) {
        try {
            logger.info("ofc_task pms so merge 开始...");
            long start = System.currentTimeMillis();
            int cnt = this.ofcTaskService.moveHistory(OfcTaskStatus.SUCEESS);
            long end = System.currentTimeMillis();
            logger.info("ofc_task pms so merge 完成... 处理行数：" + cnt + "，耗时：" + (end - start));
        } catch (BusinessException e) {
            logger.error("ofc_task pms so merge 异常... " + e.getMessage(), e);
        }
    }
}
