package com.lsh.ofc.worker.kafka;

import com.dmall.dmg.sdk.core.ConnectionFactory;
import com.dmall.dmg.sdk.core.consumer.MessageHandler;
import com.dmall.dmg.sdk.core.consumer.kafka.KafkaConsumerContainer;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Properties;

/**
 *
 * @author peter
 * @date 19/3/27
 */
@Configuration
@Slf4j
public class WumartConsumerConfig {

    @Value("${clusterId}")
    private Long clusterId;

    @Value("${appKey}")
    private String appKey;

    @Value("${secretKey}")
    private String secretKey;

    @Value("${authUrl}")
    private String authUrl;

    @Value("${group.id}")
    private String groupId;

    @Value("${num.consumers}")
    private String consumersNum;

    @Value("${topic.so.obd}")
    private String topicName;

    @Value("${auto.offset.reset}")
    private String autoOffsetReset;

    @Value("${zookeeper.connection.timeout.ms}")
    private String connTimeout;

    @Value("${zookeeper.session.timeout.ms}")
    private String sessionTimeout;

    @Value("${auto.commit.enable}")
    private String commitEnable;

    @Value("${rebalance.max.retries}")
    private String retries;

    @Value("${rebalance.backoff.ms}")
    private String backoff;

    @Value("${fetch.message.max.bytes}")
    private String maxBytes;

    @Value("${kafka.switch}")
    private int kafkaSwitch;

    @Autowired
    private ConnectionFactory connectionFactory;

    @Autowired
    private MessageHandler messageHandler;

    @Bean
    public ConnectionFactory getDmgConnectionFactory(){
        log.info("ofc 开始初始化 DmgConnectionFactory bean");
        ConnectionFactory connectionFactory = new ConnectionFactory();
        connectionFactory.setClusterId(clusterId);
        connectionFactory.setAppKey(appKey);
        connectionFactory.setSecretKey(secretKey);
        connectionFactory.setAuthUrl(authUrl);
        return connectionFactory;
    }

    @Bean
    public KafkaConsumerContainer getKafkaConsumerContainer(){

        if(kafkaSwitch == 0){
            return null;
        }
        log.info("ofc 开始初始化 KafkaConsumerContaine bean");

        KafkaConsumerContainer kafkaConsumerContainer = new KafkaConsumerContainer();
        kafkaConsumerContainer.setConnectionFactory(connectionFactory);
        kafkaConsumerContainer.setHandler(messageHandler);

        Properties properties = new Properties();
        properties.put("group.id",groupId);
        properties.put("topic.name",topicName);
        properties.put("num.consumers",consumersNum);
        properties.put("auto.offset.reset",autoOffsetReset);
        properties.put("zookeeper.connection.timeout.ms",connTimeout);
        properties.put("zookeeper.session.timeout.ms",sessionTimeout);
        properties.put("auto.commit.enable",commitEnable);
        properties.put("rebalance.max.retries",retries);
        properties.put("rebalance.backoff.ms",backoff);
        properties.put("fetch.message.max.bytes",maxBytes);
        kafkaConsumerContainer.setProperties(properties);

        return kafkaConsumerContainer;
    }
}
