#!/bin/bash
mvn package -Dmaven.test.skip=true -P test
scp ofc-worker/target/ofc-worker-1.0-SNAPSHOT-test.tar.gz work@192.168.60.59:~/fuhao/lsh-ofc/
scp ofc-provider/target/ofc-provider-1.0-SNAPSHOT-test.tar.gz work@192.168.60.59:~/fuhao/lsh-ofc/
REMOTE="work@192.168.60.59"
REMOTE_PATH="/home/work/fuhao/lsh-ofc"
ssh $REMOTE "sh $REMOTE_PATH/deploy_51.sh"