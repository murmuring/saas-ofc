package com.lsh.ofc.core.enums;

/**
 * Project Name: lsh-ofc
 * @author peter
 * Date: 2019-03-25
 * Time: 2019-03-25.
 * 北京链商电子商务有限公司
 * Package name:com.lsh.ofc.core.enums.
 * desc:类功能描述
 */
public enum CusType {
    // 1、链商北京151001 2、链商天津151002  3、链商餐馆151003 4、文固151004 5、自建
    BEIJING_WM(1,"链商北京151001"),
    TIANJING_WM(2,"链商天津151002"),
    CATERING_WM(3,"链商餐馆151003"),
    WG_WM(4,"文固151004"),
    LSH_NEW(5,"链商自建");

    private final Integer value;

    private final String desc;

    CusType(Integer value, String desc) {
        this.value = value;
        this.desc = desc;
    }

    public static CusType valueOf(Integer value) {
        if (value == null) {
            return null;
        }
        for (CusType item : CusType.values()) {
            if (item.getValue().equals(value)) {
                return item;
            }
        }
        return null;
    }

    public Integer getValue() {
        return value;
    }

    public String getDesc() {
        return desc;
    }}
