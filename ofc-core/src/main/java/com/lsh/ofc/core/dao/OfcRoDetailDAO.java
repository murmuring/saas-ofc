package com.lsh.ofc.core.dao;

import com.lsh.ofc.core.base.BaseDAO;
import com.lsh.ofc.core.entity.OfcRoDetail;
import org.springframework.stereotype.Repository;

/**
 * OFC RO明细DAO
 *
 * @author huangdong
 * @date 16/9/9
 */
@Repository
public interface OfcRoDetailDAO extends BaseDAO<OfcRoDetail, Long> {
}
