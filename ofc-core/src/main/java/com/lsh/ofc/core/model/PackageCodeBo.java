package com.lsh.ofc.core.model;

import lombok.Getter;
import lombok.Setter;
import org.springframework.util.Assert;

import java.math.BigDecimal;

/**
 * Project Name: PackageCode
 * 北京链商电子商务有限公司
 * Auth: wangliutao
 * Date: 19/5/7
 * Package Name: com.lsh.ofc.core.model
 * Description:
 */
@Getter
@Setter
public class PackageCodeBo {
    /**
     * 包裹数
     */
    private BigDecimal packageNum;

    /**
     * SO单据号
     */
    private String soBillCode;

    /**
     * 包裹编号
     */
    private String packageNo;

    /**
     * 供商
     */
    private Integer supplyId;

    /**
     * 链商商品码
     */
    private String skuCode;

    /**
     * 分拣中心，作业模式
     * 1、包裹；2、提总
     */
    private Integer taskModel;

    /**
     * 箱规
     */
    private BigDecimal saleUnit;

    public PackageCodeBo(BigDecimal packageNum, String soBillCode, String packageNo, Integer supplyId, String skuCode, BigDecimal saleUnit, Integer taskModel) {
        Assert.notNull(taskModel, "taskModel 不能为null");
        this.packageNum = packageNum;
        this.soBillCode = soBillCode;
        this.packageNo = packageNo;
        this.supplyId = supplyId;
        this.skuCode = skuCode;
        this.taskModel = taskModel;
        this.saleUnit = saleUnit;
    }

    /**
     * 拆零发货
     *
     * @param soBillCode
     * @param packageCode
     * @return
     */
    public static PackageCodeBo normalTask(String soBillCode, String packageCode) {
        return new PackageCodeBo(null, soBillCode, packageCode, null, null, null, 1);
    }

    /**
     * 按箱发货
     *
     * @param supplierId
     * @param packageCode
     * @return
     */
    public static PackageCodeBo aggregateTask(Integer supplierId, String packageCode, BigDecimal saleUnit) {
        return new PackageCodeBo(null, null, null, supplierId, packageCode, saleUnit, 2);
    }
}
