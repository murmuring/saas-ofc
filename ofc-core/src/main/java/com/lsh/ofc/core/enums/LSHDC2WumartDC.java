package com.lsh.ofc.core.enums;


/**
 * 履约WMS枚举
 *
 * @author huangdong
 * @date 16/11/11
 */
public enum LSHDC2WumartDC {

    /**
     * 0-无
     */
//    DC43_DC10("DC43", "DC10", "DC43直接履约物美DC10仓库"),
    SC01_DC10("SC01", "DC10", "SC01直接履约物美DC10仓库");

    private final String lshDC;

    private final String wuMartDC;

    private final String desc;

    LSHDC2WumartDC(String lshDC, String wuMartDC, String desc) {
        this.lshDC = lshDC;
        this.wuMartDC = wuMartDC;
        this.desc = desc;
    }

    public String getLshDC() {
        return lshDC;
    }

    public String getWuMartDC() {
        return wuMartDC;
    }

    public String getDesc() {
        return desc;
    }


    public static LSHDC2WumartDC getEnumDC(String lshDC) {
        if (lshDC == null) {
            return null;
        }
        for (LSHDC2WumartDC item : LSHDC2WumartDC.values()) {
            if (item.getLshDC().equals(lshDC)) {
                return item;
            }
        }

        return null;
    }
}
