package com.lsh.ofc.core.service.impl;

import com.google.common.collect.Lists;
import com.lsh.base.common.exception.BusinessException;
import com.lsh.ofc.core.dao.OfcTaskDAO;
import com.lsh.ofc.core.entity.OfcTask;
import com.lsh.ofc.core.enums.OfcTaskStatus;
import com.lsh.ofc.core.enums.OfcTaskType;
import com.lsh.ofc.core.exception.EC;
import com.lsh.ofc.core.service.OfcTaskService;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

/**
 * @author huangdong
 * @date 16/8/28
 */
@Service
@Slf4j
public class OfcTaskServiceImpl implements OfcTaskService {

    private final static Logger logger = LoggerFactory.getLogger(OfcTaskServiceImpl.class);

    @Autowired
    private OfcTaskDAO ofcTaskDAO;

    @Override
    @Transactional(rollbackFor = Exception.class)
    public int addTask(OfcTask task) {
        return ofcTaskDAO.insert(task);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public boolean addTasks(List<OfcTask> taskList) {
        log.info("插入 80 ofc任务 开始");
        int batchFlag = ofcTaskDAO.insertBatch(taskList);
        log.info("插入 80 ofc任务 结束 batchFlag is {}", batchFlag);

        return batchFlag > 0;
    }

    @Override
    public int countTask(OfcTask task) {
        return ofcTaskDAO.count(task);
    }

    @Override
    public List<OfcTask> fetchTasks(OfcTaskType type, Set<OfcTaskStatus> statusSet, int shardingCount, List<Integer> shardingItems, int fetchSize) {
        return this.fetchTasks(type, statusSet, shardingCount, shardingItems, fetchSize, null);
    }

    @Override
    public List<OfcTask> fetchTasks(OfcTaskType type, Set<OfcTaskStatus> statusSet, int shardingCount, List<Integer> shardingItems, int fetchSize, int delayTime) {
        return this.fetchTasks(type, statusSet, shardingCount, shardingItems, fetchSize, Integer.valueOf(delayTime));
    }

    @Override
    public List<OfcTask> fetchTasks(OfcTaskType type, Set<OfcTaskStatus> statusSet, int fetchSize, Integer delayTime) {
        if (CollectionUtils.isEmpty(statusSet)) {
            return Collections.emptyList();
        }

        List<Integer> statuses = new ArrayList<>(statusSet.size());
        for (OfcTaskStatus status : statusSet) {
            statuses.add(status.getValue());
        }
        // 该获取任务的方法，不关心任务的执行次数，只要满足条件，大于10的任务也会被获取到
        return ofcTaskDAO.fetchTasksWithoutExecCount(type.getValue(), statuses, fetchSize, delayTime);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public int updateTask(OfcTask task, boolean success) {
        OfcTask filter = new OfcTask();
        filter.setId(task.getId());
        filter.setStatus(task.getStatus());

        OfcTask expect = new OfcTask();
        int expectStatus = (success ? OfcTaskStatus.SUCEESS : OfcTaskStatus.ERROR).getValue();
        expect.setStatus(expectStatus);
        expect.setExecCount(task.getExecCount() + 1);
        expect.setUpdateTime((int) TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis()));

        return this.ofcTaskDAO.updateByFilter(expect, filter);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public int updateProcessedTask(Long id, OfcTaskStatus preStatus, boolean success) {
        return this.ofcTaskDAO.updateProcessedTask(id, preStatus.getValue(), (success ? OfcTaskStatus.SUCEESS : OfcTaskStatus.ERROR).getValue());
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public int moveHistory(OfcTaskStatus status) throws BusinessException {
        int cnt1 = this.ofcTaskDAO.insert4History(status.getValue());
        int cnt2 = this.ofcTaskDAO.delete4History(status.getValue());
        if (cnt1 != cnt2) {
            throw EC.ERROR.exception("插入数[" + cnt1 + "]与删除数[" + cnt2 + "]不相等!");
        }
        logger.info("ofc_task移动到历史表数量：" + cnt1);
        return cnt1;
    }

    @Override
    public List<OfcTask> fetchErrorTasks(OfcTaskType type, int startTime) {
        return this.ofcTaskDAO.fetchErrorTasks(type.getValue(), startTime);
    }

    @Override
    public int disablePendingTask(Long refId, OfcTaskType type) {
        return ofcTaskDAO.disablePendingTask(refId, type.getValue(), Lists.newArrayList(0, 3));
    }

    private List<OfcTask> fetchTasks(OfcTaskType type, Set<OfcTaskStatus> statusSet, int shardingCount, List<Integer> shardingItems, int fetchSize, Integer delayTime) {
        if (shardingCount <= 0 || fetchSize <= 0 || CollectionUtils.isEmpty(shardingItems)) {
            logger.info("shardingCount: {}, fetchSize: {}, shardingItems: {}", new Object[]{shardingCount, fetchSize, shardingItems});
            return Collections.emptyList();
        }
        List<Integer> statuses = new ArrayList<>(statusSet.size());
        for (OfcTaskStatus status : statusSet) {
            statuses.add(status.getValue());
        }
        List<OfcTask> list = this.ofcTaskDAO.fetchTasks(type.getValue(), statuses, shardingCount, shardingItems, fetchSize, delayTime);
        return list;
    }

    @Override
    public List<OfcTask> selectByRefIdAndType(Long refId, Integer type) {
        return this.ofcTaskDAO.selectByRefIdAndType(refId, type);
    }
}
