package com.lsh.ofc.core.handler;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.lsh.base.common.exception.BusinessException;
import com.lsh.ofc.core.constant.Constants;
import com.lsh.ofc.core.entity.OfcCustomer;
import com.lsh.ofc.core.entity.OfcRoHead;
import com.lsh.ofc.core.enums.RoStatus;
import com.lsh.ofc.core.util.OFCUtils;
import org.springframework.context.ApplicationContext;

/**
 * 【链商云仓】创建返仓处理Handler
 *
 * @author miaozhuang
 * @date 18/8/9
 */
public class CreateRoByLshCloudHandler extends CreateRoHandler {

    protected CreateRoByLshCloudHandler(final ApplicationContext context, final OfcRoHead ro, final OfcCustomer customer) {

        super(context, ro, customer);
    }

    @Override
    protected boolean process(final OfcRoHead ro, final OfcCustomer customer) throws BusinessException {

        OfcRoHead filter = new OfcRoHead();
        filter.setId(ro.getId());
        filter.setRoBillCode(ro.getRoBillCode());
        filter.setRoStatus(RoStatus.UNCREATED.getValue());

        JSONObject ext = JSON.parseObject(ro.getExt());
//        ext.put(Constants.RO_H_REF_RO_CODE, createSoResp.getRsoId());
        ext.put(Constants.RO_H_FULFILL_CREATE_TIME, OFCUtils.currentTime());
        OfcRoHead update = new OfcRoHead();
//        update.setRoCode(createSoResp.getRsoId());
        update.setRoStatus(RoStatus.CREATED.getValue());
        update.setExt(ext.toJSONString());
        int r = this.ofcRoService.updateStatus(filter, update);
        logger.info("更新RO状态，单据号=" + ro.getRoBillCode() + "。。。" + RoStatus.CREATED + " " + r);

        return true;
    }
}
