package com.lsh.ofc.core.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

/**
 * OFC订单头
 *
 * @author huangdong
 * @date 16/9/12
 */
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class OfcOrderHead extends Vender implements Serializable {

    private static final long serialVersionUID = 3999125016843792649L;
    /**
     * ID
     */
    private Long id;

    /**
     * 订单号
     */
    private Long orderCode;

    /**
     * 地域编号
     */
    private Integer regionCode;

    /**
     * 地址编号
     */
    private Long addressCode;

    /**
     * 地址信息
     */
    private String addressInfo;

    /**
     * 下单金额
     */
    private BigDecimal orderAmount;

    /**
     * 下单时间
     */
    private Integer orderTime;

    /**
     * 仓库编号
     */
    private String warehouseCode;

    /**
     * 仓库名称
     */
    private String warehouseName;

    /**
     * SKU下单总数
     */
    private BigDecimal totalSkuOrderQty;

    /**
     * SKU供货总数
     */
    private BigDecimal totalSkuSupplyQty;

    /**
     * SKU发货总数
     */
    private BigDecimal totalSkuDeliverQty;

    /**
     * SKU退货总数
     */
    private BigDecimal totalSkuReturnQty;

    /**
     * 履约状态
     */
    private Integer fulfillStatus;

    /**
     * 其它信息
     */
    private String ext;

    /**
     * 创建时间
     */
    private Integer createTime;

    /**
     * 更新时间
     */
    private Integer updateTime;

    /**
     * 是否有效（0:无效；1:有效）
     */
    private Integer valid;

    /**
     * 二次配送标记
     */
    private Integer secondDistributionFlag;
    /**
     * 前置仓code
     */
    private String preWarehouseCode;
    /**
     * 配送方式
     */
    private Integer distributionWay;
    /**
     * 配送类型
     */
    private String orderDistributionType;

    /**
     * 订单明细列表
     */
    private List<OfcOrderDetail> details;

}
