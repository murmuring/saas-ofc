package com.lsh.ofc.core.enums;

import com.lsh.ofc.core.handler.*;

/**
 * 订单履约渠道
 *
 * @author huangdong
 * @date 16/9/10
 */
public enum FulfillChannel {
    /**
     *
     */
    LSH_WMS(1, "链商WMS", CreateSoByLshWMSHandler.class, CreateRoByLshWMSHandler.class),

    WUMART_OFC(2, "物美OFC", CreateSoByWumartOFCHandler.class, CreateRoByWumartOFCHandler.class),

    WUMART_SAP(3, "物美SAP", CreateSoByWumartSAPHandler.class, CreateRoByWumartSAPHandler.class),

    SAAS_PSI(4, "进销存", CreateSoByWGHandler.class, CreateRoByWgHandler.class),

    SAAS_OFC(5, "OFC", CreateSoByOfcHandler.class, CreateRoByWgHandler.class),

    SAAS_SC(6, "分拣中心", CreateSoByLshCloudSCHandler.class, CreateRoByWgHandler.class);

    private final Integer value;

    private final String desc;

    private Class<? extends CreateSoHandler> createSoHandlerType;

    private Class<? extends CreateRoHandler> createRoHandlerType;

    FulfillChannel(Integer value, String desc, Class<? extends CreateSoHandler> createSoHandlerType, Class<? extends CreateRoHandler> createRoHandlerType) {
        this.value = value;
        this.desc = desc;
        this.createSoHandlerType = createSoHandlerType;
        this.createRoHandlerType = createRoHandlerType;
    }

    public Integer getValue() {
        return value;
    }

    public String getDesc() {
        return desc;
    }

    public Class<? extends CreateSoHandler> getCreateSoHandlerType() {
        return createSoHandlerType;
    }

    public Class<? extends CreateRoHandler> getCreateRoHandlerType() {
        return createRoHandlerType;
    }

    public static FulfillChannel valueOf(Integer value) {
        for (FulfillChannel item : FulfillChannel.values()) {
            if (item.getValue().equals(value)) {
                return item;
            }
        }
        return null;
    }

    public static final boolean isWumartOfc(FulfillChannel fulfillChannel) {
        return fulfillChannel == FulfillChannel.WUMART_OFC;
    }
}
