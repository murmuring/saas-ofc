package com.lsh.ofc.core.service.impl;

import com.alibaba.fastjson.JSON;
import com.lsh.ofc.core.dao.OfcOperateLogDAO;
import com.lsh.ofc.core.entity.OfcOperateLog;
import com.lsh.ofc.core.enums.BillType;
import com.lsh.ofc.core.enums.OfcOperateEnum;
import com.lsh.ofc.core.service.OfcOperateLogService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author peter
 */
@Service
@Slf4j
public class OfcOperateLogServiceImpl implements OfcOperateLogService {

    @Autowired
    private OfcOperateLogDAO ofcOperateLogDAO;

    @Override
    public void insert(String billCode, BillType billType, OfcOperateEnum operate, String remark,Long venderId) {

        OfcOperateLog log4insert = new OfcOperateLog();
        log4insert.setBillCode(billCode);
        log4insert.setBillType(billType.name());
        log4insert.setOperateType(operate.getType());
        log4insert.setOperateDesc(operate.getDesc());
        if(venderId == null){
            venderId = 0L;
        }
        log4insert.setVenderId(venderId);

        try {
            this.ofcOperateLogDAO.insert(log4insert);
        } catch (Exception e) {
            log.error(billCode + "插入日志异常 {}",e);
            log.error("{} 插入日志异常 {}",billCode, JSON.toJSONString(log4insert));
        }
    }

    @Override
    public List<OfcOperateLog> fetchLogs(BillType billType, long offsetId, int fetchSize) {
        return this.ofcOperateLogDAO.fetchLogs(billType.name(), offsetId, fetchSize);
    }
}
