package com.lsh.ofc.core.service;

import com.alibaba.fastjson.JSONObject;
import com.lsh.ofc.core.entity.OfcObdHead;
import com.lsh.ofc.core.entity.OfcOrderHead;
import com.lsh.ofc.core.entity.OfcSoHead;
import com.lsh.ofc.core.model.DmgSoObd;
import com.lsh.ofc.core.model.DmgSoSo;

import java.util.List;
import java.util.Map;

/**
 * @author peter
 */
public interface OrderSoObdService {

    /**
     *
     * @param targetOfcSoHead
     * @param fromOrderHead
     * @return
     */
     OfcOrderHead ofcOrderHeadBuilder(OfcSoHead targetOfcSoHead, OfcOrderHead fromOrderHead);

    /**
     *
     * @param soSoMap
     * @return
     */
    OfcSoHead ofcSoHeadBuilder(Map<String, List<DmgSoSo>> soSoMap,Map<Integer,JSONObject> redisSoDetailMap);

    /**
     *
     * @param ofcSoHead
     * @param soObdList
     * @return
     */
     OfcObdHead ofcObdHeadBuilder(OfcSoHead ofcSoHead, List<DmgSoObd> soObdList);


}
