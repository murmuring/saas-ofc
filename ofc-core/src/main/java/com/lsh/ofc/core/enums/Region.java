package com.lsh.ofc.core.enums;

/**
 * 地域枚举
 *
 * @author huangdong
 * @date 16/10/1
 */
public enum Region {
    /**
     * 区域标识
     */
    BEIJING(1000, "北京"),
    TIANJIN(1001, "天津"),
    HANGZHOU(1002, "杭州"),
    BEIJING_CG(2000, "北京餐馆"),
    BEIJING_KA(3000, "北京KA用户"),
    BEIJING_BIG_KA(5000, "北京餐馆大KA"),
    BEIJING_NG(8000, "北京宁谷物配");

    private final Integer code;

    private final String desc;

    Region(Integer code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public Integer getCode() {
        return code;
    }

    public String getDesc() {
        return desc;
    }
}
