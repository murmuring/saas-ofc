package com.lsh.ofc.core.dao;

import com.lsh.ofc.core.base.BaseDAO;
import com.lsh.ofc.core.entity.OfcReturnHead;
import org.springframework.stereotype.Repository;

/**
 * OFC返仓单头DAO
 *
 * @author huangdong
 * @date 16/9/9
 */
@Repository
public interface OfcReturnHeadDAO extends BaseDAO<OfcReturnHead, Long> {
}
