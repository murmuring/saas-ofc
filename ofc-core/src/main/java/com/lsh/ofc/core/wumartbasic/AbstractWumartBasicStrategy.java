package com.lsh.ofc.core.wumartbasic;

import com.alibaba.fastjson.JSON;
import com.lsh.ofc.core.redis.RedisTemplate;
import com.lsh.ofc.proxy.context.WumartBasicContext;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import java.util.Map;

/**
 * Project Name: lsh-ofc
 * Author: panxudong
 * 北京链商电子商务有限公司
 * Desc: 类功能描述
 * Package Name: com.lsh.ofc.proxy.handler
 * Time: 2017-07-27 下午3:21
 * @author peter
 */
@Slf4j
public abstract class AbstractWumartBasicStrategy implements IWumartBasicStrategy {

    @Autowired
    private RedisTemplate redisTemplate;

    protected final boolean basicValidate(WumartBasicContext context) {
        log.info("WumartBasicStrategy context basicValidate context " + JSON.toJSONString(context));
        if (!WumartBasicContext.isContain(context)) {
            return false;
        }
        if (context.getProperty(WumartBasicContext.REGION) != null) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public Map<String, String> getConfig(WumartBasicContext context) {
        log.info("WumartBasicStrategy context " + context.logInfo());
        String configKey = this.getConfigKey(context);
        log.info("WumartBasicStrategy context x context rediskey " + configKey);
        Map<String, String> configs = redisTemplate.hgetAll(configKey);
        log.info("WumartBasicStrategy context redis configs is " + JSON.toJSONString(configs));
        return configs;
    }
}
