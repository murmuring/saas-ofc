package com.lsh.ofc.core.entity;

import com.alibaba.fastjson.JSON;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Map;

/**
 * OFC SO单据明细
 *
 * @author huangdong
 * @date 16/9/9
 */
@Setter
@Getter
@ToString
@NoArgsConstructor
public class OfcSoDetail extends Vender implements Serializable {

    private static final long serialVersionUID = -8322614418611831457L;
    /**
     * ID
     */
    private Long id;

    /**
     * SO单据号
     */
    private String soBillCode;

    /**
     * 项目号
     */
    private Integer itemNo;

    /**
     * 商品编号
     */
    private Integer itemCode;

    /**
     * 商品编号
     */
    private Long goodsCode;

    /**
     * 商品名称
     */
    private String goodsName;

    /**
     * 商品售卖单位
     */
    private BigDecimal goodsSaleUnit;

    /**
     * 商品单价
     */
    private BigDecimal goodsPrice;

    /**
     * 商品金额
     */
    private BigDecimal goodsAmount;

    /**
     * SKU编号
     */
    private Long skuCode;

    /**
     * SKU供货码
     */
    private String skuSupplyCode;

    /**
     * SKU供货价
     */
    private BigDecimal skuSupplyPrice;

    /**
     * SKU下单数
     */
    private BigDecimal skuOrderQty;

    /**
     * SKU供货数
     */
    private BigDecimal skuSupplyQty;

    /**
     * SKU退货数
     */
    private BigDecimal skuReturnQty;

    /**
     * 税率
     */
    private BigDecimal taxRate;

    /**
     * 其它信息
     */
    private String ext;

    /**
     * 创建时间
     */
    private Integer createTime;

    /**
     * 更新时间
     */
    private Integer updateTime;

    public Map<String, Object> extendedProperties() {
        return JSON.parseObject(this.ext);
    }

}
