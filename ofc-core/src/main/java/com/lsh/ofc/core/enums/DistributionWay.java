package com.lsh.ofc.core.enums;

/**
 * Project Name: lsh-ofc
 * Created by fuhao
 * Date: 18/6/19
 * 北京链商电子商务有限公司
 * Package name:com.lsh.ofc.core.enums.
 * desc:类功能描述
 * @author peter
 */
public enum DistributionWay {
    /**
     * 订单到用户
     */
    ORDER_2_USER(1, "订单到用户"),
    /**
     * 订单到店
     */
    ORDER_2_SHOP(2, "订单到店"),
    /**
     * 提种到店
     */
    SEED_2_SHOP(3, "提种到店"),
    /**
     * 多点前置仓配送
     */
    SEED_2_DMALL(4, "多点前置仓配送");

    private final Integer value;

    private final String desc;

    DistributionWay(Integer value, String desc) {
        this.value = value;
        this.desc = desc;
    }

    public Integer getValue() {
        return value;
    }

    public String getDesc() {
        return desc;
    }

    @Override
    public String toString() {
        return String.valueOf(this.getValue());
    }

    public static DistributionWay valueOf(Integer value) {
        if (value == null) {
            return null;
        }
        for (DistributionWay item : DistributionWay.values()) {
            if (item.getValue().equals(value)) {
                return item;
            }
        }
        return null;
    }

    public static Boolean isPms(Integer distributionWay){
        if(distributionWay.equals(DistributionWay.SEED_2_SHOP.getValue())
                || distributionWay.equals(DistributionWay.SEED_2_DMALL.getValue())){
            return true;
        }else{
            return false;
        }
    }
}
