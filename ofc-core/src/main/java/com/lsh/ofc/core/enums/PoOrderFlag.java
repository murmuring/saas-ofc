package com.lsh.ofc.core.enums;

/**
 * Project Name: lsh-oms
 *
 * @author peter
 * @date 19/2/19
 * 北京链商电子商务有限公司
 * Package
 * desc:
 */
public enum PoOrderFlag {
    /**
     * 预下单
     */
    PO_NO_ORDER("0", "非PO下单"),
    /**
     * 预下单
     */
    PRE_ORDER("1", "预下单"),
    /**
     * 确认下单
     */
    CONFIRM_ORDER("2", "确认下单");

    private String type;
    private String desc;

    PoOrderFlag(String type, String desc) {

        this.type = type;
        this.desc = desc;
    }

    public String getType(){
        return type;
    }

    private String getDesc(){
        return desc;
    }
}
