package com.lsh.ofc.core.proxy.service;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.lsh.base.common.exception.BusinessException;
import com.lsh.base.common.model.CommonResult;
import com.lsh.ofc.core.entity.OfcSupplier;
import com.lsh.ofc.core.enums.Switch;
import com.lsh.ofc.core.service.SupplierConfigService;
import com.lsh.ofc.core.wumartbasic.WumartBasicCustomerStrategy;
import com.lsh.ofc.proxy.context.SupplierDto;
import com.lsh.ofc.proxy.context.WumartBasicContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

/**
 * 物美基础服务
 *
 * @author huangdong
 * @date 16/8/28
 */
@Service
public class WumartBasicService {

    @Autowired
    private WumartBasicCustomerStrategy wumartBasicCustomerStrategy;

    @Autowired
    private SupplierConfigService supplierConfigService;


    public OfcSupplier getOfcSupplier(String code, Integer regionCode, Integer supplyId, Integer supplyOrg, String supplierGroup) {

        return supplierConfigService.findSupplier(supplyId, code, supplierGroup, supplyOrg, regionCode);
    }

    public JSONObject getOfcSupplierConfig(SupplierDto supplierDto) {
        OfcSupplier supplier = this.getOfcSupplier(supplierDto.getSupplierCode(), supplierDto.getRegionCode(),
                supplierDto.getSupplierId(), supplierDto.getSupplierOrg(), supplierDto.getSupplierGroup());
        if(null == supplier){
            return null;
        }

        JSONObject config = JSON.parseObject(supplier.getConfig());

        if(null == config){
            return null;
        }

        return config;
    }

    public String tansRegionWumartUsr(WumartBasicContext context) throws BusinessException {
        JSONObject config = this.getOfcSupplierConfig(context.getSupplierDto());
        if(null == config){
            throw new BusinessException(CommonResult.ERROR, "wumart usr not found... context=" + context.logInfo());
        }
        String ret = config.getString(WumartBasicContext.USR);

        if (!StringUtils.hasLength(ret)) {
            throw new BusinessException(CommonResult.ERROR, "wumart usr not found... context=" + context.logInfo());
        }
        return ret;
    }

    public String tansRegionWumartUsr4Meipi(WumartBasicContext context) throws BusinessException {
        context.setProperty(WumartBasicContext.PARAM, WumartBasicContext.USR);
        String ret = wumartBasicCustomerStrategy.getConfigValue(context);
        return ret;
    }

    public String tansRegion2WumartVsb(WumartBasicContext context) throws BusinessException {
        JSONObject config = this.getOfcSupplierConfig(context.getSupplierDto());
        if(null == config){
            throw new BusinessException(CommonResult.ERROR, "wumart usr not found... context=" + context.logInfo());
        }
        String ret = config.getString(WumartBasicContext.VSB);
        if (!StringUtils.hasLength(ret)) {
            throw new BusinessException(CommonResult.ERROR, "wumart vsb not found... context=" + context.logInfo());
        }
        return ret;
    }


    public String tansRegion2WumartVsb4Meipi(WumartBasicContext context) throws BusinessException {
        context.setProperty(WumartBasicContext.PARAM, WumartBasicContext.VSB);
        String ret = wumartBasicCustomerStrategy.getConfigValue(context);

        return ret;
    }

    public String tansRegion2WumartZone(WumartBasicContext context) throws BusinessException {
        JSONObject config = this.getOfcSupplierConfig(context.getSupplierDto());
        if(null == config){
            throw new BusinessException(CommonResult.ERROR, "wumart usr not found... context=" + context.logInfo());
        }
        String ret = config.getString(WumartBasicContext.ZONE);
        if (!StringUtils.hasLength(ret)) {
            throw new BusinessException(CommonResult.ERROR, "wumart zone not found... context=" + context.logInfo());
        }
        return ret;
    }


    public String tansRegion2WumartZone4Meipi(WumartBasicContext context) throws BusinessException {
        context.setProperty(WumartBasicContext.PARAM, WumartBasicContext.ZONE);
        String ret = wumartBasicCustomerStrategy.getConfigValue(context);
        return ret;
    }

    public Integer tansRegion2WumartOrderSource(WumartBasicContext context) throws BusinessException {
//        context.setProperty(WumartBasicContext.PARAM, WumartBasicContext.ORDERSOURCE);
//        String ret = this.wumartBasicHandler.getConfigValue(context);
        JSONObject config = this.getOfcSupplierConfig(context.getSupplierDto());
        if(null == config){
            throw new BusinessException(CommonResult.ERROR, "wumart usr not found... context=" + context.logInfo());
        }
        String ret = config.getString(WumartBasicContext.ORDERSOURCE);
        if (!StringUtils.hasLength(ret)) {
            throw new BusinessException(CommonResult.ERROR, "wumart orderSource not found... context=" + context.logInfo());
        }
        return Integer.valueOf(ret);
    }


    public Integer tansRegion2WumartOrderSource4Meipi(WumartBasicContext context) throws BusinessException {
        context.setProperty(WumartBasicContext.PARAM, WumartBasicContext.ORDERSOURCE);
        String ret = wumartBasicCustomerStrategy.getConfigValue(context);

        return Integer.valueOf(ret);
    }

    public boolean isForceCancel(WumartBasicContext context) throws BusinessException {
//        context.setProperty(WumartBasicContext.PARAM, WumartBasicContext.ISFORCECANCEL);
//        String ret = this.wumartBasicHandler.getConfigValue(context);
        JSONObject config = this.getOfcSupplierConfig(context.getSupplierDto());
        if(null == config){
            throw new BusinessException(CommonResult.ERROR, "wumart usr not found... context=" + context.logInfo());
        }
        String ret = config.getString(WumartBasicContext.ISFORCECANCEL);
        if (!StringUtils.hasLength(ret)) {
            throw new BusinessException(CommonResult.ERROR, "config isForceCancel not found... context=" + context.logInfo());
        }
        return Switch.valuesOf(ret) == Switch.ON;
    }

    public String getTuCodePrefix(WumartBasicContext context) throws BusinessException {
        JSONObject config = this.getOfcSupplierConfig(context.getSupplierDto());
        if(null == config){
            throw new BusinessException(CommonResult.ERROR, "wumart tu prefix found... context=" + context.logInfo());
        }
        String ret = config.getString(WumartBasicContext.TUPREFIX);
//        if (!StringUtils.hasLength(ret)) {
//            throw new BusinessException(CommonResult.ERROR, "config tuPrefix not found... context=" + context.logInfo());
//        }
        return ret;
    }

    public String getWmsPath(WumartBasicContext context) throws BusinessException {
        JSONObject config = this.getOfcSupplierConfig(context.getSupplierDto());
        if(null == config){
            throw new BusinessException(CommonResult.ERROR, "wumart usr not found... context=" + context.logInfo());
        }
        String ret = config.getString(WumartBasicContext.WMSPATH);
        if (!StringUtils.hasLength(ret)) {
            throw new BusinessException(CommonResult.ERROR, "config wmsPath not found... context=" + context.logInfo());
        }
        return ret;
    }

    public String getCostomerId(WumartBasicContext context) throws BusinessException {

        JSONObject config = this.getOfcSupplierConfig(context.getSupplierDto());
        if(null == config){
            throw new BusinessException(CommonResult.ERROR, "wumart usr not found... context=" + context.logInfo());
        }
        String ret = config.getString(WumartBasicContext.CUSTOMERID);
        if (!StringUtils.hasLength(ret)) {
            throw new BusinessException(CommonResult.ERROR, "config costomerId not found... context=" + context.logInfo());
        }
        return ret;
    }

    public String getOwnerId(WumartBasicContext context) throws BusinessException {

        JSONObject config = this.getOfcSupplierConfig(context.getSupplierDto());
        if(null == config){
            throw new BusinessException(CommonResult.ERROR, "wumart usr not found... context=" + context.logInfo());
        }
        String ret = config.getString(WumartBasicContext.OWNERID);
        if (!StringUtils.hasLength(ret)) {
            throw new BusinessException(CommonResult.ERROR, "config ownerId not found... context=" + context.logInfo());
        }
        return ret;
    }

    public String getScOwnerId(WumartBasicContext context) throws BusinessException {

        JSONObject config = this.getOfcSupplierConfig(context.getSupplierDto());
        if(null == config){
            throw new BusinessException(CommonResult.ERROR, "wumart usr not found... context=" + context.logInfo());
        }
        String ret = config.getString(WumartBasicContext.SC_OWNER_ID);
        if (!StringUtils.hasLength(ret)) {
            throw new BusinessException(CommonResult.ERROR, "config ownerId not found... context=" + context.logInfo());
        }
        return ret;
    }

    public String getDeliveryType(WumartBasicContext context) throws BusinessException {

        JSONObject config = this.getOfcSupplierConfig(context.getSupplierDto());
        if(null == config){
            throw new BusinessException(CommonResult.ERROR, "wumart usr not found... context=" + context.logInfo());
        }
        String ret = config.getString(WumartBasicContext.DELIVERY_TYPE);
        if (!StringUtils.hasLength(ret)) {
            throw new BusinessException(CommonResult.ERROR, "config ownerId not found... context=" + context.logInfo());
        }
        return ret;
    }

    public String getSaleModel(WumartBasicContext context) throws BusinessException {

        JSONObject config = this.getOfcSupplierConfig(context.getSupplierDto());
        if(null == config){
            throw new BusinessException(CommonResult.ERROR, "wumart usr not found... context=" + context.logInfo());
        }
        String ret = config.getString(WumartBasicContext.SALE_MODEL);
        if (!StringUtils.hasLength(ret)) {
            throw new BusinessException(CommonResult.ERROR, "config ownerId not found... context=" + context.logInfo());
        }
        return ret;
    }


    public String getSourceDC(WumartBasicContext context) throws BusinessException {

        JSONObject config = this.getOfcSupplierConfig(context.getSupplierDto());
        if(null == config){
            throw new BusinessException(CommonResult.ERROR, "wumart usr not found... context=" + context.logInfo());
        }
        String ret = config.getString(WumartBasicContext.SOURCEDC);
        if (!StringUtils.hasLength(ret)) {
            throw new BusinessException(CommonResult.ERROR, "config ownerId not found... context=" + context.logInfo());
        }
        return ret;
    }

    public void validAddress(String province, String city, String district, String address) throws BusinessException {
        if (!StringUtils.hasText(province)) {
            throw new BusinessException(CommonResult.ERROR, "province can not be blank!");
        }
        if (!StringUtils.hasText(city)) {
            throw new BusinessException(CommonResult.ERROR, "city can not be blank!");
        }
        if (!StringUtils.hasText(district)) {
            throw new BusinessException(CommonResult.ERROR, "district can not be blank!");
        }
        if (!StringUtils.hasText(address)) {
            throw new BusinessException(CommonResult.ERROR, "address can not be blank!");
        }
        int length = province.length() + city.length() + district.length() + address.length();
        if (length + 3 > 35) { //物美SAP限制
            throw new BusinessException(CommonResult.ERROR, "address is too long!");
        }
    }

}
