package com.lsh.ofc.core.enums;

/**
 * WMS订单类型枚举
 *
 * @author peter
 * @date 16/10/1
 */
public enum WGOrderType {
    /**
     *
     */
    SO(1, "SO单"),
    PRESEll_SO(2, "预售SO"),
    PRE_WAREHOUSE_SO(3, "前置仓订单");

    private final Integer code;

    private final String desc;

    WGOrderType(Integer code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public Integer getCode() {
        return code;
    }

    public String getDesc() {
        return desc;
    }
}
