package com.lsh.ofc.core.service;

import com.lsh.base.common.exception.BusinessException;
import com.lsh.ofc.core.entity.OfcObdHead;
import com.lsh.ofc.core.entity.OfcOrderHead;
import com.lsh.ofc.core.entity.OfcSoHead;

import java.util.List;

/**
 * Project Name: lsh-ofc
 * Created by fuhao
 * Date: 18/6/19
 * Time: 18/6/19.
 * 北京链商电子商务有限公司
 * Package name:com.lsh.ofc.core.service.
 * desc:类功能描述
 * @author peter
 */
public interface OfcStoCreateService {
    /**
     *
     * @param targetOrderHead
     * @param targetSoHeads
     * @return
     * @throws BusinessException
     */
    int create(OfcOrderHead targetOrderHead, List<OfcSoHead> targetSoHeads) throws BusinessException;

    /**
     *
     * @param targetOrderHead
     * @param targetSoHead
     * @param ofcObdHead
     * @return
     * @throws BusinessException
     */
    int createSoObd(OfcOrderHead targetOrderHead, OfcSoHead targetSoHead, OfcObdHead ofcObdHead) throws BusinessException;

    /**
     *
     * @param targetOrderHead
     * @return
     * @throws BusinessException
     */
    boolean addPoTask(OfcOrderHead targetOrderHead) throws BusinessException;
}
