package com.lsh.ofc.core.enums;

/**
 *
 * @author miaozhuang
 * @date 18/10/27
 */
public enum Org2System {
    /**
     * 机构与来源对应关系
     */
    WM_SYS(1, 7),
    LSH_BJ_SYS(2, 7),
    LSH_TJ_SYS(3, 7),
    WG_BJ_SYS(4, 7),
    WG_TJ_SYS(5, 7),
    NG_BJ_SYS(10, 7);

    private Integer org;
    private Integer system;

    Org2System(Integer org, Integer system) {
        this.org = org;
        this.system = system;
    }

    public static Org2System getOrg2System(Integer org) {
        if (org == null) {
            return null;
        }

        for (Org2System org2System : Org2System.values()) {
            if (org.compareTo(org2System.getOrg()) == 0) {
                return org2System;
            }
        }

        return null;
    }

    public Integer getOrg() {
        return org;
    }

    public Integer getSystem() {
        return system;
    }
}
