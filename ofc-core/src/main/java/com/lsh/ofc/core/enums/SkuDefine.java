package com.lsh.ofc.core.enums;

/**
 * 地域枚举
 *
 * @author peter
 * @date 18/1/26
 */
public enum SkuDefine {
    /**
     *
     */
    EA(1, "EA"),
    BOX(2, "BOX"),
    KG(3, "KG");

    private final Integer code;

    private final String desc;

    SkuDefine(Integer code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public Integer getCode() {
        return code;
    }

    public String getDesc() {
        return desc;
    }
}
