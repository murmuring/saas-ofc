package com.lsh.ofc.core.enums;

/**
 * Project Name: lsh-ofc
 *
 * @author peter
 * @date 18/6/22
 * 北京链商电子商务有限公司
 * Package
 * desc: 二次配送标志  oms 传送 默认非前置仓 0 表示不是，1 表示是
 */
public enum SecondDistribution {
    /**
     * 二次配送标志
     */
    NO(0, "否"), YES(1, "是");

    private final Integer code;

    private final String desc;

    SecondDistribution(Integer code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public Integer getCode() {
        return code;
    }

    public String getDesc() {
        return desc;
    }
}
