package com.lsh.ofc.core.wumartbasic;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.lsh.ofc.core.constant.Constants;
import com.lsh.ofc.proxy.context.WumartBasicContext;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.text.MessageFormat;
import java.util.Map;

/**
 * Project Name: lsh-ofc
 * Author: panxudong
 * 北京链商电子商务有限公司
 * Desc: 类功能描述
 * Package Name: com.lsh.ofc.proxy.handler
 * Time: 2017-07-27 下午3:21
 */
@Service
@Slf4j
public class WumartBasicCreateSoStrategy extends AbstractWumartBasicStrategy {


    @Override
    public boolean validate(WumartBasicContext context) {
        if (context.size() > 3) {
            if (super.basicValidate(context)) {
                Object dcObj = context.getProperty(WumartBasicContext.DC);
                if (dcObj != null) {
                    String dc = String.valueOf(dcObj);
                    if (StringUtils.hasText(dc)) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    @Override
    public String getConfigKey(WumartBasicContext context) {
        Integer region = (Integer) context.getProperty(WumartBasicContext.REGION);
        Integer supplierOrg = (Integer)context.getProperty(WumartBasicContext.SUPPLIERORG);
        Integer supplierId = (Integer)context.getProperty(WumartBasicContext.SUPPLIERID);
        return buildRedisKey(region, supplierOrg, supplierId);
    }

    private String buildRedisKey(Integer region, Integer supplierOrg, Integer supplierId) {
        String redisKey = MessageFormat.format(Constants.OFC_SUPPLIER_CONFIG, String.valueOf(region), String.valueOf(supplierOrg), String.valueOf(supplierId));
        return redisKey;
    }

    @Override
    public String getConfigValue(WumartBasicContext context) {
        Map<String, String> configs = super.getConfig(context);
        log.info("redis getConfigValue configs is " + configs);
        log.info("redis getConfigValue context is " + context.logInfo());
        String dc = (String) context.getProperty(WumartBasicContext.DC);
        String config = configs.get(dc);

        log.info("redis getConfigValue dc config is " + JSON.toJSONString(config));
        if (!StringUtils.hasText(config)) {
            return null;
        }
        log.info("redis getConfigValue context is " + JSON.toJSONString(context));

        JSONObject configObject = JSONObject.parseObject(config);
        log.info("redis getConfigValue configObject is " + JSON.toJSONString(configObject));
        String key = String.valueOf(context.getProperty(WumartBasicContext.PARAM));

        log.info("redis getConfigValue para value is " + configObject.getString(key));
        return configObject.getString(key);
    }

}
