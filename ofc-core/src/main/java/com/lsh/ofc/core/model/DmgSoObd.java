package com.lsh.ofc.core.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.math.BigDecimal;

/**
 * Project Name: lsh-ofc
 *
 * @author peter
 * @date 19/3/27
 * 北京链商电子商务有限公司
 * Package
 * desc:
 */
@Getter
@Setter
@NoArgsConstructor
@ToString
public class DmgSoObd {

    private String obdId;
    private String sapId;
    private String sumSoId;
    private Integer soOrderItem;
    private String customerNumber;
    private Integer materialNo;
    private String mandt;
    private BigDecimal obdOccupy;
    /**
     *
     */
    private Long stoCreated;
    /**
     *
     */
    private Long obdCreated;
}
