package com.lsh.ofc.core.handler.logistics;

import com.lsh.base.common.exception.BusinessException;
import com.lsh.ofc.core.entity.OfcOrderHead;
import com.lsh.ofc.core.entity.OfcSoHead;
import com.lsh.ofc.core.entity.OfcTask;
import com.lsh.ofc.core.enums.OfcTaskStatus;
import com.lsh.ofc.core.enums.OfcTaskType;
import com.lsh.ofc.core.model.SoMergeOrderInfoBO;
import com.lsh.ofc.core.service.OfcTaskService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationContext;

import java.util.List;

/**
 * Project Name: PCLogisticsHandler
 * 北京链商电子商务有限公司
 * @author peter
 * Date: 19/3/5
 * Package Name: com.lsh.ofc.core.handler.logistics
 * Description
 */
@Slf4j
@Deprecated
public class PcLogisticsHandler extends AbstractLogisticsHandler {

    private OfcTaskService ofcTaskService;

    protected PcLogisticsHandler(final ApplicationContext context, final OfcOrderHead ofcOrderHead, final List<OfcSoHead> sos) {
        super(context, ofcOrderHead, sos);
        ofcTaskService = context.getBean(OfcTaskService.class);
    }

    @Override
    protected Boolean process(SoMergeOrderInfoBO orderInfo) throws BusinessException {
        for (OfcSoHead so : sos) {
            // 该task，针对PC猪肉供给，去物美查询obd信息
            if (so.getDistributionWay().intValue() == 90) {
                OfcTask ofcTask = new OfcTask();
                ofcTask.setRefId(so.getOrderCode());
                ofcTask.setContent(so.getSoBillCode());
                ofcTask.setType(OfcTaskType.OBD_QUERY.getValue());
                ofcTask.setStatus(OfcTaskStatus.NEW.getValue());
                this.ofcTaskService.addTask(ofcTask);
            }
            log.info("PC供给， 不下发tms、pms，不修改oms");
        }

        return Boolean.TRUE;
    }
}
