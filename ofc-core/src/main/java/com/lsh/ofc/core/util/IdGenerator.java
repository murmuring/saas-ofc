package com.lsh.ofc.core.util;

import com.relops.snowflake.Snowflake;
import org.apache.commons.lang3.StringUtils;

import java.util.UUID;

/**
 * ID生成器
 * Created by huangdong on 16/8/8.
 */
public class IdGenerator {

    private IdGenerator() {
    }

    public static String uuid() {
        char[] chars = UUID.randomUUID().toString().toCharArray();
        char[] uuid = new char[32];
        for (int i = 0, j = 0; i < 36; i++) {
            if (i == 8 || i == 13 || i == 18 || i == 23) {
                continue;
            }
            uuid[j++] = chars[i];
        }
        return new String(uuid);
    }

    private static volatile Snowflake snowflake;

    public static long genId() {
        if (snowflake == null) {
            synchronized (IdGenerator.class) {
                if (snowflake == null) {

                    String node = System.getProperty("snowflake.node");
                    if(StringUtils.isBlank(node)){
                        node = "1";
                    }

                    Snowflake instance = new Snowflake(Integer.parseInt(node));
                    snowflake = instance;
                }
            }
        }
        return snowflake.next();
    }


    public static String getId() {

        return String.valueOf(genId());
    }
}
