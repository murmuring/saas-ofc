package com.lsh.ofc.core.service.impl;

import com.lsh.ofc.core.entity.OfcSoHead;
import com.lsh.ofc.core.service.CancelOrderService;

import java.util.List;

/**
 *
 * @author panxudong
 * @date 17/3/28
 */
public abstract class AbstractCancelOrderService implements CancelOrderService {

    /**
     * 构建请求对象
     * @param list
     * @return
     */
    protected abstract List<String> buildRequest(List<OfcSoHead> list);

}
