package com.lsh.ofc.core.handler;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.lsh.ofc.core.constant.Constants;
import com.lsh.ofc.core.entity.OfcObdHead;
import com.lsh.ofc.core.entity.OfcOrderHead;
import com.lsh.ofc.core.entity.OfcSoHead;
import com.lsh.ofc.core.model.DmgSoObd;
import com.lsh.ofc.core.model.DmgSoObdVo;
import com.lsh.ofc.core.model.DmgSoSo;
import com.lsh.ofc.core.redis.RedisTemplate;
import com.lsh.ofc.core.service.OfcOrderService;
import com.lsh.ofc.core.service.OfcSoService;
import com.lsh.ofc.core.service.OfcStoCreateService;
import com.lsh.ofc.core.service.OrderSoObdService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 创建SO OBD 汇总 处理Handler
 *
 * @author peter
 * @date 19/4/9
 */
@Service
@Slf4j
public class CreateSumSoObdHandler {

    @Autowired
    private OfcOrderService ofcOrderService;

    @Autowired
    private OfcStoCreateService ofcStoCreateService;

    @Autowired
    private OrderSoObdService orderSoObdService;

    @Autowired
    private OfcSoService ofcSoService;

    @Autowired
    private RedisTemplate redisTemplate;

    public int process(final DmgSoObdVo dmgSoObdVo) {

        //构造 订单 so obdjson
        String sumSoId = "";
        List<DmgSoSo> dmgSoSoList = dmgSoObdVo.getSoSo();
        List<DmgSoObd> soObdList = dmgSoObdVo.getSoObd();
        Map<String, List<DmgSoSo>> soMap = new HashMap<>();
        for (DmgSoSo soSo : dmgSoSoList) {
            String soId = soSo.getSoId();
            sumSoId = soSo.getSumSoId();
            List<DmgSoSo> soList = soMap.get(soId);
            if (CollectionUtils.isEmpty(soList)) {
                soList = new ArrayList<>();
                soMap.put(soId, soList);
            }
            soList.add(soSo);
        }

        String content = this.getTotalSoRedis(sumSoId);
        if (StringUtils.isEmpty(content)) {
            log.info("redis 没有数据 ：sumSoId is " + sumSoId);
            return 0;
        }

        JSONObject redisSo = JSON.parseObject(content);
        log.info("物美回调so信息 redisSo is" + redisSo.toJSONString());
        JSONArray redisSoArray = redisSo.getJSONArray("details");
        if (null == redisSoArray || redisSoArray.isEmpty()) {
            log.info("redis 没有数据 ：sumSoId is " + sumSoId);
            return 0;
        }

        Map<Integer, JSONObject> redisSoDetailMap = new HashMap<>();
        for (int i = 0; i < redisSoArray.size(); i++) {
            JSONObject soDetail = redisSoArray.getJSONObject(i);
            Integer itemNbr = soDetail.getInteger("no");
            redisSoDetailMap.put(itemNbr, soDetail);
        }
        //汇总so
        OfcSoHead targetOfcSoHead = this.orderSoObdService.ofcSoHeadBuilder(soMap, redisSoDetailMap);
        OfcOrderHead filter = new OfcOrderHead();
        // 汇总so的某个订单的订单号
        filter.setOrderCode(targetOfcSoHead.getOrderCode());
        OfcOrderHead fromOrderHead = ofcOrderService.findOne(filter, false);
        // 构造so对应的订单信息
        OfcOrderHead targetOfcOrderHead = this.orderSoObdService.ofcOrderHeadBuilder(targetOfcSoHead, fromOrderHead);
        // 构造订单对应的obd信息
        if(!CollectionUtils.isEmpty(soObdList)){
            OfcObdHead targetOfcObdHead = this.orderSoObdService.ofcObdHeadBuilder(targetOfcSoHead, soObdList);

            JSONObject soExt = JSON.parseObject(targetOfcSoHead.getExt());
            soExt.put("obdCode",targetOfcObdHead.getObdCode());
        }

        targetOfcSoHead.setOrderCode(targetOfcOrderHead.getOrderCode());
        log.info("[物美 so obd 汇总] targetOfcOrderHead : " + JSON.toJSONString(targetOfcOrderHead));
        log.info("[物美 so obd 汇总] targetOfcSoHead : " + JSON.toJSONString(targetOfcSoHead));
//        log.info("[物美 so obd 汇总] targetOfcObdHead : " + JSON.toJSONString(targetOfcObdHead));

        OfcSoHead filterSo = new OfcSoHead();
        filterSo.setSoCode(targetOfcSoHead.getSoCode());
        List<OfcSoHead> soHeadList = ofcSoService.findList(filterSo, false);
        if (!CollectionUtils.isEmpty(soHeadList)) {
            log.info("物美soCode已存在 ：soCode is " + targetOfcSoHead.getSoCode());
            return 1;
        }
        //插入数据库
        return ofcStoCreateService.createSoObd(targetOfcOrderHead, targetOfcSoHead, null);
    }

    /**
     *
     * @param soBillCode
     * @return
     */
    private String getTotalSoRedis(String soBillCode) {
        String wumartOFCTotalSoKey = MessageFormat.format(Constants.WUMART_OFC_TOTAL_SO, soBillCode);
        return redisTemplate.get(wumartOFCTotalSoKey);
    }

}
