package com.lsh.ofc.core.enums;

/**
 * Project Name: lsh-ofc
 * @author peter
 * Date: 18/6/23
 * Time: 18/6/23.
 * 北京链商电子商务有限公司
 * Package name:com.lsh.ofc.core.enums.
 * desc:类功能描述
 */
public enum DistributionType {
    /**
     * so
     */
    ORDER_SO("1", "SO 正常"),
    /**
     * SO SC
     */
    ORDER_SO_SC("2", "SO 需要汇总给SC"),
    /**
     * SO PSI
     */
    ORDER_SO_PSI("3", "SO 需要汇总给PSI"),
    /**
     * 前置仓sto
     */
    SEED_STO("10","STO");

    private final String code;

    private final String desc;

    DistributionType(String code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public String getCode() {
        return code;
    }

    public String getDesc() {
        return desc;
    }

}
