package com.lsh.ofc.core.service;

import com.lsh.base.common.exception.BusinessException;
import com.lsh.ofc.core.entity.OfcObdDetail;
import com.lsh.ofc.core.entity.OfcObdHead;
import com.lsh.ofc.core.model.PackageCodeBo;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * OFC OBD Service
 *
 * @author huangdong
 * @date 16/9/12
 */
public interface OfcObdService {

    /**
     * 查询OBD信息
     *
     * @param filter
     * @param fillDetails
     * @return
     * @throws BusinessException
     */
    OfcObdHead findOne(OfcObdHead filter, boolean fillDetails) throws BusinessException;

    /**
     * 查询OBD信息列表
     *
     * @param filter
     * @param fillDetails
     * @return
     * @throws BusinessException
     */
    List<OfcObdHead> findList(OfcObdHead filter, boolean fillDetails) throws BusinessException;

    /**
     * 查询OBD信息列表
     *
     * @param orderCodes
     * @param fillDetails
     * @return
     * @throws BusinessException
     */
    List<OfcObdHead> findListByOrderCodes(List<Long> orderCodes, boolean fillDetails) throws BusinessException;

    /**
     * 根据SO单据号查询OBD明细列表
     *
     * @param soBillCode
     * @return
     * @throws BusinessException
     */
    List<OfcObdDetail> findDetails(String soBillCode) throws BusinessException;

    /**
     * 创建OBD信息
     *
     * @param order   obd
     * @param addTask flag
     * @throws BusinessException
     */
    void create(OfcObdHead order, boolean updateQty, boolean addTask) throws BusinessException;

    /**
     * 创建OBD信息
     *
     * @param order
     * @param addTask flag
     * @throws BusinessException
     */
    void confirm(OfcObdHead order, boolean addTask) throws BusinessException;

    /**
     * 合并OBD信息
     *
     * @param orderCode
     * @return
     * @throws BusinessException
     */
    int merger(Long orderCode) throws BusinessException;

    /**
     * 更新OBD信息(返仓)
     *
     * @param billCode
     * @param returnQtyMap
     * @return
     * @throws BusinessException
     */
    BigDecimal update4Return(String billCode, Map<Long, BigDecimal> returnQtyMap) throws BusinessException;

    /**
     * 更新OBD信息(返仓)
     *
     * @param billCode
     * @return
     * @throws BusinessException
     */
    int update4ext(String billCode, String ext) throws BusinessException;

    /**
     * 根据包裹数量，生成相应数量的包裹编码
     * 生成策略：soBillCode-编码； 例如：soBillCode-1、soBillCode-2、soBillCode-3
     *
     * @return
     */
    List<String> generatePackageCodes(String soBillCode, Integer packageNum);

    /**
     * 拼装包裹码；规则：soBillCode-packageNo
     *
     * @return
     */
    String buildPackageCodes(PackageCodeBo packageCodeBo);
}
