package com.lsh.ofc.core.dao;

import com.lsh.ofc.core.base.BaseDAO;
import com.lsh.ofc.core.entity.OfcTask;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * OFC任务DAO
 *
 * @author huangdong
 * @date 16/8/28
 */
@Repository
public interface OfcTaskDAO extends BaseDAO<OfcTask, Long> {

    /**
     * 批量插入任务
     *
     * @param ofcTaskList 任务列表
     * @return int
     */
    int insertBatch(List<OfcTask> ofcTaskList);

    /**
     * 根据类型和状态分片获取任务
     *
     * @param type          任务类型
     * @param statuses      状态
     * @param shardingCount 分片数
     * @param shardingItems 分片项
     * @param fetchSize     获取个数
     * @param delayTime     延迟时间
     * @return List<OfcTask>
     */
    List<OfcTask> fetchTasks(@Param("type") Integer type, @Param("statuses") List<Integer> statuses, @Param("shardingCount") int shardingCount, @Param("shardingItems") List<Integer> shardingItems, @Param("fetchSize") int fetchSize, @Param("delayTime") Integer delayTime);

    /**
     * 根据类型和状态获取任务，不关心任务执行次数
     *
     * @param type      任务类型
     * @param statuses  状态
     * @param fetchSize 获取个数
     * @param delayTime 延迟时间
     * @return List<OfcTask>
     */
    List<OfcTask> fetchTasksWithoutExecCount(@Param("type") Integer type, @Param("statuses") List<Integer> statuses, @Param("fetchSize") int fetchSize, @Param("delayTime") Integer delayTime);


    List<OfcTask> selectByRefIdAndType(@Param("refId") Long refId, @Param("type") Integer type);

    /**
     * 更新已处理任务状态
     *
     * @param id           任务id
     * @param preStatus    状态
     * @param updateStatus 更新状态
     * @return 结果标记
     */
    int updateProcessedTask(@Param("id") Long id, @Param("expectStatus") Integer preStatus, @Param("updateStatus") Integer updateStatus);

    /**
     * 插入历史数据
     *
     * @param status 状态
     * @return 结果标记
     */
    int insert4History(@Param("status") Integer status);

    /**
     * 删除历史数据
     *
     * @param status 状态
     * @return 结果标记
     */
    int delete4History(@Param("status") Integer status);

    /**
     * 查询异常任务数据
     *
     * @param type      类型
     * @param startTime 开始时间
     * @return List<OfcTask>
     */
    List<OfcTask> fetchErrorTasks(@Param("type") int type, @Param("startTime") int startTime);

    int disablePendingTask(@Param("refId") Long refId, @Param("type") Integer type, @Param("statuses") List<Integer> statuses);
}