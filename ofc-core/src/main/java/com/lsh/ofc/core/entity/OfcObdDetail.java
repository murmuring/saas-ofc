package com.lsh.ofc.core.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * OFC OBD单据明细
 *
 * @author huangdong
 * @date 16/9/12
 */
@Setter
@Getter
@NoArgsConstructor
@ToString
public class OfcObdDetail extends Vender implements Serializable {

    private static final long serialVersionUID = -7040176856494225991L;
    /**
     * ID
     */
    private Long id;

    /**
     * SO单据号
     */
    private String soBillCode;

    /**
     * SKU编号
     */
    private Integer itemCode;

    /**
     * SKU编号
     */
    private Long skuCode;

    /**
     * SKU供货码
     */
    private String skuSupplyCode;

    /**
     * SKU供货价
     */
    private BigDecimal skuSupplyPrice;

    /**
     * SKU发货总数
     */
    private BigDecimal skuDeliverQty;

    /**
     * SKU退货总数
     */
    private BigDecimal skuReturnQty;

    /**
     * 税率
     */
    private BigDecimal taxRate;

    /**
     * 其它信息
     */
    private String ext;

    /**
     * 创建时间
     */
    private Integer createTime;

    /**
     * 更新时间
     */
    private Integer updateTime;
}
