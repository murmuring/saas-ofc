package com.lsh.ofc.core.handler;

import com.alibaba.fastjson.JSON;
import com.lsh.base.common.exception.BusinessException;
import com.lsh.ofc.core.entity.OfcSoHead;
import com.lsh.ofc.core.enums.FulfillChannel;
import com.lsh.ofc.core.service.CancelOrderService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author panxudong
 * @date 17/3/28
 */
@Service
public class CancelOrderHandler {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Resource(name = "cancelOrderLshWmsHandler")
    private CancelOrderService cancelOrderLshWmsService;

    @Resource(name = "cancelOrderByWgHandler")
    private CancelOrderService cancelOrderByWgService;

    @Resource(name = "cancelOrderWumartOfcHandler")
    private CancelOrderService cancelOrderWumartOfcService;

    @Resource(name = "cancelOrderByLshCloudHandler")
    private CancelOrderService cancelOrderByLshCloudService;

    @Resource(name = "cancelOrderByLshCloudSCHandler")
    private CancelOrderService cancelOrderByLshCloudSCHandler;

    public boolean execute(List<OfcSoHead> list, FulfillChannel fulfillChannel) throws BusinessException {
        CancelOrderService cancelOrderService = this.getHandler(fulfillChannel);
        return cancelOrderService.cancelOrder(list);
    }

    private CancelOrderService getHandler(FulfillChannel fulfillChannel) {

        logger.info("订单取消 fulfillChannel {}", JSON.toJSONString(fulfillChannel));
        if (fulfillChannel == FulfillChannel.LSH_WMS) {
            return cancelOrderLshWmsService;
        } else if (fulfillChannel == FulfillChannel.SAAS_PSI) {
            return cancelOrderByWgService;
        } else if (fulfillChannel == FulfillChannel.SAAS_OFC) {
            return cancelOrderByLshCloudService;
        } else if (fulfillChannel == FulfillChannel.SAAS_SC) {
            return cancelOrderByLshCloudSCHandler;
        } else {
            return cancelOrderWumartOfcService;
        }
    }

}
