package com.lsh.ofc.core.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

/**
 * OFC RO单据头
 *
 * @author huangdong
 * @date 16/9/12
 */
@Setter
@Getter
@ToString
@NoArgsConstructor
public class OfcRoHead extends Vender implements Serializable {

    private static final long serialVersionUID = -7423955577366901311L;
    /**
     * ID
     */
    private Long id;

    /**
     * RO单据号
     */
    private String roBillCode;

    /**
     * SO单据号
     */
    private String soBillCode;

    /**
     * 订单号
     */
    private Long orderCode;

    /**
     * 返仓单号
     */
    private Long returnCode;

    /**
     * 地域编号
     */
    private Integer regionCode;

    /**
     * 地址编号
     */
    private Long addressCode;

    /**
     * 仓库编号
     */
    private String warehouseCode;

    /**
     * 仓库名称
     */
    private String warehouseName;

    /**
     * 供货商ID
     */
    private Integer supplierId;

    private String supplierGroup;

    private String supplierCode;

    /**
     * 供货商DC
     */
    private String supplierDc;

    /**
     * 供货商机构
     */
    private Integer supplierOrg;

    /**
     * 履约WMS
     */
    private Integer fulfillWms;

    /**
     * 履约渠道
     */
    private Integer fulfillChannel;

    /**
     * 履约SO单号
     */
    private String soCode;

    /**
     * RO单号
     */
    private String roCode;

    /**
     * RO状态
     */
    private Integer roStatus;

    /**
     * SKU退货总数
     */
    private BigDecimal totalSkuReturnQty;

    /**
     * 其它信息
     */
    private String ext;

    /**
     * 创建时间
     */
    private Integer createTime;

    /**
     * 更新时间
     */
    private Integer updateTime;

    /**
     * 是否有效（0:无效；1:有效）
     */
    private Integer valid;

    /**
     * RO单据明细列表
     */
    private List<OfcRoDetail> details;
}
