package com.lsh.ofc.core.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.lsh.base.common.exception.BusinessException;
import com.lsh.ofc.core.constant.Constants;
import com.lsh.ofc.core.dao.OfcSoDetailDAO;
import com.lsh.ofc.core.dao.OfcSoHeadDAO;
import com.lsh.ofc.core.entity.OfcSoDetail;
import com.lsh.ofc.core.entity.OfcSoHead;
import com.lsh.ofc.core.entity.OfcTask;
import com.lsh.ofc.core.enums.*;
import com.lsh.ofc.core.exception.EC;
import com.lsh.ofc.core.model.Costs;
import com.lsh.ofc.core.redis.RedisTemplate;
import com.lsh.ofc.core.service.OfcBillService;
import com.lsh.ofc.core.service.OfcOperateLogService;
import com.lsh.ofc.core.service.OfcSoService;
import com.lsh.ofc.core.service.OfcTaskService;
import com.lsh.ofc.core.util.IdGenerator;
import com.lsh.ofc.core.util.OFCUtils;
import com.lsh.ofc.proxy.context.WumartBasicContext;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.math.BigDecimal;
import java.text.MessageFormat;
import java.util.*;

/**
 * @author peter
 */
@Service
@Slf4j
public class OfcSoServiceImpl implements OfcSoService {

    @Autowired
    private OfcSoHeadDAO ofcSoHeadDAO;

    @Autowired
    private OfcSoDetailDAO ofcSoDetailDAO;

    @Autowired
    private OfcBillService ofcBillService;

    @Autowired
    private OfcOperateLogService ofcOperateLogService;

    @Autowired
    private OfcTaskService ofcTaskService;

    @Autowired
    private RedisTemplate redisTemplate;

    @Override
    public int count(OfcSoHead filter) throws BusinessException {
        if (filter.getValid() == null) {
            filter.setValid(Valid.enable.getValue());
        }
        return this.ofcSoHeadDAO.count(filter);
    }

    @Override
    public OfcSoHead findOne(OfcSoHead filter, boolean fillDetails) throws BusinessException {
        if (filter.getValid() == null) {
            filter.setValid(Valid.enable.getValue());
        }
        OfcSoHead so = this.ofcSoHeadDAO.findOne(filter);
        if (so == null) {
            return null;
        }
        if (fillDetails) {
            so.setDetails(this.findDtails(so.getSoBillCode()));
        }

        return so;
    }

    @Override
    public List<OfcSoHead> findList(OfcSoHead filter, boolean fillDetails) throws BusinessException {
        if (filter.getValid() == null) {
            filter.setValid(Valid.enable.getValue());
        }
        List<OfcSoHead> sos = this.ofcSoHeadDAO.findList(filter);
        if (CollectionUtils.isEmpty(sos)) {
            return Collections.emptyList();
        }
        if (fillDetails) {
            for (OfcSoHead so : sos) {
                so.setDetails(this.findDtails(so.getSoBillCode()));
            }
        }

        return sos;
    }

    @Override
    public List<OfcSoHead> findListFilterByTime(Integer startTime, Integer endTime, String supplierGroup, String supplierCode, Integer regionCode, Integer supplierOrg, Integer supplierId) throws BusinessException {
        Map<String,Object> queryMap = new HashMap<>();
        queryMap.put("startTime",startTime);
        queryMap.put("endTime",endTime);
        queryMap.put("supplierGroup",supplierGroup);
        queryMap.put("supplierCode",supplierCode);
        queryMap.put("regionCode",regionCode);
        queryMap.put("supplierOrg",supplierOrg);
        queryMap.put("supplierId",supplierId);
        log.info("queryMap:"+JSON.toJSONString(queryMap));
        List<OfcSoHead> sos = this.ofcSoHeadDAO.findListFilterByTime(queryMap);
        if (CollectionUtils.isEmpty(sos)) {
            return Collections.emptyList();
        }
        log.info("queryResult size:"+JSON.toJSONString(sos.size()));
        for (OfcSoHead so : sos) {
            so.setDetails(this.findDtails(so.getSoBillCode()));
        }

        return sos;
    }

    /**
     * @param soBillCode
     * @return
     * @throws BusinessException
     */
    @Override
    public List<OfcSoDetail> findDtails(String soBillCode) throws BusinessException {
        if (!StringUtils.hasLength(soBillCode)) {
            return Collections.emptyList();
        }
        OfcSoDetail param = new OfcSoDetail();
        param.setSoBillCode(soBillCode);
        return this.ofcSoDetailDAO.findList(param);
    }

    /**
     * @param sos
     * @return
     * @throws BusinessException
     */
    @Transactional(rollbackFor = RuntimeException.class)
    @Override
    public int insert(List<OfcSoHead> sos) throws BusinessException {
        if (CollectionUtils.isEmpty(sos)) {
            log.info("sos 订单信息不存在");
            return 0;
        }

        int cnt = 0;

        for (OfcSoHead soHead : sos) {
            if (soHead == null) {
                log.info("soHead 订单信息不存在");
                continue;
            }
            List<OfcSoDetail> details = soHead.getDetails();
            if (CollectionUtils.isEmpty(details)) {
                log.info("soHead 详情细信息 不是是空");
                continue;
            }
            int ts = OFCUtils.currentTime();
            soHead.setSoBillCode(this.getSoBillCode(soHead));
            soHead.setCreateTime(ts);
            soHead.setUpdateTime(ts);
            soHead.setValid(Valid.enable.getValue());

            cnt += this.ofcSoHeadDAO.insert(soHead);
            for (OfcSoDetail detail : soHead.getDetails()) {
                detail.setSoBillCode(soHead.getSoBillCode());
                detail.setCreateTime(ts);
                detail.setUpdateTime(ts);
                this.ofcSoDetailDAO.insert(detail);
            }
            //记录操作日志
            this.ofcOperateLogService.insert(soHead.getSoBillCode(), BillType.SO, OfcOperateEnum.SO_UNCREATED, soHead.getTotalSkuOrderQty().toString(), soHead.getVenderId());
        }

        return cnt;
    }

    /**
     * @param so
     * @return
     */
    private String getSoBillCode(OfcSoHead so) {
        String billCode;
        if (FulfillWms.NONE.getValue().equals(so.getFulfillWms())) {
            billCode = so.getOrderCode().toString();
        } else {
            JSONObject soExt = JSON.parseObject(so.getExt());
            Integer logisticsMode = soExt.getInteger("logisticsMode");
//            Integer provider_id = soExt.getInteger("provider_id");
            if (null != logisticsMode && logisticsMode.equals(LogisticsTypeEnum.CLOUD_DC_2_DC.getType())) {
                billCode = new StringBuilder(20).append(so.getOrderCode()).toString();
            } else {
                billCode = String.valueOf(IdGenerator.genId());
            }
        }

        if (StringUtils.isEmpty(billCode)) {
            billCode = String.valueOf(IdGenerator.genId());
        }

        return billCode;

    }

    /**
     * @param so
     * @return
     * @throws BusinessException
     */
    @Transactional(rollbackFor = RuntimeException.class)
    @Override
    public int insert(OfcSoHead so) throws BusinessException {

        int ts = OFCUtils.currentTime();

        List<OfcSoDetail> details = so.getDetails();
        if (CollectionUtils.isEmpty(details)) {
            return 0;
        }
        so.setValid(Valid.enable.getValue());

        int cnt = this.ofcSoHeadDAO.insert(so);
        if (cnt > 0) {
            for (OfcSoDetail detail : so.getDetails()) {
                detail.setCreateTime(ts);
                detail.setUpdateTime(ts);
                this.ofcSoDetailDAO.insert(detail);
            }
        }
        //记录操作日志
        this.ofcOperateLogService.insert(so.getSoBillCode(), BillType.SO, OfcOperateEnum.SO_UNCREATED, so.getTotalSkuOrderQty().toString(), so.getVenderId());

        return cnt;
    }

    /**
     * @param so
     * @param expect
     * @param update
     * @return
     * @throws BusinessException
     */
    @Transactional(rollbackFor = RuntimeException.class)
    @Override
    public int update4Create(OfcSoHead so, SoStatus expect, SoStatus update, Long venderId) throws BusinessException {
        if (!SoStatus.UNCREATED.equals(expect) && !SoStatus.CREATING.equals(expect) && !SoStatus.CREATE_FAIL.equals(expect)) {
            throw EC.ERROR.exception("【更新SO信息(创建)】参数错误！expectStatus" + expect);
        }
        if (!SoStatus.CREATED.equals(update) && !SoStatus.CREATING.equals(update) && !SoStatus.CREATE_FAIL.equals(update)) {
            throw EC.ERROR.exception("【更新SO信息(创建)】参数错误！updateStatus" + update);
        }
        Long id = so.getId();
        String billCode = so.getSoBillCode();
        if (id == null && billCode == null) {
            throw EC.ERROR.exception("【更新SO信息(创建)】参数错误！更新SO头主键为空！");
        }

        OfcSoHead filter = new OfcSoHead();
        filter.setId(id);
        filter.setSoBillCode(billCode);
        filter.setSoStatus(expect.getValue());
        so.setSoStatus(update.getValue());
        int ret = this.ofcSoHeadDAO.updateByFilter(so, filter);
        if (ret < 1) {
            return ret;
        }
        List<OfcSoDetail> details = so.getDetails();
        if (!CollectionUtils.isEmpty(details)) {
            for (OfcSoDetail detail : details) {
                if (detail.getId() == null) {
                    throw EC.ERROR.exception("【更新SO信息(创建)】参数错误！更新SO明细主键为空！");
                }
                this.ofcSoDetailDAO.update(detail);
            }
        }

        Integer wumartFill = 0;
        Integer wgFill = 0;
        Integer saleModel = 0;
        Integer deliveryType = 0;
        if (!StringUtils.isEmpty(so.getExt())) {
            JSONObject ext = JSON.parseObject(so.getExt());
            wumartFill = ext.getInteger(WumartBasicContext.WUMART_FILL);
            if (null == wumartFill) {
                wumartFill = 0;
            }

            wgFill = ext.getInteger(WumartBasicContext.WG_FILL);
            if (null == wgFill) {
                wgFill = 0;
            }

            saleModel = ext.getInteger(WumartBasicContext.SALE_MODEL);
            deliveryType = ext.getInteger(WumartBasicContext.DELIVERY_TYPE);
        }

        if (SoStatus.CREATED.equals(update) && "0".equals(so.getPushPsiFlag())) {
            OfcSoHead param = new OfcSoHead();
            param.setId(id);
            OfcSoHead entity = this.findList(param, true).get(0);

            //插入合单并下发订单任务
            Long orderCode = entity.getOrderCode();
            this.addMergeTask(orderCode, venderId);
//            if (saleModel == 1 && deliveryType == 3) {
//                this.addPushSo(orderCode, venderId);
//            }
            log.info( orderCode + " wumartFill " + wumartFill + " wgFill " + wgFill + " saleModel " + saleModel + " deliveryType " + deliveryType);
            if (wumartFill == 1) {
                JSONObject taskext = new JSONObject();
                taskext.put("warehouseCode", so.getWarehouseCode());
                taskext.put("warehouseName", so.getWarehouseName());
                taskext.put("newOrderCode", IdGenerator.getId());

                OfcTask taskWu = new OfcTask();
                taskWu.setRefId(Long.valueOf(billCode));
                taskWu.setType(OfcTaskType.SO_PUSH_DMG.getValue());
                taskWu.setStatus(OfcTaskStatus.NEW.getValue());
                taskWu.setVenderId(venderId);

                if (this.ofcTaskService.countTask(taskWu) <= 0 && redisTemplate.lock(MessageFormat.format(Constants.OFC_SO_DMG_LOCK, billCode), 10)) {
                    log.info("插入ofc下发so任务.订单号=" + billCode);
                    taskWu.setContent(taskext.toJSONString());
                    this.ofcTaskService.addTask(taskWu);
                }
            }
            if (wgFill == 1) {
                OfcTask holdInventoryByOpenApi = new OfcTask();
                holdInventoryByOpenApi.setRefId(Long.valueOf(billCode));
                holdInventoryByOpenApi.setType(OfcTaskType.SO_PUSH_ATP_API.getValue());
                holdInventoryByOpenApi.setStatus(OfcTaskStatus.NEW.getValue());
                holdInventoryByOpenApi.setVenderId(venderId);

                if (this.ofcTaskService.countTask(holdInventoryByOpenApi) <= 0) {
                    log.info("create job hold inventory by open api, so: " + billCode);
                    holdInventoryByOpenApi.setContent(new JSONObject().toJSONString());
                    this.ofcTaskService.addTask(holdInventoryByOpenApi);
                }
            }
            //插入OFC_BILL
            this.ofcBillService.insert(entity);
            //记录操作日志
            this.ofcOperateLogService.insert(billCode, BillType.SO, OfcOperateEnum.SO_CREATED, so.getSoCode() + ":" + so.getTotalSkuSupplyQty().toString(), venderId);
        } else if (SoStatus.CREATING.equals(update)) {
            //记录操作日志
            this.ofcOperateLogService.insert(billCode, BillType.SO, OfcOperateEnum.SO_CREATING, null, venderId);
        } else if (SoStatus.CREATE_FAIL.equals(update)) {
            //记录操作日志
            this.ofcOperateLogService.insert(billCode, BillType.SO, OfcOperateEnum.SO_CREATE_FAIL, null, venderId);
        }
        return ret;
    }


    private void addMergeTask(Long orderCode, Long venderId) {

        OfcTask task = new OfcTask();
        task.setRefId(orderCode);
        task.setType(OfcTaskType.SO_MERGE.getValue());
        task.setStatus(OfcTaskStatus.NEW.getValue());
        task.setVenderId(venderId);

        log.info("OfcTask task is " + JSON.toJSONString(task));
        if (this.ofcTaskService.countTask(task) <= 0 && redisTemplate.lock(MessageFormat.format(Constants.OFC_SO_LOCK, orderCode), 10)) {
            log.info("插入合单并下发订单任务.订单号=" + orderCode);
            task.setContent(orderCode.toString());
            this.ofcTaskService.addTask(task);
        }
    }

    private void addPushSo(Long orderCode, Long venderId) {

        OfcTask task = new OfcTask();
        task.setRefId(orderCode);
        task.setType(OfcTaskType.SO_PUSH_PSI.getValue());
        task.setStatus(OfcTaskStatus.NEW.getValue());
        task.setVenderId(venderId);
        if (this.ofcTaskService.countTask(task) <= 0 && redisTemplate.lock(MessageFormat.format(Constants.OFC_SO_PSI_LOCK, orderCode), 10)) {
            log.info("插入下发so psi任务.订单号=" + orderCode);
            task.setContent(orderCode.toString());
            this.ofcTaskService.addTask(task);
        }
    }

    /**
     * @param billCode
     * @param skuDeliverQty
     * @return
     * @throws BusinessException
     */
    @Transactional(rollbackFor = RuntimeException.class)
    @Override
    public int update4Deliver(String billCode, BigDecimal skuDeliverQty, Long venderId) throws BusinessException {
        if (billCode == null) {
            throw EC.ERROR.exception("【更新SO信息(发货)】参数错误！更新SO头主键为空！");
        }
        OfcSoHead entity = new OfcSoHead();
        entity.setTotalSkuDeliverQty(skuDeliverQty);
        entity.setSoStatus(SoStatus.DELIVERED.getValue());
        OfcSoHead filter = new OfcSoHead();
        filter.setSoBillCode(billCode);
        filter.setSoStatus(SoStatus.CREATED.getValue());
        int ret = this.ofcSoHeadDAO.updateByFilter(entity, filter);
        if (ret > 0) {
            //记录操作日志
            this.ofcOperateLogService.insert(billCode, BillType.SO, OfcOperateEnum.SO_DELIVERED, skuDeliverQty.toString(), venderId);
        }
        return ret;
    }

    /**
     * @param billCode
     * @param returnQtyMap
     * @return
     * @throws BusinessException
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public BigDecimal update4Return(String billCode, Map<Long, BigDecimal> returnQtyMap, Long venderId) throws BusinessException {
        if (billCode == null) {
            throw EC.ERROR.exception("【更新SO信息(返仓)】参数错误！更新SO头主键为空！");
        }

        BigDecimal sum = BigDecimal.ZERO;
        for (Map.Entry<Long, BigDecimal> entry : returnQtyMap.entrySet()) {
            BigDecimal qty = entry.getValue();
            int ret = this.ofcSoDetailDAO.update4Return(billCode, entry.getKey(), qty);
            if (ret != 1) {
                throw EC.ERROR.exception("【更新SO信息(返仓)】更新SO明细返仓数量失败！单号=" + billCode + "，ID=" + entry.getKey() + "，QTY=" + qty);
            }
            sum = sum.add(qty);
        }
        int ret = this.ofcSoHeadDAO.update4Return(billCode, sum);
        if (ret != 1) {
            throw EC.ERROR.exception("【更新SO信息(返仓)】更新SO返仓数量失败！单号=" + billCode + "，QTY=" + sum);
        }
        //记录操作日志
        this.ofcOperateLogService.insert(billCode, BillType.SO, OfcOperateEnum.SO_RETURN, sum.toString(), venderId);
        return sum;
    }

    /**
     * @param soStatusSet
     * @param timeInterval
     * @param offset
     * @param size
     * @return
     */
    @Override
    public List<OfcSoHead> fetchSoByStatusAndTimeStamp(Set<SoStatus> soStatusSet, int timeInterval, long offset, int size) {
        if (size <= 0) {
            return Collections.emptyList();
        }
        List<Integer> statuses = new ArrayList<>(soStatusSet.size());
        for (SoStatus soStatus : soStatusSet) {
            statuses.add(soStatus.getValue());
        }
        List<OfcSoHead> list = this.ofcSoHeadDAO.fetchSoByStatusAndTimeStamp(statuses, timeInterval, offset, size);
        return list;
    }

    /**
     * @param update
     * @param filter
     * @return
     * @throws BusinessException
     */
    @Transactional(rollbackFor = RuntimeException.class)
    @Override
    public int updateByFilter(OfcSoHead update, OfcSoHead filter) throws BusinessException {
        return ofcSoHeadDAO.updateByFilter(update, filter);
    }

    /**
     * @param billCode
     * @param items
     * @return
     * @throws BusinessException
     */
    @Override
    public Costs calcCost(String billCode, Map<String, BigDecimal> items) throws BusinessException {
        List<OfcSoDetail> details = this.findDtails(billCode);
        if (CollectionUtils.isEmpty(details)) {
            throw EC.SO_DETAILS_IS_EMPTY.exception("单号=" + billCode);
        }
        BigDecimal amountSum = BigDecimal.ZERO;
        BigDecimal ntAmountSum = BigDecimal.ZERO;
        for (OfcSoDetail detail : details) {
            String code = detail.getSkuSupplyCode();
            BigDecimal qty = items.get(code);
            if (qty == null) {
                continue;
            }
            if (qty.compareTo(BigDecimal.ZERO) == 0) {
                items.remove(code);
            }
            BigDecimal itAmount = detail.getSkuSupplyPrice().multiply(qty);
            BigDecimal ntAmount = itAmount.divide(BigDecimal.ONE.add(detail.getTaxRate()), 2, BigDecimal.ROUND_HALF_UP);
            amountSum = amountSum.add(itAmount);
            ntAmountSum = ntAmountSum.add(ntAmount);
        }
        amountSum = amountSum.setScale(2, BigDecimal.ROUND_HALF_UP);
        ntAmountSum = ntAmountSum.setScale(2, BigDecimal.ROUND_HALF_UP);
        return new Costs(amountSum, ntAmountSum);
    }

}
