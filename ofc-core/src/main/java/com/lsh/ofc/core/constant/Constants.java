package com.lsh.ofc.core.constant;

import java.math.BigDecimal;

/**
 * @author huangdong
 * @date 16/8/28
 */
public final class Constants {

    public static final String ATTR_ITEMS = "items";

    public static final String ATTR_CODE = "code";

    public static final String ATTR_QTY = "qty";

    public static final BigDecimal TAX_013 = BigDecimal.valueOf(0.13);

    public static final BigDecimal TAX_017 = BigDecimal.valueOf(0.17);

    public static final BigDecimal TAX_016 = BigDecimal.valueOf(0.16);

    public static final String OFC_BILL_SYSTEM_WUMART_SAP = "WUMART_SAP";

    public static final String OFC_BILL_SYSTEM_WUMART_SAP_JISHOU = "WUMART_SAP_JISHOU";

    public static final String KEY_MEIPI_CUSTOMER_CREATE_LOCK = "OFC_MEIPI_CUSTOMER_CREATE_LOCK";

    public static final String kEY_MEIPI_CUSTOMER_CODES = "OFC_MEIPI_CUSTOMER_CODES";

    public static final String kEY_ERROR_TASK_ALERT_TIME = "OFC_ERROR_TASK_ALERT_TIME";

    public static final String TOPIC_OBD = "OFC_BILL_SASS";

    public static final String TOPIC_OBD_STO = "OFC_BILL_STO_SASS";

    public static final String USER_ADDRESS_TRANS_LIMIT = "trans_limit";

    public static final String ORDER_H_MP_CUST_CODE = "mp_cust_code";

    public static final String ORDER_H_TRANS_TIME = "trans_time";

    public static final String ORDER_H_SUM_FLAG = "sum_flag";

    public static final String ORDER_H_SUM_SOID = "sumSoId";

    public static final String ORDER_D_DETAIL_ID = "detail_id";

    public static final String ORDER_D_TYPE = "type";

    public static final String ORDER_ITEM_WEIGHT_FLAG = "is_weighing_goods";

    public static final String ORDER_ITEM_BARCODE = "goods_barcode";

    public static final String ORDER_D_SKU_DEFINE = "sku_define";

    public static final String ORDER_D_SKU_PURCHASE_PRICE = "purchase_price";
    //第三方code值
    public static final String SO_H_REF_SO_CODE = "ref_so_code";

    public static final String SO_H_FULFILL_CREATE_TIME = "fulfill_create_time";

    public static final String SO_H_MP_CUST_CODE = ORDER_H_MP_CUST_CODE;

    public static final String SO_H_SUPPLIER_CODE = "supplier_code";

    public static final String SO_H_LGORT_TYPE = "lgort_type";

    public static final String SO_H_SUPPLY_MODEL = "supply_model";

    public static final String SO_H_TRANS_TIME = ORDER_H_TRANS_TIME;

    public static final String SO_D_TYPE = ORDER_D_TYPE;

    public static final String SO_D_OBD = "obd";

    public static final String OBD_H_BOX_NUM = "box_num";

    public static final String OBD_H_TURNOVER_BOX_NUM = "turnover_box_num";

    public static final String OBD_H_SCATTERED_BOX_NUM = "scattered_box_num";

    public static final String OBD_TOTAL_VOLUME = "total_volume";

    public static final String OBD_COLLECTION_LOCATION = "collection_location";

    public static final String OBD_H_DRIVER_INFO = "driver_info";

    public static final String OBD_H_VEHICLE_TYPE = "vehicle_type";

    public static final String OBD_H_VEHICLE_TYPE_DESC = "vehicle_type_desc";

    public static final String OBD_H_CARRIER_CODE = "carrier_code";

    public static final String OBD_H_CARRIER_NAME = "carrier_name";

    public static final String OBD_H_CREATE_TIME = "create_time";

    public static final String OBD_H_PICK_TIME = "pick_time";

    public static final String OBD_H_DELIVERY_TIME = "delivery_time";

    public static final String OBD_H_WAYBILL_CODE = "waybill_code";

    public static final String OBD_H_MP_CUST_CODE = SO_H_MP_CUST_CODE;

    public static final String OBD_D_PACK_NUM = "pack_num";

    public static final String OBD_D_BOX_NUM = "box_num";

    public static final String OBD_D_LEFT_EA_NUM = "left_ea_num";

    public static final String OBD_D_OBD_DETAIL_EXT = "obd_detail_ext";

    public static final String OBD_D_OBD_DETAIL_DEFINE = "sku_define";

    public static final String OBD_D_OBD_BARCODE = "barcode";

    public static final String OBD_D_OBD_PACK_CODE = "pack_code";

    public static final String OBD_D_OBD_PACKAGE_NUM = "package_num";

    public static final String OBD_D_OBD_PACKAGE_CODES = "package_codes";

    public static final String OBD_D_OBD_SCAN_PACKAGE_CODES = "scan_package_codes";

    public static final String OBD_D_OBD_SKU_NAME = "sku_name";

    public static final String OBD_D_OBD_ITEM_NO = "item_no";

    public static final String OBD_D_OBD_SKU_WEIGHT = "weight";

    public static final String OBD_D_OBD_TASK_MODEL = "task_model";

    public static final String OBD_D_OBD_GOODS_CODE = "goods_code";

    public static final String OBD_D_OBD_PRE_INFO = "pre_info";

    public static final String RETURN_H_BATCH = "batch";

    public static final String RETURN_ORDER_TYPE = "return_order_type";

    public static final String RO_H_REF_RO_CODE = "ref_ro_code";

    public static final String ORDER_DELIVERY_WAY = "delivery_way";

    public static final String ORDER_IS_IN_DMALL = "isInDmall";

    public static final String ORDER_PRE_WAREHOUSE_CODE = "pre_warehouse_code";

    public static final String OFC_STO_ORDER = "ofc:sto:order:";

    public static final String OFC_MERGE_STO_ORDER = "psi:sto:order:so:{0}:{1}:{2}:{3}:{4}";

    public static final String OFC_STO_PUST_STATUS = "psi:sto:push:status:{0}:{1}:{2}";

    public static final String OFC_STO_PUST_PSI_STATUS = "psi:sto:order:push:{0}";

    public static final String OFC_STO_PUST_PSI_ORDERS = "ofc:sto:push:psi:orders";

    public static final String ORDER_PRE_WAREHOUSE_NAME = "pre_warehouse_name";

    public static final String RO_H_FULFILL_CREATE_TIME = "fulfill_create_time";

    public static final String KEY_SO_ES_ID_OFFSET = "OFC_SO_ES_ID_OFFSET";

//    public static final String REDIS_HASH = "OFC_REDIS_HASH";

//    public static final String WUMART_OFC_SWITCH = "OFC_WUMART_OFC_SWITCH";

    public static final String ORDER_D_SKU_PACK_UNIT = "pack_unit";

    public static final String ORDER_D_SKU_PACK_NAME = "pack_name";

    /**
     * 2018-08-10 云仓
     * {0} 区域
     * {1} dc         仓库
     * {2} supplyId   货主
     * {3} org        机构
     */
//    public static final String OFC_SUPPLIER_PREFIX = "OFC_SUPPLIER_{0}_{1}_{2}_{3}";

//    public static final String OFC_SUPPLIER_CONFIG_LOCK = "OFC_SUPPLIER_CONFIG_LOCK";

    public static final String OFC_SUPPLIER_CONFIG_UPDATE_LOCK = "OFC_SUPPLIER_CONFIG_UPDATE_LOCK_{0}";

    /**
     * {0}    区域
     * {1}    机构
     * {2}    货主
     */
    public static final String OFC_SUPPLIER_CONFIG = "OFC_SUPPLIER_CONFIG_{0}_{1}_{2}";

    public static final String OFC_SUPPLIER_CUS_CONFIG = "OFC_SUPPLIER_CUS_CONFIG_{0}_{1}";

    public static final String OFC_SO_WUMART_CALLBACK_SUPPLY_PRICE = "price";

    public static final String OFC_OBD_LOCK = "OFC_OBD_LOCK_{0}";

    public static final String OFC_SO_LOCK = "OFC_SO_LOCK_{0}";

    public static final String OFC_SO_DMG_LOCK = "OFC_SO_DMG_LOCK_{0}";

    public static final String OFC_SO_PSI_LOCK = "OFC_SO_PSI_LOCK_{0}";

    public static final String OFC_OBD_PSI_LOCK = "OFC_OBD_PSI_LOCK_{0}";

    public static final String OFC_SO_SUM_LOCK = "OFC_SO_SUM_LOCK_{0}";

    public static final String OFC_SO_SUM_KEY = "OFC_SO_SUM_KEY_{0}";

    public static final String OFC_SO_OBD_SUM_KEY = "OFC_SO_OBD_SUM_KEY";

    public static String OBD_HEAD_EXT_PREFIX = "ofc:obd:head:ext";

    /**
     * 商店街，用户组
     */
    public static final String STORE_STREET_USER_GROUP_IDS = "OFC_STORE_STREET_USER_GROUP_IDS";
    public static final String USER_GROUP_IDS = "user_group_ids";

    public static final Long DC10_ADDRESSCODE = 201903280001L;

    public static final String WUMART_OFC_TOTAL_SO = "WUMART_OFC_TOTAL_SO_{0}";
    public static final String WUMARTFILL = "wumartFill";
    /**
     * 物流模式
     */
    public static final String OFC_ORDER_LOGISTICS_MODE = "logisticsMode";

    public static final String MIDDLE_LINE = "-";

    public static final String OBD_BOX_INFO_TO_SC = "box:obd:info:to:sc:";

    public static final String OBD_BOX_INFO_FROM_SC = "box:obd:info:from:sc:";

    public static final String OBD_D_OBD_RECEIPT_FLAG = "receipt_flag";


    /**
     * 汇总类型
     */

    public static final String TYPE_SUMMARY_PSI_STO = "1";

    public static final String TYPE_SUMMARY_SC_IBD = "2";

}
