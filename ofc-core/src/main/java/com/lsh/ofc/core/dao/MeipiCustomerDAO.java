package com.lsh.ofc.core.dao;

import com.lsh.ofc.core.base.BaseDAO;
import com.lsh.ofc.core.entity.MeipiCustomer;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

/**
 * 美批客户信息DAO
 *
 * @author huangdong
 * @date 16/10/1
 */
@Repository
public interface MeipiCustomerDAO extends BaseDAO<MeipiCustomer, Long> {

    /**
     * 根据时间戳,状态获取指定数量的
     *
     * @param cusType
     * @return
     */
    MeipiCustomer getMaxMeipiCodeByType(@Param("cusType") Integer cusType);
}
