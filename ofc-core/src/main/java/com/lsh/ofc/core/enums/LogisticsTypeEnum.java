package com.lsh.ofc.core.enums;

import com.lsh.ofc.core.handler.logistics.*;

/**
 * Project Name: LogisticsType
 * 北京链商电子商务有限公司
 * @author peter
 * Date: 19/2/26
 * Package Name: com.lsh.ofc.core.enums
 * Description: 物流模式
 */
public enum LogisticsTypeEnum {
    /**
     * DC直送：由LS仓库直接发货，配送
     */
    DC(1, "DC直送", DcLogisticsHandler.class),

    /**
     * 供商直配（LS提供平台）
     */
    // SUPPLY(2, "供商直配", PreLogisticsHandler.class),

    /**
     * 云仓-DC直流：供商将商品运送到LS仓库，使用LS的物流配送
     */
    CLOUD_DC_2_DC(3, "云仓-DC直流", CloudDcLogisticsHandler.class),

    /**
     * 前置仓配送：由门店配送到客户
     * TODO：该业务在2019-03-07暂停
     */
    PRE_WAREHOUSE(4, "前置仓配送", PreWarehouseLogisticsHandler.class),

    /**
     * 物美自配
     * 例如：LS05/LS06订单
     */
    WUMART(5, "物美自配", WumartLogisticsHandler.class),

    /**
     * 文固/链商供商自配：
     * 例如：DCx01/DCx30订单
     */
    LS_OR_WG_SUPPLY(6, "供商自配", LshWgSupplyLogisticsHandler.class),

    /**
     * 仓库配送前置仓（自定义类型）：由LS仓库，配送到门店；
     * 例如：STO（前置仓）TODO：该业务在2019-03-07暂停
     */
    DC_2_PRE_WAREHOUSE(20, "仓库配送前置仓", PreLogisticsHandler.class),

    /**
     * PC供给,DC30（自定义类型）：猪肉供给
     */
    WUMART_PC(25, "PC供给,DC30", PcLogisticsHandler.class),

    /**
     * 不配送（自定义类型）：内部不做任何处理
     */
    No_Logistics(99, "不配送", NoLogisticsHandler.class);

    private Integer type;
    private String desc;
    private Class<? extends AbstractLogisticsHandler> logisticsHandler;

    LogisticsTypeEnum(Integer type, String desc, Class<? extends AbstractLogisticsHandler> logisticsHandler) {
        this.type = type;
        this.desc = desc;
        this.logisticsHandler = logisticsHandler;
    }

    public Integer getType() {
        return type;
    }

    public String getDesc() {
        return desc;
    }

    public Class<? extends AbstractLogisticsHandler> getLogisticsHandler() {
        return logisticsHandler;
    }

    public static final LogisticsTypeEnum valueOf(Integer type) {
        for (LogisticsTypeEnum item : LogisticsTypeEnum.values()) {
            if (item.type.equals(type)) {
                return item;
            }
        }

        return null;
    }

}
