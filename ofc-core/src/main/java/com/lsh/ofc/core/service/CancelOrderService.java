package com.lsh.ofc.core.service;

import com.lsh.base.common.exception.BusinessException;
import com.lsh.ofc.core.entity.OfcSoHead;

import java.util.List;

/**
 *
 * @author panxudong
 * @date 17/3/28
 */
public interface CancelOrderService {

    /**
     * 取消订单
     * @param list  取消订单列表
     * @return
     * @throws BusinessException
     */
    boolean cancelOrder(List<OfcSoHead> list) throws BusinessException;

}
