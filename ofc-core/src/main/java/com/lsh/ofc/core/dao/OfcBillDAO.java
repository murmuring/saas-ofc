package com.lsh.ofc.core.dao;

import com.lsh.ofc.core.base.BaseDAO;
import com.lsh.ofc.core.entity.OfcBill;
import org.springframework.stereotype.Repository;

/**
 * OFC单据DAO
 *
 * @author huangdong
 * @date 16/9/20
 */
@Repository
public interface OfcBillDAO extends BaseDAO<OfcBill, Long> {
}
