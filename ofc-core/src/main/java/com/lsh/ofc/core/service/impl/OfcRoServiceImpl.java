package com.lsh.ofc.core.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.lsh.base.common.exception.BusinessException;
import com.lsh.base.common.model.CommonResult;
import com.lsh.ofc.core.constant.Constants;
import com.lsh.ofc.core.dao.OfcRoDetailDAO;
import com.lsh.ofc.core.dao.OfcRoHeadDAO;
import com.lsh.ofc.core.entity.OfcRoDetail;
import com.lsh.ofc.core.entity.OfcRoHead;
import com.lsh.ofc.core.entity.OfcSoHead;
import com.lsh.ofc.core.enums.*;
import com.lsh.ofc.core.exception.EC;
import com.lsh.ofc.core.proxy.service.WumartBasicService;
import com.lsh.ofc.core.proxy.service.WumartOfcServiceProxy;
import com.lsh.ofc.core.proxy.service.WumartSapServiceProxy;
import com.lsh.ofc.core.redis.RedisTemplate;
import com.lsh.ofc.core.service.*;
import com.lsh.ofc.core.util.IdGenerator;
import com.lsh.ofc.core.util.OFCUtils;
import com.lsh.ofc.proxy.context.WumartBasicContext;
import com.lsh.ofc.proxy.model.ObdHead;
import com.lsh.ofc.proxy.service.WgServiceProxy;
import com.lsh.ofc.proxy.service.WmsServiceProxy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.util.*;

/**
 * @author peter
 */
@Service
public class OfcRoServiceImpl implements OfcRoService {

    private final Logger logger = LoggerFactory.getLogger(OfcRoServiceImpl.class);

    @Autowired
    private OfcRoHeadDAO ofcRoHeadDAO;

    @Autowired
    private OfcRoDetailDAO ofcRoDetailDAO;

    @Autowired
    private OfcSoService ofcSoService;

    @Autowired
    private OfcRoCreateService ofcRoCreateService;

    @Autowired
    private OfcBillService ofcBillService;

    @Autowired
    private WmsServiceProxy wmsServiceProxy;

    @Autowired
    private WgServiceProxy wgServiceProxy;

    @Autowired
    private WumartSapServiceProxy wumartSapServiceProxy;

    @Autowired
    private WumartOfcServiceProxy wumartOfcServiceProxy;

    @Autowired
    private OfcOperateLogService ofcOperateLogService;

    @Autowired
    private WumartBasicService wumartBasicService;

    @Autowired
    private RedisTemplate redisTemplate;

    @Override
    public int count(OfcRoHead filter) throws BusinessException {
        return this.ofcRoHeadDAO.count(filter);
    }

    @Override
    public List<OfcRoHead> findList(OfcRoHead filter, boolean fillDetails) throws BusinessException {
        List<OfcRoHead> ros = this.ofcRoHeadDAO.findList(filter);
        if (CollectionUtils.isEmpty(ros)) {
            return Collections.emptyList();
        }
        for (OfcRoHead ro : ros) {
            OfcRoDetail param = new OfcRoDetail();
            param.setRoBillCode(ro.getRoBillCode());
            if (fillDetails) {
                ro.setDetails(this.ofcRoDetailDAO.findList(param));
            }
        }
        return ros;
    }

    /**
     * 查询SO信息R
     *
     * @param filter
     * @param fillDetails
     * @return
     * @throws BusinessException
     */
    @Override
    public OfcRoHead findOne(OfcRoHead filter, boolean fillDetails) throws BusinessException {
        OfcRoHead ro = this.ofcRoHeadDAO.findOne(filter);
        if (ro == null) {
            return null;
        }

        OfcRoDetail param = new OfcRoDetail();
        param.setRoBillCode(ro.getRoBillCode());
        if (fillDetails) {
            ro.setDetails(this.ofcRoDetailDAO.findList(param));
        }

        return ro;
    }

    @Transactional
    @Override
    public int insert(List<OfcRoHead> ros) throws BusinessException {
        if (CollectionUtils.isEmpty(ros)) {
            return 0;
        }

        int cnt = 0;
        boolean isAllConsign = true;
        int ts = OFCUtils.currentTime();
        for (OfcRoHead ro : ros) {
            if (ro == null) {
                continue;
            }
            List<OfcRoDetail> details = ro.getDetails();
            if (CollectionUtils.isEmpty(details)) {
                continue;
            }
            //TODO:目前非WMS履约的寄售不提交
            if (SupplierOrg.isConsign(ro.getSupplierOrg()) && !FulfillWms.LSH.getValue().equals(ro.getFulfillWms())) {
                ro.setRoStatus(RoStatus.IGNORED.getValue());
            } else {
                isAllConsign = false;
            }
            //TODO: 物美SAP限制最多20位，目前一单中相同货主不会拆成两单
//            String billCode = new StringBuilder(20).append(ro.getReturnCode()).append(ro.getSupplierOrg()).toString();

            String billCode = IdGenerator.getId();

            ro.setRoBillCode(billCode);
            ro.setCreateTime(ts);
            ro.setUpdateTime(ts);
            ro.setValid(Valid.enable.getValue());
            cnt += this.ofcRoHeadDAO.insert(ro);
            for (OfcRoDetail detail : ro.getDetails()) {
                detail.setRoBillCode(billCode);
                detail.setCreateTime(ts);
                detail.setUpdateTime(ts);
                this.ofcRoDetailDAO.insert(detail);
            }
            //记录操作日志
            this.ofcOperateLogService.insert(billCode, BillType.RO, OfcOperateEnum.RO_UNCREATED, ro.getTotalSkuReturnQty().toString(), ro.getVenderId());
        }
        if (isAllConsign) {
            throw EC.ERROR.exception("没有非寄售品，无法提交物美SO");
        }
        return cnt;
    }

    @Transactional
    @Override
    public int updateStatus(OfcRoHead filter, OfcRoHead entity) throws BusinessException {
        if (filter == null || entity == null) {
            return 0;
        }
        if (filter.getId() == null && !StringUtils.hasText(filter.getRoBillCode())) {
            return 0;
        }
        RoStatus expectStatus = RoStatus.valueOf(filter.getRoStatus());
        RoStatus updateStatus = RoStatus.valueOf(entity.getRoStatus());
        if (expectStatus == null || updateStatus == null) {
            return 0;
        }
        if (expectStatus.equals(updateStatus)) {
            return 0;
        }
        int ret = this.ofcRoHeadDAO.updateByFilter(entity, filter);
        if (ret < 1) {
            return ret;
        }
        if (!CollectionUtils.isEmpty(entity.getDetails())) {
            for (OfcRoDetail detail : entity.getDetails()) {
                this.ofcRoDetailDAO.update(detail);
            }
        }

        OfcRoHead param = new OfcRoHead();
        param.setId(filter.getId());
        param.setRoBillCode(filter.getRoBillCode());
        entity = this.ofcRoHeadDAO.findOne(param);
        switch (updateStatus) {
            case CREATED:
                this.ofcBillService.insert(entity);
                //记录操作日志
                this.ofcOperateLogService.insert(entity.getRoBillCode(), BillType.RO, OfcOperateEnum.RO_CREATED, entity.getRoCode(), entity.getVenderId());
                break;
            case CREATING:
                //记录操作日志
                this.ofcOperateLogService.insert(entity.getRoBillCode(), BillType.RO, OfcOperateEnum.RO_CREATING, null, entity.getVenderId());
                break;
            case CREATE_FAIL:
                //记录操作日志
                this.ofcOperateLogService.insert(entity.getRoBillCode(), BillType.RO, OfcOperateEnum.RO_CREATE_FAIL, null, entity.getVenderId());
                break;
            case COMPLETED:
                //记录操作日志
                this.ofcOperateLogService.insert(entity.getRoBillCode(), BillType.RO, OfcOperateEnum.RO_COMPLETED, null, entity.getVenderId());
                break;
            default:
                break;
        }
        return ret;
    }

    @Override
    public Integer refreshRoStatus(OfcRoHead ro) throws BusinessException {
        Integer roStatus = ro.getRoStatus();
        Integer fulfillChannel = ro.getFulfillChannel();
        Integer fulfillWms = ro.getFulfillWms();
        boolean completed = false;
        logger.info(ro.getReturnCode() + " returnCode 查询第三方状态 开始");
        try {
            //通过物美OFC履约
            if (FulfillChannel.WUMART_OFC.getValue().equals(fulfillChannel)) {
                boolean created = RoStatus.CREATED.getValue().equals(roStatus);
                if (RoStatus.CREATING.getValue().equals(roStatus) || (created && FulfillWms.Wumart.getValue().equals(fulfillWms))) {
                    OfcSoHead filter = new OfcSoHead();
                    filter.setSoBillCode(ro.getSoBillCode());
                    OfcSoHead soHead = ofcSoService.findOne(filter, false);
                    String businessId = ro.getSoBillCode();
                    if (null != soHead) {
                        JSONObject soExt = JSON.parseObject(soHead.getExt());
                        if (null != soExt) {
                            String sumFlag = soExt.getString(Constants.ORDER_H_SUM_FLAG);
                            String sumSoId = soExt.getString(Constants.ORDER_H_SUM_SOID);

                            if (!StringUtils.isEmpty(sumFlag) && sumFlag.equals(SumFlag.SUM.getCode())) {
                                businessId = sumSoId;
                            }
                        }
                    }

                    if (StringUtils.isEmpty(businessId)) {
                        businessId = ro.getSoBillCode();
                    }

                    completed = this.isReturnedToWumart(businessId, ro.getId(), created, ro.getRegionCode(), ro.getSupplierCode(), ro.getSupplierOrg(), ro.getSupplierId(),ro.getSupplierGroup());
                    if (completed) {
                        roStatus = RoStatus.COMPLETED.getValue();
                    } else {
                        roStatus = this.ofcRoHeadDAO.get(ro.getId()).getRoStatus();
                    }
                }

                if (RoStatus.CREATED.getValue().equals(roStatus) && FulfillWms.LSH.getValue().equals(fulfillWms)) {

                    String wmsPath = this.wumartBasicService.getWmsPath(WumartBasicContext.buildContext(ro.getRegionCode(), ro.getSupplierCode(), ro.getSupplierOrg(), ro.getSupplierId(), ro.getSupplierGroup()));
                    completed = this.wmsServiceProxy.isReturnCompleted(ro.getRoCode(), wmsPath);
                    if (completed) {
                        roStatus = RoStatus.COMPLETED.getValue();
                    }
                }
                //通过物美SAP履约
            } else if (FulfillChannel.WUMART_SAP.getValue().equals(fulfillChannel)) {
                //仅创建完成的需要刷新状态
                if (!RoStatus.CREATED.getValue().equals(roStatus)) {
                    return roStatus;
                }
                boolean ret = this.isReturnedToWumart(ro.getRoCode(), ro.getRegionCode());
                if (ret) {
                    roStatus = RoStatus.COMPLETED.getValue();
                }
                //通过链商WMS履约
            } else if (FulfillChannel.LSH_WMS.getValue().equals(fulfillChannel)) {
                //仅创建完成的需要刷新状态
                if (!RoStatus.CREATED.getValue().equals(roStatus)) {
                    return roStatus;
                }

                String wmsPath = this.wumartBasicService.getWmsPath(WumartBasicContext.buildContext(ro.getRegionCode(), ro.getSupplierCode(), ro.getSupplierOrg(), ro.getSupplierId(), ro.getSupplierGroup()));
                completed = this.wmsServiceProxy.isReturnCompleted(ro.getRoCode(), wmsPath);
                if (completed) {
                    roStatus = RoStatus.COMPLETED.getValue();
                }
                //通过链商WG履约
            } else if (FulfillChannel.SAAS_PSI.getValue().equals(fulfillChannel) ||
                    FulfillChannel.SAAS_SC.getValue().equals(fulfillChannel) ||
                    FulfillChannel.SAAS_OFC.getValue().equals(fulfillChannel)) {
                //仅创建完成的需要刷新状态
                logger.info(ro.getReturnCode() + " returnCode 查询第三方状态 开始 LSH_WG");
                if (!RoStatus.CREATED.getValue().equals(roStatus)) {
                    return roStatus;
                }

                String wgPath = this.wumartBasicService.getWmsPath(WumartBasicContext.buildContext(ro.getRegionCode(), ro.getSupplierCode(), ro.getSupplierOrg(), ro.getSupplierId(), ro.getSupplierGroup()));
                Map<String, Integer> completedMap = this.wgServiceProxy.isReturnCompleted(Collections.singletonList(ro.getRoCode()), wgPath, ro.getVenderId());

                int flag = 0;
                int supplierId = 0;
                if (completedMap != null && completedMap.get(ro.getRoCode()) != null) {
                    flag = completedMap.get(ro.getRoCode());
                    supplierId = ro.getSupplierId();
                }

                if (flag == 1) {
                    roStatus = RoStatus.COMPLETED.getValue();
                    completed = true;
                }
//                else if (flag == 2 && supplierId != 0) {
//                    roStatus = RoStatus.COMPLETED.getValue();
//                    completed = true;
//                }
            }

        } catch (Throwable e) {
            if (e instanceof BusinessException) {
                throw e;
            }
            logger.info("【" + ro.getReturnCode() + "】【" + ro.getRoBillCode() + "】查询返仓结果异常...RO单号=" + ro.getRoCode() + " ..." + e.getMessage(), e);
            throw new BusinessException(EC.ERROR.getCode(), e.getMessage(), e);
        }

        if (completed && roStatus.compareTo(RoStatus.COMPLETED.getValue()) >= 0) {
            OfcRoHead filter = new OfcRoHead();
            filter.setId(ro.getId());
            filter.setRoBillCode(ro.getRoBillCode());
            filter.setRoStatus(RoStatus.CREATED.getValue());
            OfcRoHead update = new OfcRoHead();
            update.setRoStatus(roStatus);
            logger.info("filter :" + JSON.toJSONString(filter) + "  update :" + JSON.toJSONString(update));
            int status = this.updateStatus(filter, update);
            logger.info("ro updateStatus : {}", status);
        }

        return roStatus;
    }

    @Override
    public List<OfcRoHead> fetchRoByStatusAndTimeStamp(Set<RoStatus> roStatusSet, int timeInterval, long offset, int size) {
        if (size <= 0) {
            return Collections.emptyList();
        }
        List<Integer> statuses = new ArrayList<>(roStatusSet.size());
        for (RoStatus roStatus : roStatusSet) {
            statuses.add(roStatus.getValue());
        }
        List<OfcRoHead> list = this.ofcRoHeadDAO.fetchRoByStatusAndTimeStamp(statuses, timeInterval, offset, size);
        return list;
    }

    /**
     * 物美返仓是否已完成（物美SAP）
     *
     * @param roCode
     * @param regionCode
     * @return
     * @throws BusinessException
     */
    private boolean isReturnedToWumart(String roCode, Integer regionCode) throws BusinessException {
        logger.info("查询物美返仓结果开始，RO单号=" + roCode);
        ObdHead ret = this.wumartSapServiceProxy.queryObdStatus4Ro(roCode, regionCode);
        logger.info("查询物美返仓结果完成，RO单号=" + roCode + ", ret=" + ret);
        if (ret == null || CollectionUtils.isEmpty(ret.getDetails())) {
            logger.info("退货OBD记录已存在！退货单号=" + roCode);
            return false;
        }
        return true;
    }

    /**
     * 物美返仓是否已完成（物美OFC）
     *
     * @param billCode
     * @param batchNo
     * @param created
     * @return
     * @throws BusinessException
     */
    private boolean isReturnedToWumart(String billCode, Long batchNo, boolean created, Integer regionCode, String supplierCode, Integer supplierOrg, Integer supplierId, String supplierGroup) throws BusinessException {
        logger.info("【物美OFC】查询物美返仓结果开始，单号=" + billCode + "，批次号=" + batchNo);
        String content = this.wumartOfcServiceProxy.queryMeipiOrder(WumartOfcServiceProxy.OrderType.RETURN, billCode, batchNo, WumartBasicContext.buildContext(regionCode, supplierCode, supplierOrg, supplierId, supplierGroup));
        logger.info("【物美OFC】查询物美返仓结果开始，单号=" + billCode + "，批次号=" + batchNo + ", content=" + content);
        JSONObject json = JSON.parseObject(content);
        logger.info("【物美OFC】查询物美返仓结果开始，单号=" + billCode + "，批次号=" + batchNo + ", created=" + created);
        if (!created) {
            JSONObject soInfo = json.getJSONObject("soInfo");
            if (soInfo == null) {
                return false;
            }
            CommonResult<Boolean> ret = this.ofcRoCreateService.callback(soInfo);
            if (ret == null || !Boolean.TRUE.equals(ret.getData())) {
                return false;
            }

        }

        JSONObject obdInfo = json.getJSONObject("obdInfo");
        if (obdInfo == null) {
            return false;
        }
        if (CollectionUtils.isEmpty(obdInfo.getJSONArray("details"))) {
            return false;
        }

        String soCode = obdInfo.getString("soCode");
        if (!StringUtils.isEmpty(soCode)) {
            redisTemplate.set("ofc:sumSo:obdInfo:" + soCode, obdInfo.toJSONString(), 30000000);
        }

        return true;
    }
}
