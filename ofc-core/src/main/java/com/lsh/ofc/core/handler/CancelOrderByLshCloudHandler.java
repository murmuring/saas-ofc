package com.lsh.ofc.core.handler;

import com.lsh.base.common.exception.BusinessException;
import com.lsh.ofc.core.entity.OfcSoHead;
import com.lsh.ofc.core.service.impl.AbstractCancelOrderService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * @author miaozhuang
 * @date 18/3/28
 */
@Slf4j
@Service("cancelOrderByLshCloudHandler")
public class CancelOrderByLshCloudHandler extends AbstractCancelOrderService {

    @Override
    public boolean cancelOrder(List<OfcSoHead> list) throws BusinessException {
        this.buildRequest(list);
        log.info("LSx01 - DC43，订单取消，orderCode：{}", list.get(0).getOrderCode());
        return true;
    }

    @Override
    protected List<String> buildRequest(List<OfcSoHead> list) {
        List<String> requestList = new ArrayList<>();

        return requestList;
    }
}
