package com.lsh.ofc.core.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.math.BigDecimal;

/**
 * Project Name: lsh-ofc
 *
 * @author peter
 * @date 19/3/27
 * 北京链商电子商务有限公司
 * Package
 * desc:
 */
@Getter
@Setter
@NoArgsConstructor
@ToString
public class DmgSoSo {
    /**
     * 汇总soid
     */
    private String sumSoId;
    /**
     * 链商so id
     */
    private String soId;
    /**
     * 物美SAP soid
     */
    private String sapId;
    /**
     * 客户号
     */
    private String customerNumber;
    /**
     * 链商ofc 行项目
     */
    private Integer soOrderItem;
    /**
     * 物美ofc 汇总so 行项目
     */
    private Integer sumSoOrderItem;
    /**
     * 物美码
     */
    private Integer materialNo;
    /**
     * qty
     */
    private BigDecimal quantity;
    /**
     * 创建时间
     */
    private Long soCreated;
    /**
     * 大区号
     */
    private String mandt;
    /**
     * 预占数量
     */
    private BigDecimal occupy;

    /**
     * 价格
     */
    private BigDecimal price;
}
