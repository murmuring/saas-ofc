package com.lsh.ofc.core.service;

import com.lsh.base.common.exception.BusinessException;

public interface Order4PCService {

    void createOrder(String content) throws BusinessException;
}
