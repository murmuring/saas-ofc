package com.lsh.ofc.core.service.impl;

import com.alibaba.fastjson.JSON;
import com.lsh.base.common.exception.BusinessException;
import com.lsh.base.common.model.CommonResult;
import com.lsh.base.common.utils.CollectionUtils;
import com.lsh.ofc.core.dao.OfcSupplierDAO;
import com.lsh.ofc.core.entity.OfcSupplier;
import com.lsh.ofc.core.service.SupplierConfigService;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class SupplierConfigServiceImpl implements SupplierConfigService {

    private static final Logger LOGGER = LoggerFactory.getLogger(SupplierConfigServiceImpl.class);

    @Resource
    private OfcSupplierDAO dao;

    @Override
    public OfcSupplier findSupplier(Integer supplierId,
                                    String supplierCode,
                                    String supplierGroup,
                                    Integer supplierOrg,
                                    Integer regionCode,
                                    String supplierDc) {
        List<OfcSupplier> result = dao.find(supplierCode, supplierOrg, supplierId, supplierGroup, regionCode, supplierDc);
        LOGGER.info("find supplier: {} by supplier id: {}, supplier code: {}, supplier group: {}, supplier org: {}, region code: {}, supplier dc: {}",
                new Object[]{JSON.toJSONString(result), supplierId, supplierCode, supplierGroup, supplierOrg, regionCode, supplierDc});
        return CollectionUtils.isNotEmpty(result) ? result.get(0) : null;
    }

    @Override
    public OfcSupplier findSupplier(Integer supplierId,
                                    String supplierCode,
                                    String supplierGroup,
                                    Integer supplierOrg,
                                    Integer regionCode) {
        return findSupplier(supplierId, supplierCode, supplierGroup, supplierOrg, regionCode, null);
    }

    @Override
    public Map<String, Object> findSupplierAndGetFulfillConfig(Integer regionCode, String supplierDc, Integer supplierOrg, Integer supplierId) {
        OfcSupplier supplier = findSupplier(supplierId, null, null, supplierOrg, regionCode, supplierDc);
        if (supplier == null) {
            throw new BusinessException(CommonResult.ERROR, "can not get any config");
        }
        return JSON.parseObject(supplier.getConfig(), HashMap.class);
    }

    @Override
    public String getUsrInFulfillConfigOfSupplier(Integer regionCode, String supplierDc, Integer supplierOrg, Integer supplierId) {
        Map<String, Object> config = findSupplierAndGetFulfillConfig(regionCode, supplierDc, supplierOrg, supplierId);
        String result = (config != null) ? (String) config.get("usr") : null;
        if (StringUtils.isBlank(result)) {
            throw new BusinessException(CommonResult.ERROR, "can not get any config");
        }
        return result;
    }

    @Override
    public int save(OfcSupplier supplier) {
        return dao.insert(supplier);
    }
}
