package com.lsh.ofc.core.dao;

import com.lsh.ofc.core.base.BaseDAO;
import com.lsh.ofc.core.entity.OfcOrderHead;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.util.List;

/**
 * OFC订单头DAO
 *
 * @author huangdong
 * @date 16/8/1
 */
@Repository
public interface OfcOrderHeadDAO extends BaseDAO<OfcOrderHead, Long> {

    /**
     * 更新返仓数量（增量）
     *
     * @param orderCode
     * @param skuReturnQty
     * @return
     */
    int update4Return(@Param("orderCode") Long orderCode, @Param("skuReturnQty") BigDecimal skuReturnQty);

    List<OfcOrderHead> findListFilterByTime(@Param("startTime") Integer startTime,
                                            @Param("endTime") Integer endTime,
                                            @Param("regionCode") Integer regionCode,
                                            @Param("distributionWay") Integer distributionWay);
}
