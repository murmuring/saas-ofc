package com.lsh.ofc.core.entity;

import com.alibaba.fastjson.JSON;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * OFC SO单据头
 *
 * @author huangdong
 * @date 16/9/9
 */
@Setter
@Getter
@NoArgsConstructor
public class OfcSoHead extends Vender implements Serializable {

    private static final long serialVersionUID = 1596448451568743910L;
    /**
     * ID
     */
    private Long id;

    /**
     * SO单据号
     */
    private String soBillCode;

    /**
     * 订单号
     */
    private Long orderCode;

    /**
     * 下单时间
     */
    private Integer orderTime;

    /**
     * 地域编号
     */
    private Integer regionCode;

    /**
     * 地址编号
     */
    private Long addressCode;

    /**
     * 仓库编号
     */
    private String warehouseCode;

    /**
     * 仓库名称
     */
    private String warehouseName;

    /**
     * 供货商ID
     */
    private Integer supplierId;

    private String supplierCode;

    private String supplierGroup;

    /**
     * 供货商DC
     */
    private String supplierDc;

    /**
     * 供货商机构
     */
    private Integer supplierOrg;

    /**
     * 履约WMS
     */
    private Integer fulfillWms;

    /**
     * 履约渠道
     */
    private Integer fulfillChannel;

    /**
     * 履约SO单号
     */
    private String soCode;

    /**
     * SO状态
     */
    private Integer soStatus;

    /**
     * SKU下单总数
     */
    private BigDecimal totalSkuOrderQty;

    /**
     * SKU供货总数
     */
    private BigDecimal totalSkuSupplyQty;

    /**
     * SKU发货总数
     */
    private BigDecimal totalSkuDeliverQty;

    /**
     * SKU退货总数
     */
    private BigDecimal totalSkuReturnQty;

    /**
     * 其它信息
     */
    private String ext;

    /**
     * 创建时间
     */
    private Integer createTime;

    /**
     * 更新时间
     */
    private Integer updateTime;

    /**
     * 是否有效（0:无效；1:有效）
     */
    private Integer valid;
    /**
     * 二次配送标记
     */
    private Integer secondDistributionFlag;
    /**
     * 前置仓code
     */
    private String preWarehouseCode;
    /**
     * 配送方式
     */
    private Integer distributionWay;
    /**
     * 配送类型
     */
    private String orderDistributionType;
    /**
     * SO单据明细列表
     */
    private String pushPsiFlag;

    private List<OfcSoDetail> details;

    public Map<String, Object> extendedProperties() {
        return JSON.parseObject(this.ext);
    }
}
