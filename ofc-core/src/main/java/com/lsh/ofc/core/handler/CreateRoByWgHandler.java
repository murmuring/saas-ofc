package com.lsh.ofc.core.handler;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.lsh.base.common.exception.BusinessException;
import com.lsh.ofc.core.constant.Constants;
import com.lsh.ofc.core.entity.OfcCustomer;
import com.lsh.ofc.core.entity.OfcRoHead;
import com.lsh.ofc.core.entity.OfcSupplier;
import com.lsh.ofc.core.enums.DistributionWay;
import com.lsh.ofc.core.enums.RoStatus;
import com.lsh.ofc.core.exception.EC;
import com.lsh.ofc.core.proxy.service.WumartBasicService;
import com.lsh.ofc.core.service.SupplierConfigService;
import com.lsh.ofc.core.util.OFCUtils;
import com.lsh.ofc.proxy.context.WumartBasicContext;
import com.lsh.ofc.proxy.model.CreateWgRoReqHead;
import com.lsh.ofc.proxy.model.CreateWgSoRespHead;
import com.lsh.ofc.proxy.service.WgServiceProxy;
import org.springframework.context.ApplicationContext;

/**
 * 【文固】创建返仓处理Handler
 *
 * @author miaozhuang
 * @date 16/9/9
 */
public class CreateRoByWgHandler extends CreateRoHandler {

    private final WumartBasicService wumartBasicService;

    private final WgServiceProxy wgServiceProxy;

    private final SupplierConfigService supplierConfigService;

    protected CreateRoByWgHandler(final ApplicationContext context, final OfcRoHead ro, final OfcCustomer customer) {
        super(context, ro, customer);
        this.wumartBasicService = context.getBean(WumartBasicService.class);
        this.wgServiceProxy = context.getBean(WgServiceProxy.class);
        this.supplierConfigService = context.getBean(SupplierConfigService.class);
    }

    @Override
    protected boolean process(final OfcRoHead ro, final OfcCustomer customer) throws BusinessException {

        JSONObject roExt = JSON.parseObject(ro.getExt());

        JSONObject soPre = roExt.getJSONObject("so_pre");
        Integer distribution_way = null;
        String pre_warehouse_code = null;
        if (soPre != null) {
            distribution_way = soPre.getInteger("distribution_way");
            pre_warehouse_code = soPre.getString("pre_warehouse_code");
        }

        OfcSupplier ofcSupplier = null;
        if (DistributionWay.isPms(distribution_way)) {

            logger.info("ofc query supplier " + JSON.toJSONString(ro));
            ofcSupplier = this.supplierConfigService.findSupplier(ro.getSupplierId(), ro.getSupplierCode(), ro.getSupplierGroup(), ro.getSupplierOrg(), ro.getRegionCode(), ro.getSupplierDc());
            if (ofcSupplier == null) {
                throw EC.ERROR.exception("OFC供货商信息不存在! code = " + pre_warehouse_code);
            }
            logger.info("ofc query supplier " + JSON.toJSONString(ofcSupplier));
        }

        String costomerId = this.wumartBasicService.getCostomerId(WumartBasicContext.buildContext(ro.getRegionCode(), ro.getSupplierCode(), ro.getSupplierOrg(), ro.getSupplierId(),ro.getSupplierGroup()));
        CreateWgRoReqHead createRoReq = this.getCreateWgRoReq(costomerId, ofcSupplier);
        String wgPath = this.wumartBasicService.getWmsPath(WumartBasicContext.buildContext(ro.getRegionCode(), ro.getSupplierCode(), ro.getSupplierOrg(), ro.getSupplierId(), ro.getSupplierGroup()));
        CreateWgSoRespHead createSoResp = wgServiceProxy.createRo(createRoReq, wgPath);

        OfcRoHead filter = new OfcRoHead();
        filter.setId(ro.getId());
        filter.setRoBillCode(ro.getRoBillCode());
        filter.setRoStatus(RoStatus.UNCREATED.getValue());

        JSONObject ext = JSON.parseObject(ro.getExt());
        ext.put(Constants.RO_H_REF_RO_CODE, createSoResp.getRsoId());
        ext.put(Constants.RO_H_FULFILL_CREATE_TIME, OFCUtils.currentTime());
        OfcRoHead update = new OfcRoHead();
        update.setRoCode(createSoResp.getRsoId());
        update.setRoStatus(RoStatus.CREATED.getValue());
        update.setExt(ext.toJSONString());
        int r = this.ofcRoService.updateStatus(filter, update);
        logger.info("更新RO状态，单据号=" + ro.getRoBillCode() + "。。。" + RoStatus.CREATED + " " + r);

        return true;
    }
}
