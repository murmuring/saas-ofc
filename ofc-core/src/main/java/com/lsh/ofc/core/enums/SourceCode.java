package com.lsh.ofc.core.enums;

/**
 * Project Name: lsh-ofc
 * Created by fuhao
 * Date: 18/7/1
 * Time: 18/7/1.
 * 北京链商电子商务有限公司
 * Package name:com.lsh.ofc.core.enums.
 * desc:类功能描述
 */
public enum SourceCode {
    /**
     *
     */
    WUMART_1(1, 1),
    LSBJ_7(2, 7),
    LSTJ_7(3, 7),
    WG_4(4, 4);

    private final Integer org;

    private final Integer sourceCode;

    SourceCode(Integer org, Integer sourceCode) {
        this.org = org;
        this.sourceCode = sourceCode;
    }

    public Integer getOrg() {
        return org;
    }

    public Integer getSourceCode() {
        return sourceCode;
    }

    public static SourceCode valueOf(Integer value) {
        if (value == null) {
            return null;
        }
        for (SourceCode item : SourceCode.values()) {
            if (item.getOrg().equals(value)) {
                return item;
            }
        }
        return null;
    }
}
