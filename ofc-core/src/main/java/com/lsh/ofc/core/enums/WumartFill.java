package com.lsh.ofc.core.enums;

/**
 * Project Name: lsh-ofc
 * @author fuhao
 * Date: 2019-04-04
 * Time: 2019-04-04.
 * 北京链商电子商务有限公司
 * Package name:com.lsh.ofc.core.entity.
 * desc:类功能描述
 */
public enum WumartFill {
    /**
     *
     */
    IS_NOT_WUMARKT_FILL(0,"不履约物美，也无需创建进销存PO、SO"),
    IS_WUMARKT_FILL(1,"二次履约物美"),
    IS_NOT_WUMARKT_FILL_2_PSI(2,"不履约物美，创建进销存PO、SO");
    private final Integer value;

    private final String desc;

    WumartFill(Integer value, String desc) {
        this.value = value;
        this.desc = desc;
    }

    public static WumartFill valueOf(Integer value) {
        if (value == null) {
            return null;
        }
        for (WumartFill item : WumartFill.values()) {
            if (item.getValue().equals(value)) {
                return item;
            }
        }
        return null;
    }

    public Integer getValue() {
        return value;
    }

    public String getDesc() {
        return desc;
    }

}
