package com.lsh.ofc.core.service;

import com.lsh.base.common.exception.BusinessException;
import com.lsh.ofc.core.entity.MeipiCustomer;

/**
 * OFC客户信息Service
 * Created by huangdong on 16/8/30.
 */
public interface SaasMeipiCustomerService {

    /**
     * 创建美批客户
     *
     * @param regionCode
     * @return
     * @throws BusinessException
     */
    String addSaasMpCust(Integer regionCode, Integer cusType, String custCode, Long venderId) throws BusinessException;

    int count(MeipiCustomer customer) throws BusinessException;

    MeipiCustomer getMaxMeipiCodeByType(Integer custype) throws BusinessException;
}
