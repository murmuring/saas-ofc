package com.lsh.ofc.core.handler.logistics;

import com.lsh.base.common.exception.BusinessException;
import com.lsh.ofc.core.entity.OfcOrderHead;
import com.lsh.ofc.core.entity.OfcSoHead;
import com.lsh.ofc.core.model.SoMergeOrderInfoBO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationContext;

import java.util.List;

/**
 * Project Name: LshWgSupplyLogisticsHandler
 * 北京链商电子商务有限公司
 * @author peter
 * Date: 19/3/21
 * Package Name: com.lsh.ofc.core.handler.logistics
 * Description:
 */
@Slf4j
public class LshWgSupplyLogisticsHandler extends AbstractLogisticsHandler {

    protected LshWgSupplyLogisticsHandler(final ApplicationContext context, final OfcOrderHead orderHead, final List<OfcSoHead> sos) {
        super(context, orderHead, sos);
    }

    @Override
    protected Boolean process(SoMergeOrderInfoBO orderInfo) throws BusinessException {
        // 通知OMS，修改状态
        super.ofcSoMergeNotifyOtherSystemService.notifyOms(orderInfo);

        return true;
    }
}
