package com.lsh.ofc.core.constant;

/**
 * @author peter
 * @date 19/6/26
 */
public final class Constant_tms {

    public static final String DELIVERY_TIME_MEMO = "delivery_time_memo";

    public static final String TRANS_LIMIT = "trans_limit";

    public static final String CLOUD_DC_FLAG = "cloud_dc_flag";

    public static final String PARENT_HAVE_CLOUD_CHILDREN = "parent_have_cloud_children";

    public static final String SOURCE_SYSTEM = "source_system";

    public static final String SO_USER_ID = "so_user_id";

    public static final String ITEMS = "items";

    public static final String STATUS = "status";

    public static final String BILL_ID = "bill_id";
    public static final String ORDER_TYPE = "order_type";

    public static final String P_ORDER_ID = "p_order_id";

    public static final String ORDER_ID = "order_id";

    public static final String ADDRESS_ID = "address_id";

    public static final String PROVINCE_NAME = "province_name";

    public static final String CITY_NAME = "city_name";

    public static final String COUNTY_NAME = "county_name";

    public static final String ADDRESS = "address";

    public static final String MARKET_NAME = "market_name";

    public static final String REAL_POSITION = "real_position";

    public static final String ZONE_ID = "zone_id";

    public static final String WAREHOUSE_ID = "warehouse_id";

    public static final String WAREHOUSE_NAME = "warehouse_name";

    public static final String MONEY = "money";

    public static final String ORDERED_AT = "ordered_at";

    public static final String CONTAINER = "container";

    public static final String LOGISTICS_MODE = "logisticsMode";

}
