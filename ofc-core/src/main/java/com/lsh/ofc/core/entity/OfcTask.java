package com.lsh.ofc.core.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

/**
 * OFC任务
 *
 * @author huangdong
 * @date 16/8/28
 */
@Setter
@Getter
@NoArgsConstructor
@ToString
public class OfcTask extends Vender implements Serializable {

    private static final long serialVersionUID = 621060686175863830L;
    /**
     * ID
     */
    private Long id;

    /**
     * 关联ID
     */
    private Long refId;

    /**
     * 任务类型
     */
    private Integer type;

    /**
     * 任务状态
     */
    private Integer status;

    /**
     * 任务内容
     */
    private String content;

    /**
     * 执行次数
     */
    private Integer execCount;

    /**
     * 备注
     */
    private String remark;

    /**
     * 创建时间
     */
    private Integer createTime;

    /**
     * 更新时间
     */
    private Integer updateTime;

    /**
     * 是否有效（0:无效；1:有效）
     */
    private Integer valid;
}
