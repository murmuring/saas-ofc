package com.lsh.ofc.core.dao;

import com.lsh.ofc.core.base.BaseDAO;
import com.lsh.ofc.core.entity.OfcSoHead;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * OFC SO头DAO
 *
 * @author huangdong
 * @date 16/9/9
 */
@Repository
public interface OfcSoHeadDAO extends BaseDAO<OfcSoHead, Long> {

    /**
     * 更新返仓数量（增量）
     *
     * @param soBillCode
     * @param skuReturnQty
     * @return
     */
    int update4Return(@Param("soBillCode") String soBillCode, @Param("skuReturnQty") BigDecimal skuReturnQty);

    /**
     * 根据时间戳,状态获取指定数量的OfcSoHead
     *
     * @param statuses
     * @param timeInterval
     * @param offset
     * @param size
     * @return
     */
    List<OfcSoHead> fetchSoByStatusAndTimeStamp(@Param("statuses") List<Integer> statuses, @Param("timeInterval") int timeInterval, @Param("offset") long offset, @Param("size") int size);



    List<OfcSoHead> findListFilterByTime(@Param("startTime") Integer startTime,
                                            @Param("endTime") Integer endTime,
                                            @Param("supplierGroup") String supplierGroup,
                                            @Param("regionCode") Integer regionCode,
                                            @Param("supplierDc") String supplierDc,
                                            @Param("supplierOrg") Integer supplierOrg,
                                            @Param("supplierId") Integer supplierId
                                         );
    List<OfcSoHead> findListFilterByTime(Map<String,Object> queryMap);

}
