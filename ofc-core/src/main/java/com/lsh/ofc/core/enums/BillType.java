package com.lsh.ofc.core.enums;

/**
 * 单据类型
 *
 * @author huangdong
 * @date 16/11/1
 */
public enum BillType {
    /**
     *
     */
    ORDER, RETURN, SO, OBD, RO;


    public static boolean compareValue(String name) {
        BillType[] billTypes = BillType.values();

        for (BillType billType : billTypes) {
            if(billType.name().equals(name)){
                return true;
            }
        }

        return false;
    }
}
