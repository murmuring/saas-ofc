package com.lsh.ofc.core.handler;

import com.lsh.base.common.exception.BusinessException;
import com.lsh.ofc.core.entity.OfcSoHead;
import com.lsh.ofc.core.service.impl.AbstractCancelOrderService;
import com.lsh.ofc.proxy.service.ScServiceProxy;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * @author miaozhuang
 * @date 18/3/28
 */
@Slf4j
@Service("cancelOrderByLshCloudSCHandler")
public class CancelOrderByLshCloudSCHandler extends AbstractCancelOrderService {

    @Autowired
    private ScServiceProxy scServiceProxy;

    @Override
    public boolean cancelOrder(List<OfcSoHead> list) throws BusinessException {
        this.buildRequest(list);

        OfcSoHead ofcSoHead = list.get(0);
        // TODO: 云仓订单，orderType为2
        log.info("分拣中心 - SC01，订单取消，orderCode：{}", list.get(0).getOrderCode());
        boolean isSuccess = scServiceProxy.cancelCloudOrder(ofcSoHead.getSoBillCode(), ofcSoHead.getWarehouseCode(), 4);

        return isSuccess;
    }

    @Override
    protected List<String> buildRequest(List<OfcSoHead> list) {
        List<String> requestList = new ArrayList<>();

        return requestList;
    }
}
