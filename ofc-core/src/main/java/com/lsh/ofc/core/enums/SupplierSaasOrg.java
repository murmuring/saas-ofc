package com.lsh.ofc.core.enums;

/**
 * 供货机构枚举
 *
 * @author huangdong
 * @date 16/9/10
 */
public enum SupplierSaasOrg {
    /**
     *
     */
    Saas(1, "Saas");

    private final Integer value;

    private final String desc;

    SupplierSaasOrg(Integer value, String desc) {
        this.value = value;
        this.desc = desc;
    }

    public Integer getValue() {
        return value;
    }

    public String getDesc() {
        return desc;
    }

}
