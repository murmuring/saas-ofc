package com.lsh.ofc.core.handler;

import com.lsh.base.common.exception.BusinessException;
import com.lsh.ofc.core.entity.MeipiCustomer;
import com.lsh.ofc.core.service.MeipiCustomerService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @author panxudong
 * @date 17/3/22
 */
@Service
public class MeipiCustomerHandler {

    @Resource(name = "meipiCustomerSapHandler")
    private MeipiCustomerService meipiCustomerSapService;

    @Resource(name = "meipiCustomerOfcHandler")
    private MeipiCustomerService meipiCustomerOfcService;

    // TODO: 2019-03-25  此处默认是物美生成客户号，未能有其它生成方式。
    public boolean isWumartOfc() {
        return true;
    }

    private MeipiCustomerService getHandler() {
        if (isWumartOfc()) {
            return meipiCustomerOfcService;
        } else {
            return meipiCustomerSapService;
        }
    }

    public String addMpCust(Integer regionCode, Integer cusType) throws BusinessException {
        return this.getHandler().addMpCust(regionCode, cusType);
    }

    public String modMpCust(MeipiCustomer mpCustomer) throws BusinessException {
        return this.getHandler().modMpCust(mpCustomer);
    }

    public MeipiCustomer popMpCust(Integer regionCode, Integer cusType) throws BusinessException {
        return this.getHandler().popMpCust(regionCode, cusType);
    }

}
