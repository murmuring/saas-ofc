package com.lsh.ofc.core.handler.logistics;

import com.lsh.base.common.exception.BusinessException;
import com.lsh.ofc.core.entity.OfcOrderHead;
import com.lsh.ofc.core.entity.OfcSoHead;
import com.lsh.ofc.core.entity.OfcSupplier;
import com.lsh.ofc.core.model.SoMergeOrderInfoBO;
import com.lsh.ofc.core.proxy.service.WumartBasicService;
import com.lsh.ofc.core.service.SupplierConfigService;
import com.lsh.ofc.proxy.model.CreatePmsSoReqDetail;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationContext;

import java.util.ArrayList;
import java.util.List;

/**
 * Project Name: SoMerge2PmsHandler
 * 北京链商电子商务有限公司
 *
 * @author peter
 * Date: 19/2/27
 * Package Name: com.lsh.ofc.core.handler.logistics
 * Description:
 */
@Slf4j
@Deprecated
public class PreWarehouseLogisticsHandler extends AbstractLogisticsHandler {
    private WumartBasicService wumartBasicService;

    private SupplierConfigService supplierConfigService;

    protected PreWarehouseLogisticsHandler(final ApplicationContext context, final OfcOrderHead ofcOrderHead, final List<OfcSoHead> sos) {
        super(context, ofcOrderHead, sos);
        this.wumartBasicService = context.getBean(WumartBasicService.class);
        this.supplierConfigService = context.getBean(SupplierConfigService.class);
    }

    @Override
    protected Boolean process(final SoMergeOrderInfoBO orderInfo) throws BusinessException {
        String stoExt = "";
        List<CreatePmsSoReqDetail> pmsSoReqDetailList = new ArrayList<>();
        for (OfcSoHead so : sos) {
            if (so == null) {
                continue;
            }

            ofcSoMergeNotifyOtherSystemService.buildPmsSoDetails(so, pmsSoReqDetailList);
            stoExt = so.getExt();
        }

        // 订单通知PMS
        super.ofcSoMergeNotifyOtherSystemService.notifyPms(orderInfo, pmsSoReqDetailList, stoExt);

        // 通知OMS，修改状态
        super.ofcSoMergeNotifyOtherSystemService.notifyOms(orderInfo);

        return true;
    }

    @Override
    protected String getWarehouseCode(OfcSoHead so, String supplierCode) throws BusinessException {
//        OfcSupplier ofcSupplier = this.wumartBasicService.getOfcSupplier(String.valueOf(so.getPreWarehouseCode()), so.getRegionCode(), so.getSupplierId(), so.getSupplierOrg());
        OfcSupplier ofcSupplier = this.supplierConfigService.findSupplier(so.getSupplierId(), so.getSupplierCode(), so.getSupplierGroup(),
                so.getSupplierOrg(), so.getRegionCode(), so.getSupplierDc());
        // 此处用于展示oms在mis的展示，需要展示前置仓号。
        String warehouseCode = ofcSupplier.getWarehouseCode();
        return warehouseCode;
    }

}
