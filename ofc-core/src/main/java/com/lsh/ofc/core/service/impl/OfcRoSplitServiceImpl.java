package com.lsh.ofc.core.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.lsh.base.common.exception.BusinessException;
import com.lsh.ofc.core.entity.*;
import com.lsh.ofc.core.enums.FulfillChannel;
import com.lsh.ofc.core.enums.RoStatus;
import com.lsh.ofc.core.exception.EC;
import com.lsh.ofc.core.handler.MeipiCustomerHandler;
import com.lsh.ofc.core.service.OfcObdService;
import com.lsh.ofc.core.service.OfcOrderService;
import com.lsh.ofc.core.service.OfcRoSplitService;
import com.lsh.ofc.core.service.OfcSoService;
import com.lsh.ofc.proxy.context.WumartBasicContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.*;

/**
 * @author huangdong
 * @date 16/9/9
 */
@Service
public class OfcRoSplitServiceImpl implements OfcRoSplitService {

    private final Logger logger = LoggerFactory.getLogger(OfcRoSplitServiceImpl.class);

    @Autowired
    private OfcOrderService ofcOrderService;

    @Autowired
    private OfcSoService ofcSoService;

    @Autowired
    private OfcObdService ofcObdService;

    @Autowired
    private MeipiCustomerHandler meipiCustomerHandler;

    @Transactional(rollbackFor = Exception.class)
    @Override
    public List<OfcRoHead> split(OfcReturnHead order) throws BusinessException {
        //Map<SKU编号, Map<商品编号, SKU数>>
        Map<Long, Map<Long, BigDecimal>> skuQtyMap = this.mergeSkuInfoAsMap(order.getDetails());

        Long orderCode = order.getOrderCode();
        Long returnCode = order.getReturnCode();

        BigDecimal totalReturnQty = BigDecimal.ZERO;
        List<OfcObdHead> obds = this.fetchObds(orderCode, skuQtyMap.keySet());
        List<OfcRoHead> ros = new ArrayList<>(obds.size());
        for (OfcObdHead obd : obds) {
//            order.setVenderId(obd.getVenderId());
            //已退数量>=发货数量
            if (obd.getTotalSkuReturnQty().compareTo(obd.getTotalSkuDeliverQty()) >= 0) {
                continue;
            }
            List<OfcObdDetail> details = obd.getDetails();
            Map<Long, BigDecimal> soDetailReturnQtyMap = new HashMap<>((int) (details.size() / (0.75) + 10));
            Map<Long, BigDecimal> obdDetailReturnQtyMap = new HashMap<>((int) (details.size() / (0.75) + 10));
            //Map<商品编号, SKU数>
            Map<Long, BigDecimal> returnQtyMap = new HashMap<>((int) (details.size() / (0.75) + 10));
            for (OfcObdDetail detail : obd.getDetails()) {
                BigDecimal leftQty = detail.getSkuDeliverQty().subtract(detail.getSkuReturnQty());
                logger.info(order.getId() + "leftQty is " + leftQty);
                if (leftQty.compareTo(BigDecimal.ZERO) <= 0) {
                    continue;
                }
                Long skuCode = detail.getSkuCode();
                Map<Long, BigDecimal> qtyMap = skuQtyMap.get(skuCode);
                if (CollectionUtils.isEmpty(qtyMap)) {
                    continue;
                }
                //汇总OBD明细退货数量，分配销售商品退货数量
                BigDecimal returnQty = this.calcReturnQty(leftQty, qtyMap, returnQtyMap);
                obdDetailReturnQtyMap.put(detail.getId(), returnQty);
                //如果退货数据全部分配完，从集合中删除该SKU对应记录
                if (qtyMap.isEmpty()) {
                    skuQtyMap.remove(skuCode);
                }
            }
            //分配退货数量集合为空
            if (returnQtyMap.isEmpty()) {
                continue;
            }

            String billCode = obd.getSoBillCode();
            //组装返仓信息，分配SO明细退货数量
            OfcRoHead ro = this.buildRo(orderCode, billCode, returnQtyMap, soDetailReturnQtyMap);
            if (ro == null) {
                continue;
            }
            ro.setReturnCode(returnCode);

            //更新OBD返仓数量
            BigDecimal ret1 = this.ofcObdService.update4Return(billCode, obdDetailReturnQtyMap);
            //更新SO返仓数量
            BigDecimal ret2 = this.ofcSoService.update4Return(billCode, soDetailReturnQtyMap, order.getVenderId());
            if (ret1.compareTo(ret2) != 0) {
                throw EC.ERROR.exception("退货数量不一致！" + ret1 + "!=" + ret2);
            }
            ro.setTotalSkuReturnQty(ret1);

            JSONObject orderExt = JSON.parseObject(order.getExt());
            JSONObject roExt = JSON.parseObject(ro.getExt());

            orderExt.put("so_pre", roExt);
            ro.setExt(orderExt.toJSONString());
            ros.add(ro);
            totalReturnQty = totalReturnQty.add(ret1);
        }
        //如果未分配完
        if (!skuQtyMap.isEmpty()) {
            throw EC.ERROR.exception("部分商品无法返仓！map=" + JSON.toJSONString(skuQtyMap));
        }

        //更新Order返仓数量
        this.ofcOrderService.update4Return(orderCode, totalReturnQty, order.getVenderId());
        return ros;
    }


    /**
     * 合并商品信息
     *
     * @param details
     * @return Map&lt;SKU编号, Map&lt;商品编号, SKU数&gt;&gt;
     */
    private Map<Long, Map<Long, BigDecimal>> mergeSkuInfoAsMap(List<OfcReturnDetail> details) {
        //Map<SKU编号, Map<商品编号, SKU数>>
        Map<Long, Map<Long, BigDecimal>> skuQtyMap = new HashMap<>();
        for (OfcReturnDetail detail : details) {
            Long goodsCode = detail.getGoodsCode();
            Long skuCode = detail.getSkuCode();
            BigDecimal skuQty = detail.getSkuQty();

            Map<Long, BigDecimal> map = skuQtyMap.get(skuCode);
            if (map == null) {
                map = new HashMap<>(15);
                map.put(goodsCode, skuQty);
                skuQtyMap.put(skuCode, map);
            } else {
                BigDecimal qty = map.get(goodsCode);
                if (qty == null) {
                    qty = skuQty;
                } else {
                    qty = qty.add(skuQty);
                }
                map.put(goodsCode, qty);
            }
        }
        return skuQtyMap;
    }

    /**
     * 根据订单号获取OBD信息
     *
     * @param orderCode
     * @param skuCodes
     * @return
     * @throws BusinessException
     */
    private List<OfcObdHead> fetchObds(Long orderCode, Set<Long> skuCodes) throws BusinessException {
        OfcObdHead filter = new OfcObdHead();
        filter.setOrderCode(orderCode);
        List<OfcObdHead> obds = this.ofcObdService.findList(filter, true);
        if (CollectionUtils.isEmpty(obds)) {
            throw EC.SO_OBD_NOT_EXIST.exception("订单号=" + orderCode);
        }
        List<OfcObdHead> rets = new ArrayList<>(obds.size());
        for (OfcObdHead obd : obds) {
            List<OfcObdDetail> details = obd.getDetails();
            if (CollectionUtils.isEmpty(details)) {
                continue;
            }
            List<OfcObdDetail> list = new ArrayList<>(details.size());
            for (OfcObdDetail detail : details) {
                if (skuCodes.contains(detail.getSkuCode())) {
                    list.add(detail);
                }
            }
            if (!list.isEmpty()) {
                obd.setDetails(list);
                rets.add(obd);
            }
        }

        return rets;
    }


    /**
     * 汇总OBD明细退货数量，分配销售商品退货数量
     *
     * @param leftQty 商品可退数量
     * @param qtyMap  商品SKU请求退货数量，Map&lt;商品编号, SKU数&gt;
     * @param map     商品SKU请求分配退货数量，Map&lt;商品编号, SKU数&gt;
     * @return
     */
    private BigDecimal calcReturnQty(final BigDecimal leftQty, final Map<Long, BigDecimal> qtyMap, final Map<Long, BigDecimal> map) {
        BigDecimal returnQty = BigDecimal.ZERO;
        Iterator<Map.Entry<Long, BigDecimal>> it = qtyMap.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry<Long, BigDecimal> entry = it.next();
            BigDecimal skuQty = entry.getValue();
            if (skuQty == null || skuQty.compareTo(BigDecimal.ZERO) <= 0) {
                it.remove();
                continue;
            }
            Long goodsCode = entry.getKey();
            BigDecimal qty = map.get(goodsCode);
            if (qty == null) {
                qty = BigDecimal.ZERO;
            }
            BigDecimal tmpLeftQty = leftQty.subtract(returnQty);
            if (skuQty.compareTo(tmpLeftQty) <= 0) {
                returnQty = returnQty.add(skuQty);
                qty = qty.add(skuQty);
                it.remove(); //如果退货数据全部分配完，从集合中删除该商品对应记录
            } else {
                returnQty = returnQty.add(tmpLeftQty);
                qty = qty.add(tmpLeftQty);
                entry.setValue(skuQty.subtract(tmpLeftQty));
            }
            map.put(goodsCode, qty);
        }
        return returnQty;
    }

    /**
     * 组装返仓信息，分配SO明细退货数量
     *
     * @param orderCode
     * @param billCode
     * @param qtyMap
     * @param map
     * @return
     * @throws BusinessException
     */
    private OfcRoHead buildRo(final Long orderCode, final String billCode, final Map<Long, BigDecimal> qtyMap, final Map<Long, BigDecimal> map) throws BusinessException {
        OfcSoHead filter = new OfcSoHead();
        filter.setOrderCode(orderCode);
        filter.setSoBillCode(billCode);
        List<OfcSoHead> sos = this.ofcSoService.findList(filter, true);
        if (CollectionUtils.isEmpty(sos)) {
            throw EC.SO_ORDER_NOT_EXIST.exception("订单号=" + orderCode + "，单号=" + billCode);
        }
        if (sos.size() != 1) {
            throw EC.SO_ORDER_IS_ERROR.exception("存在多条SO单记录，订单号=" + orderCode + "，单号=" + billCode);
        }

        OfcSoHead so = sos.get(0);
        List<OfcSoDetail> soDetails = so.getDetails();
        List<OfcRoDetail> roDetails = new ArrayList<>(soDetails.size());

        for (OfcSoDetail detail : soDetails) {
            Long goodsCode = detail.getGoodsCode();
            BigDecimal qty = qtyMap.get(goodsCode);
            if (qty == null || qty.compareTo(BigDecimal.ZERO) <= 0) {
                qtyMap.remove(goodsCode);
                continue;
            }
            // TODO 前面已做校验
            qtyMap.remove(goodsCode);

            OfcRoDetail roDetail = new OfcRoDetail();
            roDetail.setItemNo(detail.getItemNo());
            roDetail.setGoodsCode(detail.getGoodsCode());
            roDetail.setGoodsName(detail.getGoodsName());
            roDetail.setGoodsSaleUnit(detail.getGoodsSaleUnit());
            // TODO: 2019-05-22，将此价格，修改为供货价（此处本来是售价）
            roDetail.setGoodsPrice(detail.getGoodsPrice());
            roDetail.setGoodsAmount(detail.getGoodsPrice().multiply(qty).divide(detail.getGoodsSaleUnit(), 2, BigDecimal.ROUND_HALF_UP));
            roDetail.setSkuCode(detail.getSkuCode());
            roDetail.setSkuSupplyCode(detail.getSkuSupplyCode());
            roDetail.setSkuReturnQty(qty);
            roDetail.setVenderId(detail.getVenderId());
            roDetails.add(roDetail);
            map.put(detail.getId(), qty);
        }

        if (!qtyMap.isEmpty()) {
            logger.info("ro deal qtyMap is {}", JSON.toJSONString(qtyMap));
            throw EC.ERROR.exception("qtyMap is " + JSON.toJSONString(qtyMap));
        }

        if (roDetails.isEmpty()) {
            return null;
        }
        OfcRoHead head = new OfcRoHead();
        head.setSoBillCode(billCode);
        head.setOrderCode(orderCode);
        head.setAddressCode(so.getAddressCode());
        head.setRegionCode(so.getRegionCode());
        head.setWarehouseCode(so.getWarehouseCode());
        head.setWarehouseName(so.getWarehouseName());
        head.setSupplierId(so.getSupplierId());
        head.setSupplierDc(so.getSupplierDc());
        head.setSupplierOrg(so.getSupplierOrg());
        head.setSupplierCode(so.getSupplierCode());
        head.setSupplierGroup(so.getSupplierGroup());
        head.setFulfillWms(so.getFulfillWms());
        head.setVenderId(so.getVenderId());

        Integer fulfillChannel; //TODO:物美sap过渡物美OFC
        if (FulfillChannel.valueOf(so.getFulfillChannel()) == FulfillChannel.WUMART_SAP) {
            fulfillChannel = FulfillChannel.WUMART_OFC.getValue();
        } else {
            fulfillChannel = so.getFulfillChannel();
        }
        head.setFulfillChannel(fulfillChannel);

        head.setSoCode(so.getSoCode());
        head.setRoStatus(RoStatus.UNCREATED.getValue());

        // TODO 添加前置仓信息到ext
        JSONObject roExt = new JSONObject();
        roExt.put("second_distribution_flag", so.getSecondDistributionFlag());
        roExt.put("distribution_way", so.getDistributionWay());
        roExt.put("distribution_type", so.getOrderDistributionType());
        roExt.put("pre_warehouse_code", so.getPreWarehouseCode());
        // 把so中客户号类型set到ro_ext中
        roExt.put(WumartBasicContext.CUSTYPE, JSON.parseObject(so.getExt()).getInteger(WumartBasicContext.CUSTYPE));


        head.setExt(roExt.toJSONString());
        head.setDetails(roDetails);
        return head;
    }

}
