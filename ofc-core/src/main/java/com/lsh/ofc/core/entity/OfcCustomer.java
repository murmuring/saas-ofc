package com.lsh.ofc.core.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

/**
 * OFC客户信息
 *
 * @author huangdong
 * @date 16/10/1
 */
@Getter
@Setter
@ToString
@NoArgsConstructor
public class OfcCustomer extends Vender implements Serializable {

    private static final long serialVersionUID = 3890754435524307219L;

    /** ID */
    private Long id;

    /**
     * 客户编号
     */
    private Long custCode;

    /**
     * 客户名称
     */
    private String custName;

    /**
     * 地域编号
     */
    private Integer regionCode;

    /**
     * 联系人姓名
     */
    private String contactName;

    /**
     * 联系人电话
     */
    private String contactPhone;

    /**
     * 省
     */
    private String province;

    /**
     * 市
     */
    private String city;

    /**
     * 区
     */
    private String district;

    /**
     * 地址
     */
    private String address;

    /**
     * 定位
     */
    private String location;

    /**
     * 美批客户编号
     */
    private String mpCustCode;

    /**
     * 美批客户地区
     */
    private String mpCustZone;

    /**
     * 其它信息
     */
    private String ext;

    /**
     * 创建时间
     */
    private Integer createTime;

    /**
     * 更新时间
     */
    private Integer updateTime;

    /**
     * 是否有效（0:无效；1:有效）
     */
    private Integer valid;

    /**
     * 1、链商北京151001 2、链商天津151002  3、链商餐馆151003 4、文固151004 5、自建
     */
    private Integer cusType;

    /**
     * 供货商机构 (1:物美；2:北京链商；3:天津链商；4:文固)
     */
    private Integer supplyOrg;
    private String dc;
    private Integer supplierId;

    private String supplierCode;

    private String supplierGroup;
}
