package com.lsh.ofc.core.service;

import com.lsh.ofc.core.entity.OfcSupplier;

import java.util.Map;

public interface SupplierConfigService {

    /**
     * @param supplierId
     * @param supplierCode
     * @param supplierGroup
     * @param supplierOrg
     * @param regionCode
     * @param supplierDc    只有ofc 才知道
     * @return
     */
    OfcSupplier findSupplier(Integer supplierId,
                             String supplierCode,
                             String supplierGroup,
                             Integer supplierOrg,
                             Integer regionCode,
                             String supplierDc);

    /**
     * 确定一条履约规则
     *
     * @param supplierCode
     * @param regionCode
     * @param supplierOrg
     * @param supplierId
     * @param supplierGroup
     * @return
     */
    OfcSupplier findSupplier(Integer supplierId,
                             String supplierCode,
                             String supplierGroup,
                             Integer supplierOrg,
                             Integer regionCode);

    Map<String, Object> findSupplierAndGetFulfillConfig(Integer regionCode, String supplierDc, Integer supplierOrg, Integer supplierId);

    String getUsrInFulfillConfigOfSupplier(Integer regionCode, java.lang.String supplierDc, Integer supplierOrg, Integer supplierId);

    int save(OfcSupplier supplier);
}
