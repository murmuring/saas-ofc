package com.lsh.ofc.core.enums;

/**
 * 供货机构枚举
 *
 * @author huangdong
 * @date 16/9/10
 */
public enum SupplierOrg {
    /**
     *
     */
    WUMART(1, "物美"),
    LSH(2, "北京链商"),
    LSH_TJ(3, "天津链商"),
    WG(4, "文固");

    private final Integer value;

    private final String desc;

    SupplierOrg(Integer value, String desc) {
        this.value = value;
        this.desc = desc;
    }

    public Integer getValue() {
        return value;
    }

    public String getDesc() {
        return desc;
    }

    /**
     * 判断供货机构是否为寄售
     *
     * @param org
     * @return
     */
    public static boolean isConsign(Integer org) {
        return SupplierOrg.LSH.getValue().equals(org) || SupplierOrg.LSH_TJ.getValue().equals(org);
    }
}
