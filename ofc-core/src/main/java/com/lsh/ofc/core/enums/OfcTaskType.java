package com.lsh.ofc.core.enums;

/**
 * OFC任务类型枚举
 *
 * @author huangdong
 * @date 16/9/10
 */
public enum OfcTaskType {
    /**
     * task 类型说明
     */
    ORDER_PMS_PUSH(5, "下发订单到pms"),

    SO_CREATE(11, "SO创建"),

    SO_PUSH_DMG(110, "SO下发物美ofc"),

    SO_PUSH_ATP_API(115, "SO下发openapi预占库存"),

    SO_MERGE(12, "SO合单"),

    CANCEL_ORDER_PMS(15, "pms取消订单"),

    SO_PUSH_PSI(16, "so push psi 模拟so过账"),

    SO_PO_PSI(18, "so 通知进销存创建PO"),

    OBD_MERGE(21, "DO通知"),

    OBD_PO_PSI(22, "obd 通知进销存创建PO并过账"),

    OBD_SO_PSI(23, "obd 通知进销存创建SO并过账"),

    OBD_QUERY(25, "OBD查询"),

    OBD_BOX_PUSH(27, "OBD 包裹数据推送SC"),

    OBD_BOX_CONFERM_PUSH(28, "OBD 包裹数推送商城"),

    RO_CREATE(31, "RO创建"),

    RO_MERGE(32, "RO合单"),

    SO_VALIDATE(41, "SO创建验证"),

    CANCEL_VALIDATE(42, "取消订单验证"),

    STO_CREATE(60,"STO创建"),

    STO_OBD_DMALL(65,"下发STO-OBD给多点"),

    STO_ORDER_SO_CREATE(80,"SO-STO-ORDER"),

    CUSTOMER_REFRESH(91, "客户信息刷新");

    private final Integer value;

    private final String desc;

    OfcTaskType(Integer value, String desc) {
        this.value = value;
        this.desc = desc;
    }

    public Integer getValue() {
        return value;
    }

    public String getDesc() {
        return desc;
    }

    @Override
    public String toString() {
        return String.valueOf(this.getValue());
    }

    public static OfcTaskType valueOf(Integer value) {
        if (value == null) {
            return null;
        }
        for (OfcTaskType item : OfcTaskType.values()) {
            if (item.getValue().equals(value)) {
                return item;
            }
        }

        return null;
    }
}
