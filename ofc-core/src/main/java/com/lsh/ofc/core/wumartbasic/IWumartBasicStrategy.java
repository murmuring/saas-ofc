package com.lsh.ofc.core.wumartbasic;

import com.lsh.ofc.proxy.context.WumartBasicContext;

import java.util.Map;

/**
 *
 * @author panxudong
 * @date 17/7/27
 */
public interface IWumartBasicStrategy {

    boolean validate(WumartBasicContext context);

    String  getConfigKey(WumartBasicContext context);

    Map<String, String> getConfig(WumartBasicContext context);

    String getConfigValue(WumartBasicContext context);
}
