package com.lsh.ofc.core.service;

import com.alibaba.fastjson.JSONArray;
import com.lsh.ofc.core.entity.OfcSoHead;
import com.lsh.ofc.core.model.SoMergeOrderInfoBO;
import com.lsh.ofc.proxy.model.CreatePmsSoReqDetail;

import java.util.List;

/**
 * Project Name: OfcSoMergeNotifyOtherSystemService
 * 北京链商电子商务有限公司
 * @author wangliutao
 * Date: 19/3/4
 * Package Name: com.lsh.ofc.core.service
 * Description:
 */
public interface OfcSoMergeNotifyOtherSystemService {

    /**
     * 构建pms so 详情
     *
     * @param so
     * @param pmsSoReqDetailList
     */
    void buildPmsSoDetails(OfcSoHead so, List<CreatePmsSoReqDetail> pmsSoReqDetailList);

    /**
     * 构建tms so 详情
     * @param so
     * @param items
     */
    void buildTmsSoDetails(OfcSoHead so, JSONArray items);

    /**
     * 通知pms
     * @param orderInfo
     * @param pmsSoReqDetailList
     * @param stoExt
     * @return
     */
    boolean notifyPms(SoMergeOrderInfoBO orderInfo, List pmsSoReqDetailList, String stoExt);

    /**
     * 通知tms
     * @param items
     * @param orderInfo
     * @return
     */
    boolean notifyTms(JSONArray items, SoMergeOrderInfoBO orderInfo);

    /**
     * 通知 oms
     * @param orderInfo
     */
    void notifyOms(SoMergeOrderInfoBO orderInfo);
}
