package com.lsh.ofc.core.model;

import lombok.*;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * Project Name: lsh-ofc
 * Created by fuhao
 * Date: 18/7/1
 * Time: 18/7/1.
 * 北京链商电子商务有限公司
 * Package name:com.lsh.ofc.core.model.
 * desc:类功能描述
 */
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
@Builder
public class StoSoHead implements Serializable {
    /**
     * SO单据号
     */
    private String soBillCode;

    /**
     * 订单号
     */
    private Long orderCode;

    /**
     * 供货商ID
     */
    private Integer supplierId;
    /**
     * 供货商DC
     */
    private String supplierDc;

    /**
     * 供货商机构
     */
    private Integer supplierOrg;
    /**
     * 履约WMS
     */
    private Integer fulfillWms;

    /**
     * 履约渠道
     */
    private Integer fulfillChannel;
    /**
     * 地域编号
     */
    private Integer regionCode;
    /**
     * 仓库编号
     */
    private String warehouseCode;

    /**
     * 仓库名称
     */
    private String warehouseName;

    /**
     * SKU下单总数
     */
    private BigDecimal totalSkuOrderQty;

    /**
     * SKU供货总数
     */
    private BigDecimal totalSkuSupplyQty;


    /**
     * 前置仓code
     */
    private String preWarehouseCode;
    /**
     * 二次配送标记
     */
    private Integer secondDistributionFlag;

    /**
     * 配送方式
     */
    private Integer distributionWay;
}
