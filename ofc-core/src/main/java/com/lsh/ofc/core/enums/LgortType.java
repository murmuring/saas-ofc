package com.lsh.ofc.core.enums;

/**
 * Project Name: lsh-ofc
 *
 * @author peter
 * @date 19/4/2
 * 北京链商电子商务有限公司
 * Package
 * desc:
 */
public enum LgortType {

    /**
     * 在库
     */
    IN_DC(1, "SO_LIANS_ZK"),

    /**
     * 直流
     */
    WUMART_DC(2, "SO_LIANS_ZL");


    private final Integer code;

    private final String desc;

    LgortType(Integer code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public Integer getCode() {
        return code;
    }

    public String getDesc() {
        return desc;
    }

    public static LgortType getEnumType(Integer code) {
        if (code == null) {
            return null;
        }
        for (LgortType item : LgortType.values()) {
            if (item.getCode().equals(code)) {
                return item;
            }
        }

        return null;
    }
}
