package com.lsh.ofc.core.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

/**
 * Project Name: lsh-oms
 * @author peter
 * Date: 2019-09-03
 * Time: 2019-09-03.
 * 北京链商电子商务有限公司
 * Package name:com.lsh.ofc.core.model.base
 * desc:类功能描述
 */
@Getter
@Setter
@ToString
@NoArgsConstructor
public class Vender implements Serializable {

    /**
     *  Long.parseLong(RpcContext.getContext().getAttachment("venderId"));
     */
    private Long venderId;
}
