package com.lsh.ofc.core.enums;

/**
 * Project Name: lsh-ofc
 * @author by fuhao
 * Date: 2019-04-11
 * Time: 2019-04-11.
 * 北京链商电子商务有限公司
 * Package name:com.lsh.ofc.core.enums.
 * desc: SO 订单履约类型
 */
public enum SoOrderType {
    /**
     *
     */
    NOT_PRE_SALE(1,"非预售"),
    PRE_SALE(2,"预售单");

    private final Integer value;

    private final String desc;

    SoOrderType(Integer value, String desc) {
        this.value = value;
        this.desc = desc;
    }

    public static SoOrderType valueOf(Integer value) {
        if (value == null) {
            return null;
        }
        for (SoOrderType item : SoOrderType.values()) {
            if (item.getValue().equals(value)) {
                return item;
            }
        }
        return null;
    }

    public Integer getValue() {
        return value;
    }

    public String getDesc() {
        return desc;
    }}
