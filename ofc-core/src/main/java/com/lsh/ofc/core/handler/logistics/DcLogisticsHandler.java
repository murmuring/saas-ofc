package com.lsh.ofc.core.handler.logistics;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.lsh.base.common.exception.BusinessException;
import com.lsh.ofc.core.entity.*;
import com.lsh.ofc.core.model.SoMergeOrderInfoBO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationContext;

import java.util.List;

/**
 * Project Name: SoMerge2TmsHandler
 * 北京链商电子商务有限公司
 * @author peter
 * Date: 19/2/27
 * Package Name: com.lsh.ofc.core.handler.logistics
 * Description:
 */
@Slf4j
public class DcLogisticsHandler extends AbstractLogisticsHandler {

    protected DcLogisticsHandler(final ApplicationContext context, final OfcOrderHead orderHead, final List<OfcSoHead> sos) {
        super(context, orderHead, sos);
    }

    @Override
    protected Boolean process(SoMergeOrderInfoBO orderInfo) throws BusinessException {

        JSONArray details = new JSONArray();
        for (OfcSoHead so : sos) {
            super.ofcSoMergeNotifyOtherSystemService.buildTmsSoDetails(so, details);
        }
        log.info("DCLogisticsHandler orderInfo is {}",JSON.toJSONString(orderInfo));
        // 订单下发TMS
        super.ofcSoMergeNotifyOtherSystemService.notifyTms(details, orderInfo);
        // 通知OMS，修改状态
        super.ofcSoMergeNotifyOtherSystemService.notifyOms(orderInfo);

        return true;
    }
}
