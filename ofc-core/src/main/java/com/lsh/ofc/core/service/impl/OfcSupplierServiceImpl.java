package com.lsh.ofc.core.service.impl;

import com.alibaba.fastjson.JSON;
import com.lsh.ofc.core.constant.Constants;
import com.lsh.ofc.core.dao.OfcSupplierDAO;
import com.lsh.ofc.core.entity.OfcSupplier;
import com.lsh.ofc.core.handler.WumartBasicHandler;
import com.lsh.ofc.core.redis.RedisTemplate;
import com.lsh.ofc.core.service.OfcSupplierService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.MessageFormat;
import java.util.List;

/**
 * @author peter
 */
@Service
@Slf4j
public class OfcSupplierServiceImpl implements OfcSupplierService {

    @Autowired
    private OfcSupplierDAO ofcSupplierDAO;

    @Autowired
    private RedisTemplate redisTemplate;

    @Autowired
    private WumartBasicHandler wumartBasicHandler;

//    @Override
//    public OfcSupplier getSupplierByCode(String code, Integer regionCode, Integer supplyId, Integer supplyOrg) {
//        OfcSupplier supplier;
//
//        String redisKey = this.buildRedisKey(code, regionCode, supplyId, supplyOrg);
//        log.info("ofc supplier key " + redisKey);
////        boolean isExist = this.redisTemplate.hexists(Constants.REDIS_HASH, redisKey);
////        log.info("ofc supplier key " + redisKey + " isExist is " + isExist);
////        if (!isExist) {
////            supplier = this.setSupplierToRedis(redisKey, code, regionCode, supplyId, supplyOrg);
////        } else {
////            String supplierString = this.redisTemplate.hget(Constants.REDIS_HASH, redisKey);
////            if (StringUtils.hasText(supplierString)) {
////                supplier = JSON.parseObject(supplierString, OfcSupplier.class);
////            } else {
////                supplier = this.setSupplierToRedis(redisKey, code, regionCode, supplyId, supplyOrg);
////            }
////        }
//
//        supplier = this.setSupplierToRedis(redisKey, code, regionCode, supplyId, supplyOrg);
//
//        return supplier;
//    }
//
//    @Override
//    public List<OfcSupplier> findList(OfcSupplier params) {
//        return this.ofcSupplierDAO.findList(params);
//    }
//
//    private OfcSupplier setSupplierToRedis(String redisKey, String code, Integer regionCode, Integer supplyId, Integer supplyOrg) {
//
//        log.info("ofc ss supplier code : " + code + " regionCode : " + regionCode + " : supplyOrg : " + supplyOrg);
//        OfcSupplier supplier = this.getSupplierByCodeForDb(code, regionCode, supplyId, supplyOrg);
//        if (supplier == null) {
//            return null;
//        }
//
//        this.redisTemplate.hset(Constants.REDIS_HASH, redisKey, JSON.toJSONString(supplier));
//
//        this.wumartBasicHandler.updateConfig(supplier);
//        return supplier;
//    }
//
//    @Override
//    public OfcSupplier getSupplierByCodeOne(String code, Integer regionCode, Integer supplyOrg) {
//        OfcSupplier param = new OfcSupplier();
//        param.setCode(code);
//        param.setRegionCode(regionCode);
//        param.setSupplierOrg(supplyOrg);
//        return this.ofcSupplierDAO.findOne(param);
//    }
//
//    private OfcSupplier getSupplierByCodeForDb(String code, Integer regionCode, Integer supplyId, Integer supplyOrg) {
//        OfcSupplier param = new OfcSupplier();
//        param.setCode(code);
//        param.setRegionCode(regionCode);
//        param.setSupplierId(supplyId);
//        param.setSupplierOrg(supplyOrg);
//        return this.ofcSupplierDAO.findOne(param);
//    }
//
//    private String buildRedisKey(String code, Integer regionCode, Integer supplyId, Integer supplyOrg) {
//        String redisKey = MessageFormat.format(Constants.OFC_SUPPLIER_PREFIX, regionCode, code, supplyId, supplyOrg);
//        return redisKey;
//    }
//
//
//    /**
//     * 根据供货商编号获取OFC供货商信息
//     *
//     * @param code
//     * @param regionCode
//     * @param supplyOrg
//     * @param supplyId
//     * @return
//     */
//    @Override
//    public OfcSupplier getSupplierOneFromDb(String code, Integer regionCode, Integer supplyOrg, Integer supplyId) {
//        OfcSupplier param = new OfcSupplier();
//        param.setCode(code);
//        param.setRegionCode(regionCode);
//        param.setSupplierOrg(supplyOrg);
//        param.setSupplierId(supplyId);
//        return this.ofcSupplierDAO.findOne(param);
//    }
//
//    @Override
//    public OfcSupplier getSupplierSaasFromDb(String code, Integer regionCode, Integer supplyOrg, Integer supplyId, Long venderId) {
//        OfcSupplier param = new OfcSupplier();
//        param.setCode(code);
//        param.setRegionCode(regionCode);
//        param.setSupplierOrg(supplyOrg);
//        param.setSupplierId(supplyId);
//        param.setVenderId(venderId);
//        return this.ofcSupplierDAO.findOne(param);
//    }
//
//    @Override
//    public int insertSupplier(OfcSupplier ofcSupplier) {
//        return this.ofcSupplierDAO.insert(ofcSupplier);
//    }

}
