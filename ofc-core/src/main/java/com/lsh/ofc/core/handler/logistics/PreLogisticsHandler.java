package com.lsh.ofc.core.handler.logistics;

import com.lsh.base.common.exception.BusinessException;
import com.lsh.ofc.core.entity.OfcOrderHead;
import com.lsh.ofc.core.entity.OfcSoHead;
import com.lsh.ofc.core.model.SoMergeOrderInfoBO;
import com.lsh.ofc.proxy.model.CreatePmsSoReqDetail;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationContext;

import java.util.ArrayList;
import java.util.List;

/**
 * Project Name: DC2PreLogisticsHandler
 * 北京链商电子商务有限公司
 * @author peter
 * Date: 19/3/4
 * Package Name: com.lsh.ofc.core.handler.logistics
 * Description:
 */
@Slf4j
@Deprecated
public class PreLogisticsHandler extends AbstractLogisticsHandler {

    protected PreLogisticsHandler(final ApplicationContext context, final OfcOrderHead ofcOrderHead, final List<OfcSoHead> sos) {
        super(context, ofcOrderHead, sos);
    }

    @Override
    protected Boolean process(final SoMergeOrderInfoBO orderInfo) throws BusinessException {
        String stoExt = "";
        List<CreatePmsSoReqDetail> pmsSoReqDetailList = new ArrayList<>();
        for (OfcSoHead so : sos) {
            if (so == null) {
                continue;
            }

            ofcSoMergeNotifyOtherSystemService.buildPmsSoDetails(so, pmsSoReqDetailList);
            stoExt = so.getExt();
        }
        // 订单通知PMS
        super.ofcSoMergeNotifyOtherSystemService.notifyPms(orderInfo, pmsSoReqDetailList, stoExt);

        return true;
    }
}
