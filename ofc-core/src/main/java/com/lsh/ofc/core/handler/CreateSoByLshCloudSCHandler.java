package com.lsh.ofc.core.handler;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.lsh.base.common.exception.BusinessException;
import com.lsh.ofc.core.constant.Constants;
import com.lsh.ofc.core.entity.OfcCustomer;
import com.lsh.ofc.core.entity.OfcOrderHead;
import com.lsh.ofc.core.entity.OfcSoDetail;
import com.lsh.ofc.core.entity.OfcSoHead;
import com.lsh.ofc.core.enums.SoStatus;
import com.lsh.ofc.core.exception.EC;
import com.lsh.ofc.core.util.OFCUtils;
import com.lsh.ofc.proxy.model.CreateScSoReqHead;
import com.lsh.ofc.proxy.service.ScServiceProxy;
import org.springframework.context.ApplicationContext;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * 【链商云仓】创建SO处理Handler
 *
 * @author miaozhuang
 * @date 18/8/10
 */
public class CreateSoByLshCloudSCHandler extends CreateSoHandler {

    private final ScServiceProxy scServiceProxy;

    protected CreateSoByLshCloudSCHandler(final ApplicationContext context, final OfcOrderHead ofcOrderHead, final OfcSoHead so, final OfcCustomer customer) {
        super(context, ofcOrderHead, so, customer);
        this.scServiceProxy = context.getBean(ScServiceProxy.class);
    }

    @Override
    protected boolean process(final OfcSoHead so, final OfcCustomer customer) throws BusinessException {
        // TODO：下发给wms
        CreateScSoReqHead scSoReqHead = super.getCreateScSoReq();
        boolean isSuccess = scServiceProxy.pushCloudSo(scSoReqHead);
        if (!isSuccess) {
            logger.error("云仓订单，下发给SC失败！soBillCode: " + so.getSoBillCode());
            throw EC.ERROR.exception("云仓订单，下发给WMS失败！");
        }

        List<OfcSoDetail> updateDetails = new ArrayList<>(so.getDetails().size());
        BigDecimal totalSkuQty = BigDecimal.ZERO;
        for (OfcSoDetail detail : so.getDetails()) {
            BigDecimal respQty = detail.getSkuOrderQty();
            if (respQty == null) {
                respQty = BigDecimal.ZERO;
            }

            OfcSoDetail updateDetail = new OfcSoDetail();
            updateDetail.setId(detail.getId());
            updateDetail.setSkuSupplyQty(respQty);
            updateDetails.add(updateDetail);
            totalSkuQty = totalSkuQty.add(respQty);
        }

        logger.info("*****CreateLshCloudSoRespHead updateDetails is " + JSON.toJSONString(updateDetails));

        JSONObject ext = JSON.parseObject(so.getExt());
        ext.put(Constants.SO_H_REF_SO_CODE, so.getSoBillCode());
        ext.put(Constants.SO_H_FULFILL_CREATE_TIME, OFCUtils.currentTime());
        OfcSoHead updateHead = new OfcSoHead();
        updateHead.setId(so.getId());
        updateHead.setSoBillCode(so.getSoBillCode());
        updateHead.setSoStatus(SoStatus.CREATED.getValue());
        updateHead.setSoCode(so.getSoBillCode());
        updateHead.setTotalSkuSupplyQty(totalSkuQty);
        updateHead.setExt(ext.toJSONString());
        updateHead.setDetails(updateDetails);
        updateHead.setPushPsiFlag(so.getPushPsiFlag());
        logger.info("*****CreateLshCloudSoRespHead update4Create");
        this.ofcSoService.update4Create(updateHead, SoStatus.UNCREATED, SoStatus.CREATED, so.getVenderId());

        return true;
    }

}
