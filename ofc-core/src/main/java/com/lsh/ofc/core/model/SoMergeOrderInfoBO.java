package com.lsh.ofc.core.model;

import com.lsh.ofc.core.entity.OfcOrderHead;
import com.lsh.ofc.core.entity.OfcSoHead;
import lombok.*;

import java.math.BigDecimal;
import java.util.*;

/**
 * Project Name: SoMergeOrderInfoBO
 * 北京链商电子商务有限公司
 * @author: wangliutao
 * Date: 19/3/4
 * Package Name: com.lsh.ofc.core.model
 * Description:
 */
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class SoMergeOrderInfoBO {

    private Long pOrderCode;
    /**
     * 订单号
     */
    private Long orderCode;

    /**
     * wms 订单类型
     */
    private Integer orderType;

    /**
     * 订单所在的DC
     */
    private String orderDc;

    /**
     * 美批号
     */
    private List<String> mpCustCodes;

    /**
     * cusType
     */
    private Integer cusType;

    /**
     * 订单的sku数量
     */
    private BigDecimal totalSkuOrderQty;

    /**
     * 订单的sku供应数量
     */
    private BigDecimal totalSkuSupplyQty;

    /**
     * 用户订单
     */
    private OfcOrderHead ofcOrderHead;

    /**
     * so单
     */
    private List<OfcSoHead> soHeads;

    /**
     * 订单，对应的soCodes
     */
    private List<String> soCodes;

    /**
     * 该集合中，可能包含soBillCode、soCode
     */
    private List<String> mixtureSoCodes;

    /**
     * 订单，对应的soBIllCodes
     */
    private List<String> soBillCodes;

    /**
     * 订单商品，所在仓库的集合
     */
    private Set<String> warehouseCodes;

    /**
     * so单和来源的对应关系 'Map<soBillCode, org>'
     */
    private Map<String, Integer> sourceSystem;

    /**
     * 订单商品，对应机构的集合
     */
    private List<Integer> soSupplyOrg;


    private Long venderId;

}
