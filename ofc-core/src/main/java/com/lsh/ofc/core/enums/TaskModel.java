package com.lsh.ofc.core.enums;

/**
 * Project Name: lsh-ofc
 *
 * @author peter
 * @date 19/4/2
 * 北京链商电子商务有限公司
 * Package
 * desc:
 */
public enum TaskModel {

    /**
     * 整箱
     */
    TASK_BOX(1, "整箱"),

    /**
     * 拆零
     */
    TASK_PART(2, "拆零");


    private final Integer code;

    private final String desc;

    TaskModel(Integer code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public Integer getCode() {
        return code;
    }

    public String getDesc() {
        return desc;
    }

    public static TaskModel getEnumType(Integer code) {
        if (code == null) {
            return null;
        }
        for (TaskModel item : TaskModel.values()) {
            if (item.getCode().equals(code)) {
                return item;
            }
        }

        return null;
    }
}
