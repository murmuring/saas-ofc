package com.lsh.ofc.core.handler.logistics;

import com.lsh.base.common.exception.BusinessException;
import com.lsh.ofc.core.entity.OfcOrderHead;
import com.lsh.ofc.core.entity.OfcSoHead;
import com.lsh.ofc.core.model.SoMergeOrderInfoBO;
import org.springframework.context.ApplicationContext;

import java.util.List;

/**
 * Project Name: NoLogisticsHandler
 * 北京链商电子商务有限公司
 * @author peter
 * Date: 19/3/4
 * Package Name: com.lsh.ofc.core.handler.logistics
 * Description:
 */
public class NoLogisticsHandler extends AbstractLogisticsHandler {

    public NoLogisticsHandler(final ApplicationContext context, final OfcOrderHead ofcOrderHead, final List<OfcSoHead> sos) {
        super(context, ofcOrderHead, sos);
    }

    @Override
    protected Boolean process(SoMergeOrderInfoBO orderInfo) throws BusinessException {
        return Boolean.TRUE;
    }
}
