package com.lsh.ofc.core.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

/**
 * Project Name: lsh-ofc
 *
 * @author peter
 * @date 19/3/27
 * 北京链商电子商务有限公司
 * Package
 * desc:
 */
@Getter
@Setter
@NoArgsConstructor
@ToString
public class DmgSoObdVo {

    private Long pushCreated;

    private List<DmgSoSo> soSo;

    private List<DmgSoObd> soObd;
}
