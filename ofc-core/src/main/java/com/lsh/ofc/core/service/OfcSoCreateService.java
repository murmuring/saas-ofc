package com.lsh.ofc.core.service;

import com.alibaba.fastjson.JSONObject;
import com.lsh.base.common.exception.BusinessException;
import com.lsh.base.common.model.CommonResult;
import com.lsh.ofc.core.entity.OfcOrderHead;
import com.lsh.ofc.core.entity.OfcSoHead;

import java.util.List;

/**
 * OFC SO创建Service
 *
 * @author huangdong
 * @date 16/9/10
 */
public interface OfcSoCreateService {

    /**
     * 准备(记录商品信息、拆单)
     * @param order     订单号
     * @return          返回值
     * @throws BusinessException
     */
    List<OfcSoHead> prepare(OfcOrderHead order) throws BusinessException;

    /**
     * 处理(提交SO)
     *
     * @param sos                so单
     * @return                   返回值
     * @throws BusinessException 异常
     */
    int process(OfcOrderHead ofcOrderHead, List<OfcSoHead> sos,Integer channel) throws BusinessException;

    /**
     * 合单
     *
     * @param orderCode           订单号
     * @return                    返回值
     * @throws BusinessException  异常
     */
    int merger(Long orderCode) throws BusinessException;

    /**
     * 回调
     * @param data                参数
     * @return                    返回值
     * @throws BusinessException  异常
     */
    CommonResult<Boolean> callback(JSONObject data) throws BusinessException;
}
