package com.lsh.ofc.core.enums;

/**
 * RO状态枚举
 *
 * @author miaozhuang
 * @date 16/9/10
 */
public enum SupplyDc2Code {
    /**
     *
     */
    DC09(1, "DC09"),
    DC37(2, "DC37"),
    DC42(3, "DC42"),
    DC43(4, "DC43"),
    DC46(5, "DC46"),
    DC47(6, "DC47"),
    LS02(7, "LS02"),
    LS03(8, "LS03"),
    LS37(9, "LS37");

    private final Integer code;

    private final String dc;

    SupplyDc2Code(Integer code, String dc) {
        this.code = code;
        this.dc = dc;
    }

    public Integer getCode() {
        return code;
    }

    public String getDc() {
        return dc;
    }

    public static SupplyDc2Code getDc2Code(String dc) {
        for (SupplyDc2Code item : SupplyDc2Code.values()) {
            if (item.getDc().equals(dc)) {
                return item;
            }
        }
        return null;
    }
}
