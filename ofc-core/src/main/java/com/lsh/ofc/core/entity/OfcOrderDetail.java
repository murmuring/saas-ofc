package com.lsh.ofc.core.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * OFC订单明细
 * Created by huangdong on 16/9/12.
 */
@Setter
@Getter
@NoArgsConstructor
@ToString
public class OfcOrderDetail extends Vender implements Serializable {

    private static final long serialVersionUID = 3375311778994605533L;
    /**
     * ID
     */
    private Long id;

    /**
     * 单号
     */
    private Long orderCode;

    private Integer itemCode;

    /**
     * 商品编号
     */
    private Long goodsCode;

    /**
     * 商品名称
     */
    private String goodsName;

    /**
     * 商品售卖单位
     */
    private BigDecimal goodsSaleUnit;

    /**
     * 商品单价
     */
    private BigDecimal goodsPrice;

    /**
     * 商品数量
     */
    private BigDecimal goodsQty;

    /**
     * 商品金额
     */
    private BigDecimal goodsAmount;

    /**
     * SKU编号
     */
    private Long skuCode;

    /**
     * SKU数量
     */
    private BigDecimal skuQty;

    /**
     * 其它信息
     */
    private String ext;

    /**
     * 创建时间
     */
    private Integer createTime;

    /**
     * 更新时间
     */
    private Integer updateTime;
}
