package com.lsh.ofc.core.kafka;

import com.lsh.base.common.exception.BusinessException;
import com.lsh.ofc.core.exception.EC;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;

import java.util.Properties;

/**
 *
 * @author huangdong
 * @date 16/8/28
 */
@Slf4j
public class KafkaTemplate implements InitializingBean, DisposableBean {

    private Properties props;

    private KafkaProducer producer;

    public KafkaTemplate(Properties props) {
        this.props = props;
    }

    public synchronized boolean send(String topic, String content) throws BusinessException {
        log.info("[send][" + topic + "][start]msg:" + content);
        try {
            this.producer.send(new ProducerRecord<String, String>(topic, content)).get();
            log.info("[send][" + topic + "][end]msg:" + content);
            return true;
        } catch (Exception e) {
            log.error("[send][" + topic + "][error]msg:" + content + "\n" + e.getMessage(), e);
            throw new BusinessException(EC.ERROR.getCode(), "[send][" + topic + "][error]msg:" + content + "\n" + e.getMessage(), e);
        }
    }

    @Override
    synchronized public void afterPropertiesSet() throws Exception {
        this.producer = new KafkaProducer(props);
    }

    @Override
    synchronized public void destroy() throws Exception {
        if (this.producer != null) {
            this.producer.close();
        }
    }
}
