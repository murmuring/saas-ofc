package com.lsh.ofc.core.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * Project Name: lsh-ofc
 *
 * @author peter
 * @date 2019-08-05
 * 北京链商电子商务有限公司
 * Package
 * desc:
 */
@AllArgsConstructor
@Getter
public enum SumFlag {

    NORMAL("0","正常"),SUM("1","汇总");


    private final String code;

    private final String desc;


}
