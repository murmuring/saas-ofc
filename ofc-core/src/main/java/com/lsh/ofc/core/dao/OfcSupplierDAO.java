package com.lsh.ofc.core.dao;

import com.lsh.ofc.core.base.BaseDAO;
import com.lsh.ofc.core.entity.OfcSupplier;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * OFC供货商DAO
 *
 * @author huangdong
 * @date 16/8/1
 */
@Repository
public interface OfcSupplierDAO extends BaseDAO<OfcSupplier, Integer> {

    List<OfcSupplier> find(@Param("supplier_code") String supplierCode,
                           @Param("supplier_org") Integer supplierOrg,
                           @Param("supplier_id") Integer supplierId,
                           @Param("supplier_group") String supplierGroup,
                           @Param("region_code") Integer regionCode,
                           @Param("supplier_dc") String supplierDc);
}
