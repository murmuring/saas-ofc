package com.lsh.ofc.core.service.impl;

import com.lsh.ofc.core.base.BaseTestCase;
import com.lsh.ofc.core.entity.OfcTask;
import com.lsh.ofc.core.enums.OfcTaskStatus;
import com.lsh.ofc.core.enums.OfcTaskType;
import com.lsh.ofc.core.service.OfcTaskService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.annotation.Resource;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:application-context.xml")
@TestPropertySource("classpath:application.properties")
@Sql(scripts = "classpath:cleanup.sql", executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
public class OfcTaskServiceImplTest extends BaseTestCase {

    @Resource
    private OfcTaskService tasks;

    private static final Long REF_ID = 1L;

    @Test
    public void should_disable_pending_task() {
        OfcTask pendingTask = new OfcTask();
        pendingTask.setRefId(REF_ID);
        pendingTask.setType(OfcTaskType.SO_CREATE.getValue());
        pendingTask.setStatus(OfcTaskStatus.NEW.getValue());
        pendingTask.setVenderId(1L);
        pendingTask.setContent("");
        tasks.addTask(pendingTask);

        int affected = tasks.disablePendingTask(REF_ID, OfcTaskType.SO_CREATE);
        assertThat(affected).isEqualTo(1);
    }

    @Test
    public void should_disable_retry_task() {
        OfcTask pendingTask = new OfcTask();
        pendingTask.setRefId(REF_ID);
        pendingTask.setType(OfcTaskType.SO_CREATE.getValue());
        pendingTask.setStatus(OfcTaskStatus.ERROR.getValue());
        pendingTask.setVenderId(1L);
        pendingTask.setContent("");
        tasks.addTask(pendingTask);

        int affected = tasks.disablePendingTask(REF_ID, OfcTaskType.SO_CREATE);
        assertThat(affected).isEqualTo(1);
    }

    @Test
    public void should_failed_disable_success_task() {
        OfcTask pendingTask = new OfcTask();
        pendingTask.setRefId(REF_ID);
        pendingTask.setType(OfcTaskType.SO_CREATE.getValue());
        pendingTask.setStatus(OfcTaskStatus.SUCEESS.getValue());
        pendingTask.setVenderId(1L);
        pendingTask.setContent("");
        tasks.addTask(pendingTask);

        int affected = tasks.disablePendingTask(REF_ID, OfcTaskType.SO_CREATE);
        assertThat(affected).isEqualTo(0);
    }
}