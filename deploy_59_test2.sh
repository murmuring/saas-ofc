#!/usr/bin/env bash
mvn package -Dmaven.test.skip=true -P test2
scp ofc-provider/target/ofc-provider-1.0-SNAPSHOT-test2.tar.gz work@192.168.60.59:~/lsh-aop/lsh-ofc/ofc-provider
scp ofc-worker/target/ofc-worker-1.0-SNAPSHOT-test2.tar.gz work@192.168.60.59:~/lsh-aop/lsh-ofc/ofc-worker
REMOTE="work@192.168.60.59"
REMOTE_PATH="/home/work/fuhao/lsh-aop/lsh-ofc/ofc-provider"
ssh $REMOTE "sh $REMOTE_PATH/deploy.sh ofc-provider-1.0-SNAPSHOT-test2.tar.gz"
REMOTE_PATH="/home/work/fuhao/lsh-aop/lsh-ofc/ofc-worker"
ssh $REMOTE "sh $REMOTE_PATH/deploy.sh ofc-worker-1.0-SNAPSHOT-test2.tar.gz true"