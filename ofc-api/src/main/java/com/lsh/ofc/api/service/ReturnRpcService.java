package com.lsh.ofc.api.service;

import com.lsh.base.common.exception.BusinessException;
import com.lsh.base.common.model.CommonResult;
import com.lsh.ofc.api.dto.ReturnHeadDTO;
import com.lsh.ofc.api.dto.WumartReturnHeadDTO;

import java.util.List;

/**
 * 返仓单REST服务
 *
 * @author huangdong
 * @date 16/8/28
 */
public interface ReturnRpcService {

    /**
     * 创建返仓RO
     *
     * @param dto
     * @return
     * @throws BusinessException
     */
    CommonResult<List<String>> createRo(ReturnHeadDTO dto) throws BusinessException;

    /**
     * 查询返仓状态
     *
     * @param returnCode
     * @return
     * @throws BusinessException
     */
    CommonResult<Object> queryRoStatus(Long returnCode) throws BusinessException;


    /**
     * 物美返仓的创建
     *
     * @param dto
     * @return
     * @throws BusinessException
     */
    CommonResult<List<String>> createWumartRo(WumartReturnHeadDTO dto) throws BusinessException;
}
