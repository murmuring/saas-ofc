package com.lsh.ofc.api.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

/**
 * Project Name: lsh-ofc
 *
 * @author peter
 * @date 18/11/26
 * 北京链商电子商务有限公司
 * Package
 * desc:
 */
@Setter
@Getter
@NoArgsConstructor
@ToString
public class SupplierSaasDTO implements Serializable {
    private static final long serialVersionUID = 5014647076720469725L;
    /**
     * 区域
     */
    private Integer regionCode;
    /**
     * warehouse code
     */
    private String code;
    /**
     * dc
     */
    private String supplierDc;
    /**
     * venderId
     */
    private Long venderId;

    private String ext;

    /**
     * 货主
     */
    private Integer supplierId;

    private String supplierGroup;

    /**
     * gs
     */
    private Integer supplierOrg;
}
