package com.lsh.ofc.api.service;

import com.lsh.base.common.exception.BusinessException;
import com.lsh.ofc.api.dto.SoHeadDTO;

import java.util.List;

/**
 * Project Name: lsh-ofc
 *
 * @author peter
 * @date 18/5/17
 * 北京链商电子商务有限公司
 * Package
 * desc:
 */
public interface SoService {

    /**
     * 查询SO单信息
     *
     * @param orderCode
     * @return
     * @throws BusinessException
     */
   List<SoHeadDTO> querySignOrders(Long orderCode) throws BusinessException;

    /**
     * 查询SO单信息
     *
     * @param soBillCode
     * @return
     * @throws BusinessException
     */
    SoHeadDTO querySoHeadInfo(String soBillCode) throws BusinessException;

}
