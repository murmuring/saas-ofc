package com.lsh.ofc.api.dto;

import com.lsh.ofc.api.validation.ValidationMessage;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.Valid;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

/**
 * Project Name: WumartReturnHeadDTO
 * 北京链商电子商务有限公司
 * Auth: wangliutao
 * Date: 19/5/22
 * Package Name: com.lsh.ofc.api.dto
 * Description:
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class WumartReturnHeadDTO implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 返仓单号
     */
    @NotNull(message = ValidationMessage.NOT_NULL)
    @Min(value = 0, message = ValidationMessage.ERROR)
    private Long rpoCode;

    /**
     * 物美PO单号
     */
    @NotBlank(message = ValidationMessage.NOT_BLANK)
    private String poCode;

    /**
     * soBillCode
     */
//    @NotBlank(message = ValidationMessage.NOT_BLANK)
//    private String soBillCode;

    /**
     * 仓库编号
     */
//    @NotBlank(message = ValidationMessage.NOT_BLANK)
//    private String warehouseCode;

    /**
     * 返仓单金额
     */
//    @NotNull(message = ValidationMessage.NOT_NULL)
//    @DecimalMin(value = "0", message = ValidationMessage.ERROR)
//    private BigDecimal orderAmount;

    /**
     * 返仓单创建时间
     */
    @NotNull(message = ValidationMessage.NOT_NULL)
    @Min(value = 0, message = ValidationMessage.ERROR)
    private Integer createTime;

    /**
     * 明细列表
     */
    @Valid
    @NotEmpty(message = ValidationMessage.NOT_EMPTY)
    private List<WumartReturnDetailDTO> details;
}
