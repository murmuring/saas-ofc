package com.lsh.ofc.api.dto;

import com.lsh.ofc.api.validation.ValidationMessage;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.Valid;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

/**
 * 返仓单头
 *
 * @author huangdong
 * @date 16/8/28
 */
@ToString
@Setter
@Getter
@NoArgsConstructor
public class ReturnHeadDTO implements Serializable {

    private static final long serialVersionUID = -2231129782398247708L;


    /**
     * 返仓单号
     */
    @NotNull(message = ValidationMessage.NOT_NULL)
    @Min(value = 0, message = ValidationMessage.ERROR)
    private Long returnCode;

    /**
     * 返仓类型 1-现场返仓 2-售后返仓
     */
    @NotNull(message = ValidationMessage.NOT_NULL)
    @Min(value = 0, message = ValidationMessage.ERROR)
    private Integer returnOrderType;

    /**
     * 订单号
     */
    @NotNull(message = ValidationMessage.NOT_NULL)
    @Min(value = 0, message = ValidationMessage.ERROR)
    private Long orderCode;

    /**
     * 地域编号
     */
    @NotNull(message = ValidationMessage.NOT_NULL)
    @Min(value = 0, message = ValidationMessage.ERROR)
    private Integer regionCode;

    /**
     * 地址编号
     */
    @NotNull(message = ValidationMessage.NOT_NULL)
    @Min(value = 0, message = ValidationMessage.ERROR)
    private Long addressCode;

    /**
     * 仓库编号
     */
    @NotBlank(message = ValidationMessage.NOT_BLANK)
    private String warehouseCode;

    /**
     * 仓库名称
     */
    @NotBlank(message = ValidationMessage.NOT_BLANK)
    private String warehouseName;

    /**
     * 返仓单金额
     */
    @NotNull(message = ValidationMessage.NOT_NULL)
    @DecimalMin(value = "0", message = ValidationMessage.ERROR)
    private BigDecimal orderAmount;

    /**
     * 返仓单创建时间
     */
    @NotNull(message = ValidationMessage.NOT_NULL)
    @Min(value = 0, message = ValidationMessage.ERROR)
    private Integer createTime;

    /**
     * 返仓次数
     */
    @NotNull(message = ValidationMessage.NOT_NULL)
    @Min(value = 1, message = ValidationMessage.ERROR)
    @Max(value = 2, message = ValidationMessage.ERROR)
    private Integer count;

    /**
     * 明细列表
     */
    @Valid
    @NotEmpty(message = ValidationMessage.NOT_EMPTY)
    private List<ReturnDetailDTO> details;

    private Long venderId;

}
