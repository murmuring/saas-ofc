package com.lsh.ofc.api.dto;

import com.lsh.ofc.api.validation.ValidationMessage;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * 订单头
 *
 * @author huangdong
 * @date 16/8/28
 */
@Setter
@Getter
@NoArgsConstructor
@ToString
public class OrderHeadShipDTO implements Serializable {

    private static final long serialVersionUID = 9073480600808306620L;
    /**
     * 订单号
     */
    @NotNull(message = ValidationMessage.NOT_NULL)
    @Min(value = 0, message = ValidationMessage.ERROR)
    private Long orderCode;

    private Long venderId;

    private Integer receiptFlag;
}
