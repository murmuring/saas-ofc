package com.lsh.ofc.api.dto;

import com.lsh.ofc.api.validation.ValidationMessage;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

/**
 * 订单头
 *
 * @author huangdong
 * @date 16/8/28
 */
@Setter
@Getter
@NoArgsConstructor
@ToString
public class OrderHeadDTO implements Serializable {

    private static final long serialVersionUID = 9073480600808306620L;

    /**
     * 订单号
     */
    @NotNull(message = ValidationMessage.NOT_NULL)
    @Min(value = 0, message = ValidationMessage.ERROR)
    private Long orderCode;

    @NotNull(message = ValidationMessage.NOT_NULL)
    @Min(value = 0, message = ValidationMessage.ERROR)
    private Long venderId;
    /**
     * 地域编号
     */
    @NotNull(message = ValidationMessage.NOT_NULL)
    @Min(value = 0, message = ValidationMessage.ERROR)
    private Integer regionCode;

    /**
     * 地址编号
     */
    @NotNull(message = ValidationMessage.NOT_NULL)
    @Min(value = 0, message = ValidationMessage.ERROR)
    private Long addressCode;

    /**
     * 地址信息
     */
    @NotBlank(message = ValidationMessage.NOT_BLANK)
    private String addressInfo;

    /**
     * 订单金额
     */
    @NotNull(message = ValidationMessage.NOT_NULL)
    @DecimalMin(value = "0", message = ValidationMessage.ERROR)
    private BigDecimal orderAmount;

    /**
     * 订单创建时间
     */
    @NotNull(message = ValidationMessage.NOT_NULL)
    @Min(value = 0, message = ValidationMessage.ERROR)
    private Integer createTime;

    /**
     *  TODO 云仓 2018-09-13
     *  父单号
     */
    @NotNull(message = ValidationMessage.NOT_NULL)
    @Min(value = 0, message = ValidationMessage.ERROR)
    private Long parentOrderCode;

    @NotBlank(message = ValidationMessage.NOT_BLANK)
    private String orderDc;

    @NotNull(message = ValidationMessage.NOT_NULL)
    @Min(value = 0, message = ValidationMessage.ERROR)
    private Integer providerId;

    @NotBlank(message = ValidationMessage.NOT_BLANK)
    private String parentHaveCloudChildren;

    private String providerName;

    private String orderExt;
    /**
     * 明细列表
     */
    @NotEmpty(message = ValidationMessage.NOT_EMPTY)
    private List<OrderDetailDTO> details;
}
