package com.lsh.ofc.api.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.util.List;

/**
 * 返仓单头
 *
 * @author huangdong
 * @date 16/8/28
 */
@ToString
@Getter
@Setter
@NoArgsConstructor
public class RoQueryDTO implements Serializable {

    private static final long serialVersionUID = -2231129782398247708L;

    /**
     * robillCode
     */
    private List<String> roBillCodes;

}
