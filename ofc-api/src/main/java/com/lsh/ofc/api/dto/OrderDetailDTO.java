package com.lsh.ofc.api.dto;

import com.lsh.ofc.api.validation.ValidationMessage;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 订单明细
 *
 * @author huangdong
 * @date 16/8/28
 */
@Getter
@Setter
@NoArgsConstructor
@ToString
public class OrderDetailDTO implements Serializable {

    private static final long serialVersionUID = -8929615850327664053L;

    /**
     * 明细ID
     */
    @NotNull(message = ValidationMessage.NOT_NULL)
    @Min(value = 0, message = ValidationMessage.ERROR)
    private Long detailId;

    @NotNull(message = ValidationMessage.NOT_NULL)
    @Min(value = 1, message = ValidationMessage.ERROR)
    private Integer itemCode;

    /**
     * 商品编号
     */
    @NotNull(message = ValidationMessage.NOT_NULL)
    @Min(value = 0, message = ValidationMessage.ERROR)
    private Long goodsCode;

    /**
     * 商品名称
     */
    @NotBlank(message = ValidationMessage.NOT_BLANK)
    private String goodsName;

    /**
     * 商品国条码
     */
    @NotBlank(message = ValidationMessage.NOT_BLANK)
    private String goodsBarCode;

    /**
     * 商品类型
     */
    @NotNull(message = ValidationMessage.NOT_NULL)
    @Min(value = 0, message = ValidationMessage.ERROR)
    private Integer goodsType;

    /**
     * 商品售卖单位
     */
    @NotNull(message = ValidationMessage.NOT_NULL)
    @Min(value = 0, message = ValidationMessage.ERROR)
    private BigDecimal goodsSaleUnit;

    /**
     * 商品单价
     */
    @NotNull(message = ValidationMessage.NOT_NULL)
    @DecimalMin(value = "0", message = ValidationMessage.ERROR)
    private BigDecimal goodsPrice;

    /**
     * 商品数量
     */
    @NotNull(message = ValidationMessage.NOT_NULL)
    @DecimalMin(value = "0", message = ValidationMessage.ERROR)
    private BigDecimal goodsQty;

    /**
     * 商品金额
     */
    @NotNull(message = ValidationMessage.NOT_NULL)
    @DecimalMin(value = "0", message = ValidationMessage.ERROR)
    private BigDecimal goodsAmount;

    /**
     * SKU编号
     */
    @NotNull(message = ValidationMessage.NOT_NULL)
    @Min(value = 0, message = ValidationMessage.ERROR)
    private Long skuCode;

    /**
     * SKU数量
     */
    @NotNull(message = ValidationMessage.NOT_NULL)
    @DecimalMin(value = "0", message = ValidationMessage.ERROR)
    private BigDecimal skuQty;


//    @NotNull(message = ValidationMessage.NOT_NULL)
//    @Min(value = 0, message = ValidationMessage.ERROR)
    private Integer isWeighingGoods;

    // 商品附加属性
    private String  goodsExtAttrs;

}
