package com.lsh.ofc.api.service.supplier;

import com.lsh.base.common.exception.BusinessException;
import com.lsh.base.common.model.CommonResult;
import com.lsh.ofc.api.dto.SupplierDTO;
import com.lsh.ofc.api.dto.SupplierSaasDTO;

/**
 * Project Name: lsh-ofc
 *
 * @author peter
 * @date 18/11/26
 * 北京链商电子商务有限公司
 * Package
 * desc:
 */
public interface SupplierService {

    /**
     * 创建SO订单
     *
     * @param supplierDTO
     * @return
     * @throws BusinessException
     */
    CommonResult<Boolean> addCloudSupplier(SupplierDTO supplierDTO) throws BusinessException;

    /**
     * 创建SO订单
     *
     * @param supplierDTO
     * @return
     * @throws BusinessException
     */
    CommonResult<Boolean> addSaasSupplier(SupplierSaasDTO supplierDTO) throws BusinessException;

}
