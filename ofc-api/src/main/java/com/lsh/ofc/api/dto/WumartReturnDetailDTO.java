package com.lsh.ofc.api.dto;

import com.lsh.ofc.api.validation.ValidationMessage;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * Project Name: WumartReturnHeadDTO
 * 北京链商电子商务有限公司
 * Auth: wangliutao
 * Date: 19/5/22
 * Package Name: com.lsh.ofc.api.dto
 * Description:
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class WumartReturnDetailDTO implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * EA单价
     */
//    @NotNull(message = ValidationMessage.NOT_NULL)
//    @DecimalMin(value = "0", message = ValidationMessage.ERROR)
//    private BigDecimal price;

    /**
     * EA总金额
     */
//    @NotNull(message = ValidationMessage.NOT_NULL)
//    @DecimalMin(value = "0", message = ValidationMessage.ERROR)
//    private BigDecimal skuAmount;

    /**
     * SKU编号
     */
    @NotNull(message = ValidationMessage.NOT_NULL)
    private String skuSupplyCode;

    /**
     * SKU数量
     */
    @NotNull(message = ValidationMessage.NOT_NULL)
    @DecimalMin(value = "0", message = ValidationMessage.ERROR)
    private BigDecimal qty;

}
