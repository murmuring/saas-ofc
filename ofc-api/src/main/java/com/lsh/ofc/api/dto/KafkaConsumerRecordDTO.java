package com.lsh.ofc.api.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

/**
 * Project Name: lsh-ofc
 *
 * @author peter
 * @date 19/3/29
 * 北京链商电子商务有限公司
 * Package
 * desc:
 */
@Setter
@Getter
@NoArgsConstructor
@ToString
public class KafkaConsumerRecordDTO implements Serializable {
    private static final long serialVersionUID = 2562268579159069342L;

    private String topic;
//    private byte[] messageByte;
    private String messageId;
    private String message;
    private int partition;
    private long offset;
    private String receiptHandle;
    private long sentTimestamp;
}
