package com.lsh.ofc.proxy.model;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.*;

import java.math.BigDecimal;

/**
 * Project Name: WgPoTransferDetail
 * 北京链商电子商务有限公司
 * Auth: wangliutao
 * Date: 19/3/28
 * Package Name: com.lsh.ofc.proxy.model
 * Description:
 */
@Data
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CreateWgPoTransferReqDetail {
    /**
     * 商品id
     */
    @JSONField(name = "goods_id")
    private String goodsId;

    /**
     * 行号
     */
    @JSONField(name = "line_no")
    private Integer lineNo;

    /**
     * EA数量
     */
    private BigDecimal qty;

    /**
     * EA价格（含税)
     */
    private BigDecimal price;

    /**
     * 收货规格（传1，进销存取自身维护的箱规）
     */
    @JSONField(name = "pack_unit")
    private Integer packUnit;
}
