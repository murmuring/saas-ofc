package com.lsh.ofc.proxy.model;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

/**
 * Project Name: QueryWgSoRespHead
 * 北京链商电子商务有限公司
 * Auth: wangliutao
 * Date: 18/3/27
 * Package Name: com.lsh.ofc.proxy.model
 * Description:
 */
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class QueryWgSoRespDetail {

    /**
     * so单id
     */
    @JSONField(name ="so_id")
    private String soId;

    /**
     * 行号
     */
    @JSONField(name = "line_no")
    private String lineNo;

    /**
     * 商品码
     */
    @JSONField(name ="goods_id")
    private String goodsId;

    /**
     * 单位
     */
    private String unit;

    /**
     * 数量
     */
    private String qty;

    /**
     * 订单价格
     */
    @JSONField(name ="sales_price")
    private String salesPrice;

    /**
     * 实际订单数量
     */
    @JSONField(name ="real_qty")
    private String realQty;

    /**
     * 实际订单价格
     */
    @JSONField(name ="real_price")
    private String realPrice;

    /**
     * 仓库码
     * 注:so单查询,使用的字段
     */
    @JSONField(name ="warehouse_code")
    private String warehouseCode;
}
