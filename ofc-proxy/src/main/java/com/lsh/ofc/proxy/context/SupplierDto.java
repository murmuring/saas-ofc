package com.lsh.ofc.proxy.context;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * Project Name: lsh-ofc
 * Created by peter on 2019-11-08.
 * 北京链商电子商务有限公司
 * Package
 * desc:
 */
@Setter
@Getter
@NoArgsConstructor
@ToString
public class SupplierDto {

    /**
     * 地域编号
     */
    private Integer regionCode;
    /**
     * 编号
     */
    private String supplierCode;
    /**
     * 供货商机构
     */
    private Integer supplierOrg;
    /**
     * SupplierId
     */
    private Integer supplierId;


    private String supplierGroup;
}
