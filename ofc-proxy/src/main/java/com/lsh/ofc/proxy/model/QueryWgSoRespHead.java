package com.lsh.ofc.proxy.model;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

/**
 * Project Name: QueryWgSoRespHead
 * 北京链商电子商务有限公司
 * Auth: wangliutao
 * Date: 18/3/27
 * Package Name: com.lsh.ofc.proxy.model
 * Description:
 */
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class QueryWgSoRespHead {
    /**
     * 客户id
     */
    @JSONField(name ="customer_id")
    private String customerId;

    /**
     * 批次号
     */
    @JSONField(name ="batch_no")
    private String batchNo;

    /**
     * so单id
     */
    @JSONField(name ="so_id")
    private String soId;

    /**
     * 订单状态
     * 注:so单查询,使用的字段
     */
    private String status;

    /**
     * 行项目信息
     */
    private List<QueryWgSoRespDetail> items;
}
