package com.lsh.ofc.proxy.model;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.*;

import java.util.List;

/**
 * Project Name: WgPoTransferHead
 * 北京链商电子商务有限公司
 * @author  wangliutao
 * Date: 19/3/28
 * Package Name: com.lsh.ofc.proxy.model
 * Description:
 */
@Data
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CreatePreWgPoReqHead extends BaseReqHead {

    /**
     * 仓库
     */
    @JSONField(name = "from_warehouse_code")
    private String fromWarehouseCode;

    @JSONField(name = "to_warehouse_code")
    private String toWarehouseCode;

//    /**
//     * 供商id
//     */
//    @JSONField(name = "vender_id")
//    private Integer venderId;
//
//    /**
//     * 货主id
//     */
//    @JSONField(name = "owner_id")
//    private Integer ownerId;

//    /**
//     * 记录号
//     */
//    @JSONField(name = "record_id")
//    private String recordId;
//
//    /**
//     * 物美po单号
//     */
//    @JSONField(name = "wm_po_id")
//    private String wmPoId;

    /**
     * 物美so单号
     */
    @JSONField(name = "so_id")
    private String soId;

    /**
     *
     */
    @JSONField(name = "summary_time")
    private Integer summaryTime;

    /**
     * 订单详情
     */
    @JSONField(name = "details")
    private List<CreatePreWgPoReqDetail> details;
}
