package com.lsh.ofc.proxy.model;

import com.alibaba.fastjson.annotation.JSONField;
import com.lsh.ofc.proxy.dao.base.OpenItem;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;

/**
 * Project Name: lsh-atp
 * @author peter
 * Date: 16/7/16
 * 北京链商电子商务有限公司
 * Package com.lsh.atp.api.model.Hold.
 * desc:预占接口请求对象
 */
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class HoldOpenRequest implements Serializable {

    /**
     * 序列化id
     */
    private static final long serialVersionUID = -8395139533938868928L;

    private Long address_id;
    /**
     * 商品区域码
     */
    @JSONField(name = "zone_id")
    private Long zone_code;

    @JSONField(name = "supplier_group")
    private String supplierGroup;

    /**
     * 是否扣减
     */
    @JSONField(name = "is_decrease")
    private Integer isDecrease;

//    /**
//     * 渠道
//     */
//    private String channel;

    /**
     * 操作流水号
     */
    private String sequence;

//    /**
//     * 预占结束时间
//     */
//    private Long hold_end_time;

    private Long venderId;

    private Long batch_code;

    private Long uid;

    @JSONField(name = "order_id")
    private Long orderId;
    /**
     * 商品列表
     */
    private List<OpenItem> items;

}
