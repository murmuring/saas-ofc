package com.lsh.ofc.proxy.model;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

/**
 * lsh-ofc
 * Created by peter on 16/9/10.
 */
@NoArgsConstructor
@Getter
@Setter
public class CreatePmsSoReqHead {

    /**
     * 上游订单id
     */
    @JSONField(name = "order_id")
    private String orderId;

    /**
     * 上游订单id
     */
    @JSONField(name = "other_id")
    private String soBillCode;
    /**
     * 上游订单id
     */
    @JSONField(name = "so_code")
    private String soCode;
    /**
     * 仓库id
     */
    @JSONField(name = "pre_warehouse_id")
    private String preWarehouseId;

    /**
     * 仓库id
     */
    @JSONField(name = "warehouse_id")
    private String warehouseId;
    /**
     *
     */
    @JSONField(name = "supplier_org")
    private String supplierOrg;
    /**
     *
     */
    @JSONField(name = "order_distribution_type")
    private String orderDistributionType;
    /**
     *
     */
    private String ext;

    /**
     * 行项目
     */
    private List<CreatePmsSoReqDetail> items;
}
