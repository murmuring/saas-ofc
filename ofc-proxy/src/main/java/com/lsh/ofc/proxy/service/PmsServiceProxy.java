package com.lsh.ofc.proxy.service;

import com.alibaba.fastjson.JSON;
import com.lsh.base.common.exception.BusinessException;
import com.lsh.ofc.proxy.enums.DistributionType;
import com.lsh.ofc.proxy.util.HttpClientUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.Consts;
import org.apache.http.HttpEntity;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.message.BasicNameValuePair;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Project Name: lsh-payment
 * Created by peter on 16/11/16.
 * 北京链商电子商务有限公司
 * Package
 * desc:
 */
@Service
public class PmsServiceProxy {

    private final Logger logger = Logger.getLogger(PmsServiceProxy.class);

    @Value("${pms.server.path}")
    private String pmsServerPath;

    private static final String PUSH_ORDER_SO = "/bill/so/add";

    private static final String PUSH_ORDER_STO = "/bill/sto/add";


    public boolean pushOrderSo(List<BasicNameValuePair> pairs, String distributionType) throws BusinessException {
        if(StringUtils.isNotEmpty(distributionType)){
            return false;
        }

        String url = this.pmsServerPath + PUSH_ORDER_SO;
        if (distributionType.equals(DistributionType.SEED_STO.getCode())) {
            url = this.pmsServerPath + PUSH_ORDER_STO;
        }
        logger.info("[pms sto so][POST][URL=" + url + "]" + JSON.toJSONString(pairs));

        HttpEntity entity = new UrlEncodedFormEntity(pairs, Consts.UTF_8);
        String content = HttpClientUtils.post(url, entity).getData();
        Integer ret = JSON.parseObject(content).getInteger("ret");
        return Integer.valueOf(0).equals(ret);
    }


}
