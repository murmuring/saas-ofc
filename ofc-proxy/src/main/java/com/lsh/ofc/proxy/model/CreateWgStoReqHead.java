package com.lsh.ofc.proxy.model;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

/**
 * Project Name: lsh-ofc
 * @author peter
 * Date: 18/6/20
 * Time: 18/6/20.
 * 北京链商电子商务有限公司
 * Package name:com.lsh.ofc.proxy.model.
 * desc:类功能描述
 */
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class CreateWgStoReqHead extends BaseReqHead {
    /**
     * 发货仓库
     */
    @JSONField(name = "send_warehouse_code")
    private String sendWarehouseCode;

    /**
     * 收货仓库
     */
    @JSONField(name = "receive_warehouse_code")
    private String receiveWarehouseCode;

    /**
     * 上游订单号
     */
    @JSONField(name = "order_id")
    private String orderId;

    /**
     * 客户id
     */
    @JSONField(name = "customer_id")
    private String customerId;

    /**
     * 客户名称
     */
    @JSONField(name = "customer_name")
    private String customerName;

    /**
     * 地址
     */
    @JSONField(name = "address_info")
    private String addressInfo;

    /**
     * 货主//TODO 货主(1:物美，2:链商，3:文固)
     */
    @JSONField(name = "owner_id")
    private String ownerId;


    /**
     * 1:不自动收货  0:自动收货
     */
    @JSONField(name = "auto_receive")
    private String autoReceive  = "1" ;


    List<CreateWgStoReqDetail> items;



    int times = 0;

    String wgPath;


    String summaryType;
}
