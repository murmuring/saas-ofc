package com.lsh.ofc.proxy.model;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;

/**
 * lsh-ofc
 * Created by peter on 16/9/10.
 */
@NoArgsConstructor
@Getter
@Setter
public class CreatePmsSoReqDetail {

    @JSONField(name = "other_detail_id")
    private Integer itemNo;

    @JSONField(name = "so_bill_code")
    private String soBillCode;

    @JSONField(name = "supplier_org")
    private Integer supplierOrg;


    @JSONField(name = "supplier_dc")
    private String supplierDc;

    /**
     * 商品码
     */
    @JSONField(name = "item_id")
    private Long skuCode;

    /**
     * 商品码
     */
    @JSONField(name = "sku_id")
    private Long goodsCode;
    /**
     * 商品码
     */
    @JSONField(name = "sku_name")
    private String goodsName;

    /**
     * 商品码
     */
    @JSONField(name = "money")
    private BigDecimal goodsMoney;

    /**
     * 商品码
     */
    private String code;

    /**
     * 销售价
     */
    private BigDecimal price;

    /**
     * 数量
     */
    @JSONField(name = "real_qty")
    private BigDecimal realQty;

    /**
     *
     */
    @JSONField(name = "sku_define")
    private String skuDefine;

    /**
     * 商品售卖单位
     */
    @JSONField(name = "sale_unit")
    private BigDecimal saleUnit;

    /**
     * 扩展信息
     */
    private String ext;

}
