package com.lsh.ofc.proxy.service;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.lsh.base.common.exception.BusinessException;
import com.lsh.base.common.model.CommonResult;
import com.lsh.ofc.proxy.model.*;
import com.lsh.ofc.proxy.util.HttpClientUtils;
import com.lsh.ofc.proxy.util.MethodCallLogCollector;
import org.apache.http.Consts;
import org.apache.http.Header;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.message.BasicHeader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * @author peter
 * @date 18/8/26
 */
@Service
public class WgServiceProxy {

    private final Logger logger = LoggerFactory.getLogger(WgServiceProxy.class);

    private static final String WG_URI_CREATE_SO_URL = "/api/so/create";
    private static final String WG_URI_CREATE_RETURN_URL = "/api/rso/create";
    private static final String WG_URI_CANCEL_SO_URL = "/api/so/cancel";
    private static final String WG_URI_RSO_BACK = "/api/rso/query";
    private static final String WG_URI_STO_CREATE = "/api/to/create";
    private static final String WG_URI_PO_CREATE = "/api/purchaseorder/add";
    private static final String WG_URI_PO_NOTIFY = "/api/purchaseorder/ofcBack";
    private static final String WG_URI_GOOD_WEIGHT = "/api/goodsinfo/getList";
    private static final String WG_URL_BATCH_QUERY = "/api/vender/batchQuery";
    private static final String WG_URI_CREATE_SO_TRANSFER_URL = "/api/so/soObd";
    private static final String WG_URI_CREATE_PO_TRANSFER_URL = "/api/purchaseorder/goodsTransfer";
    private static final String WG_URL_GETVENDERINFO = "/api/vender/getVenderInfo";

    public CreateWgSoRespHead createSo(CreateWgSoReqHead order, String wgPath) throws BusinessException {
        return this.sendOrder(JSON.toJSONString(order), false, wgPath, order.getMisVenderId());
    }

    public CreateWgSoRespHead createRo(CreateWgRoReqHead order, String wgPath) throws BusinessException {
        return this.sendOrder(JSON.toJSONString(order), true, wgPath, order.getMisVenderId());
    }

    /**
     * 请求进销存，创建SO，并直接过账
     *
     * @param soOrder
     * @param wgPath
     * @return
     * @throws BusinessException
     */
    public Boolean sendSoAndTransfer(CreateWgSoTransferReqHead soOrder, String wgPath) throws BusinessException {
        String uri = wgPath + WG_URI_CREATE_SO_TRANSFER_URL;
        String content = JSON.toJSONString(soOrder);
        StringEntity entity = new StringEntity(content, Consts.UTF_8);
        entity.setContentType(ContentType.APPLICATION_JSON.toString());

        MethodCallLogCollector.params(content);
        logger.info("[POST][URI=" + uri + "][Request:" + content + "]");
        CommonResult<String> result = HttpClientUtils.post(uri, entity, this.buildHeaders(soOrder.getMisVenderId()));
        logger.info("[POST][URI=" + uri + "][Response:" + JSON.toJSONString(result) + "]");

        if (!CommonResult.SUCCESS.equals(result.getCode())) {
            throw new BusinessException(CommonResult.ERROR, result.getMessage());
        }

        String data = result.getData();
        JSONObject json = JSONObject.parseObject(data);
        Integer ret = json.getInteger("ret");
        //成功
        if (Integer.valueOf(0).equals(ret)) {
            return true;
        } else {
            throw new BusinessException(CommonResult.ERROR, json.getString("msg"));
        }
    }

    /**
     * 请求进销存，创建PO，并直接过账
     *
     * @param poOrder
     * @param wgPath
     * @return poId
     * @throws BusinessException
     */
    public String sendPoAndTransfer(CreateWgPoTransferReqHead poOrder, String wgPath) throws BusinessException {
        String uri = wgPath + WG_URI_CREATE_PO_TRANSFER_URL;
        String content = JSON.toJSONString(poOrder);
        StringEntity entity = new StringEntity(content, Consts.UTF_8);
        entity.setContentType(ContentType.APPLICATION_JSON.toString());

        MethodCallLogCollector.params(content);
        logger.info("[POST][URI=" + uri + "][Request:" + content + "]");
        CommonResult<String> result = HttpClientUtils.post(uri, entity, this.buildHeaders(poOrder.getMisVenderId()));
        logger.info("[POST][URI=" + uri + "][Response:" + JSON.toJSONString(result) + "]");

        if (!CommonResult.SUCCESS.equals(result.getCode())) {
            throw new BusinessException(CommonResult.ERROR, result.getMessage());
        }

        String data = result.getData();
        JSONObject json = JSONObject.parseObject(data);
        Integer ret = json.getInteger("ret");
        //成功
        if (Integer.valueOf(0).equals(ret)) {
            JSONObject contentJson = json.getJSONObject("content");
            if (contentJson != null) {
                String poId = contentJson.getString("po_id");
                return poId;
            }

            return null;
        } else {
            throw new BusinessException(CommonResult.ERROR, json.getString("msg"));
        }
    }

    public CreateWgSoRespHead createSto(CreateWgStoReqHead stoOrder, String wgPath) throws BusinessException {
        String sto_id = sendSto(stoOrder, wgPath);
        CreateWgSoRespHead createWgSoRespHead = CreateWgSoRespHead.builder()
                .customerId(stoOrder.getCustomerId())
                .warehouseCode(stoOrder.getSendWarehouseCode())
                .orderId(stoOrder.getOrderId())
                .batchNo("0")
                .orderType("3")
                .adressInfo(stoOrder.getAddressInfo())
                .soId(sto_id)
                .items(new ArrayList<CreateWgSoRespDetail>())
                .build();

        for (CreateWgStoReqDetail item : stoOrder.getItems()) {
            CreateWgSoRespDetail createWgSoRespDetail = CreateWgSoRespDetail.builder()
                    .lineNo(item.getLineNo())
                    .goodsId(item.getGoodsId())
                    .qty(item.getQty())
                    .realQty(item.getQty().toString())
                    .warehouseCode(stoOrder.getSendWarehouseCode())
                    .build();
            createWgSoRespHead.getItems().add(createWgSoRespDetail);
        }
        return createWgSoRespHead;
    }

    public String sendSto(CreateWgStoReqHead stoOrder, String wgPath) throws BusinessException {
        String uri = wgPath + WG_URI_STO_CREATE;
        String content = JSON.toJSONString(stoOrder);
        StringEntity entity = new StringEntity(content, Consts.UTF_8);
        entity.setContentType(ContentType.APPLICATION_JSON.toString());

        MethodCallLogCollector.params(content);
        logger.info("[POST][URI=" + uri + "][Request:" + content + "]");
        CommonResult<String> result = HttpClientUtils.post(uri, entity, this.buildHeaders(stoOrder.getMisVenderId()));
        logger.info("[POST][URI=" + uri + "][Response:" + JSON.toJSONString(result) + "]");

        if (!CommonResult.SUCCESS.equals(result.getCode())) {
            throw new BusinessException(CommonResult.ERROR, result.getMessage());
        }

        String data = result.getData();
        JSONObject json = JSONObject.parseObject(data);
        Integer ret = json.getInteger("ret");
        //成功
        if (Integer.valueOf(0).equals(ret)) {
            JSONObject body = JSONObject.parseObject(json.getString("content"));
            return body.getString("sto_id");
        } else {
            throw new BusinessException(CommonResult.ERROR, json.getString("msg"));
        }
    }

    public Boolean sendPrePo(CreatePreWgPoReqHead poOrder, String wgPath) throws BusinessException {
        String uri = wgPath + "/api/sosummary/create";
        String content = JSON.toJSONString(poOrder);
        StringEntity entity = new StringEntity(content, Consts.UTF_8);
        entity.setContentType(ContentType.APPLICATION_JSON.toString());

        MethodCallLogCollector.params(content);
        logger.info("[POST][URI=" + uri + "][Request:" + content + "]");
        CommonResult<String> result = HttpClientUtils.post(uri, entity, this.buildHeaders(poOrder.getMisVenderId()));
        logger.info("[POST][URI=" + uri + "][Response:" + JSON.toJSONString(result) + "]");

        if (!CommonResult.SUCCESS.equals(result.getCode())) {
            throw new BusinessException(CommonResult.ERROR, result.getMessage());
        }

        String data = result.getData();
        JSONObject json = JSONObject.parseObject(data);
        Integer ret = json.getInteger("ret");
        //成功
        if (Integer.valueOf(0).equals(ret)) {
            return true;
        } else {
            throw new BusinessException(CommonResult.ERROR, json.getString("msg"));
        }
    }

//    public Boolean sendPo(CreateWgPoReqHead poOrder, String wgPath) throws BusinessException {
//        String uri = wgPath + WG_URI_PO_CREATE;
//        String content = JSON.toJSONString(poOrder);
//        StringEntity entity = new StringEntity(content, Consts.UTF_8);
//        entity.setContentType(ContentType.APPLICATION_JSON.toString());
//
//        MethodCallLogCollector.params(content);
//        logger.info("[POST][URI=" + uri + "][Request:" + content + "]");
//        CommonResult<String> result = HttpClientUtils.post(uri, entity, this.buildHeaders(poOrder.getMisVenderId()));
//        logger.info("[POST][URI=" + uri + "][Response:" + JSON.toJSONString(result) + "]");
//
//        if (!CommonResult.SUCCESS.equals(result.getCode())) {
//            throw new BusinessException(CommonResult.ERROR, result.getMessage());
//        }
//
//        String data = result.getData();
//        JSONObject json = JSONObject.parseObject(data);
//        Integer ret = json.getInteger("ret");
//        //成功
//        if (Integer.valueOf(0).equals(ret)) {
//            return true;
//        } else {
//            throw new BusinessException(CommonResult.ERROR, json.getString("msg"));
//        }
//    }

    public Boolean sendPoInfo(String content, String wgPath) throws BusinessException {
        String uri = wgPath + WG_URI_PO_NOTIFY;
        StringEntity entity = new StringEntity(content, Consts.UTF_8);
        entity.setContentType(ContentType.APPLICATION_JSON.toString());

        MethodCallLogCollector.params(content);
        logger.info("[POST][URI=" + uri + "][Request:" + content + "]");
        CommonResult<String> result = HttpClientUtils.post(uri, entity, this.buildHeaders(1L));
        logger.info("[POST][URI=" + uri + "][Response:" + JSON.toJSONString(result) + "]");

        if (!CommonResult.SUCCESS.equals(result.getCode())) {
            throw new BusinessException(CommonResult.ERROR, result.getMessage());
        }

        String data = result.getData();
        JSONObject json = JSONObject.parseObject(data);
        Integer ret = json.getInteger("ret");
        //成功
        if (Integer.valueOf(0).equals(ret)) {
            return true;
        } else {
            throw new BusinessException(CommonResult.ERROR, json.getString("msg"));
        }
    }


    /**
     * 取消订单
     *
     * @param soIds
     * @return
     * @throws BusinessException
     */
    public boolean cancelOrder(List<String> soIds, String warehousCode, String wgPath, Long venderId) throws BusinessException {
        String uri = wgPath + WG_URI_CANCEL_SO_URL;
        JSONObject jsonObject = new JSONObject(6);
        jsonObject.put("so_id", soIds);
        jsonObject.put("warehouse_code", warehousCode);
        String content = jsonObject.toJSONString();
        StringEntity entity = new StringEntity(content, Consts.UTF_8);
        entity.setContentType(ContentType.APPLICATION_JSON.toString());

        logger.info("[POST][wg][URI= " + uri + " ][request:" + content + "]");
        CommonResult<String> result = HttpClientUtils.post(uri, entity, this.buildHeaders(venderId));
        logger.info("[POST][wg][URI= " + uri + " ][response:" + JSON.toJSONString(result) + "]");

        if (!CommonResult.SUCCESS.equals(result.getCode())) {
            throw new BusinessException(CommonResult.ERROR, result.getMessage());
        }

        String data = result.getData();
        JSONObject json = JSONObject.parseObject(data);
        int status = json.getInteger("ret").intValue();
        if (status == 0) {
            String details = json.getString("content");

            if (!details.startsWith("{")) {
                return false;
            }
            JSONObject body = JSONObject.parseObject(details);
            if (body.getBoolean("result").booleanValue() == true) {
                return true;
            }
            return false;
        } else {
            return false;
        }
    }

    /**
     * 提交订单
     *
     * @param content
     * @param isReturn 是否是返仓单
     * @param wgPath
     * @return
     * @throws BusinessException
     */
    public CreateWgSoRespHead sendOrder(String content, Boolean isReturn, String wgPath, Long venderId) throws BusinessException {
        String uri = wgPath;
        if (isReturn) {
            uri += WG_URI_CREATE_RETURN_URL;
        } else {
            uri += WG_URI_CREATE_SO_URL;
        }

        StringEntity entity = new StringEntity(content, Consts.UTF_8);
        entity.setContentType(ContentType.APPLICATION_JSON.toString());

        MethodCallLogCollector.params(content);
        logger.info("[POST][URI=" + uri + "][Request:" + content + "]");
        CommonResult<String> result = HttpClientUtils.post(uri, entity, this.buildHeaders(venderId));
        logger.info("[POST][URI=" + uri + "][Response:" + JSON.toJSONString(result) + "]");

        if (!CommonResult.SUCCESS.equals(result.getCode())) {
            throw new BusinessException(CommonResult.ERROR, result.getMessage());
        }

        String data = result.getData();
        JSONObject json = JSONObject.parseObject(data);
        Integer ret = json.getInteger("ret");
        //成功
        if (Integer.valueOf(0).equals(ret)) {
            if (isReturn) {
                return this.getCreateWgSoRespHead4Return(json.getString("content"));
            } else {
                return this.getCreateWgSoRespHead4Create(json.getString("content"));
            }
        } else {
            throw new BusinessException(CommonResult.ERROR, json.getString("msg"));
        }
    }


    private Header[] buildHeaders(Long venderId) {
        if (venderId == null) {
            venderId = 0L;
        }
        Header[] headers = {new BasicHeader("Content-Type", "application/json"),
                new BasicHeader("VENDERID", venderId + "")};
        return headers;
    }

    private CreateWgSoRespHead getCreateWgSoRespHead4Return(String content) throws BusinessException {
        JSONObject body = JSONObject.parseObject(content);

        CreateWgSoRespHead order = new CreateWgSoRespHead();
        order.setSoId(body.getString("so_id"));
        order.setOrderId(body.getString("order_id"));
        order.setCustomerId(body.getString("customer_id"));
        order.setWarehouseCode(body.getString("warehouse_code"));
        order.setRsoId(body.getString("rso_id"));

        JSONArray respItems = body.getJSONArray("items");
        if (respItems.size() == 0) {
            throw new BusinessException(CommonResult.ERROR, "文固返回的行项目为空");
        }
        List<CreateWgSoRespDetail> details = new ArrayList<>(respItems.size());

        Iterator<Object> iterator = respItems.iterator();
        while (iterator.hasNext()) {
            JSONObject respItem = (JSONObject) iterator.next();
            //todo:参数纠正
            CreateWgSoRespDetail item = new CreateWgSoRespDetail();
            item.setLineNo(respItem.getString("line_no"));
            item.setGoodsId(respItem.getString("goods_id"));
            item.setQty(respItem.getBigDecimal("qty"));
            item.setRealQty(respItem.getString("real_qty"));
            item.setRealPrice(respItem.getString("real_price"));
            item.setSalesPrice(respItem.getString("sales_price"));
            item.setWarehouseCode(respItem.getString("warehouse_code"));
            item.setRsoId(respItem.getString("rso_id"));
        }
        order.setItems(details);

        return order;
    }

    private CreateWgSoRespHead getCreateWgSoRespHead4Create(String content) throws BusinessException {
        CreateWgSoRespHead respHead = JSON.parseObject(content, CreateWgSoRespHead.class);

        if (respHead == null) {
            throw new BusinessException(CommonResult.ERROR, "文固返回信息为空");
        }

        if (respHead.getItems() == null || respHead.getItems().size() == 0) {
            throw new BusinessException(CommonResult.ERROR, "文固返回的行项目为空");
        }

        return respHead;
    }

    /**
     * 返仓是否完成
     *
     * @param soCodes
     * @param wgPath
     * @return
     * @throws BusinessException
     */

    public Map<String, Integer> isReturnCompleted(List<String> soCodes, String wgPath, Long venderId) throws BusinessException {
        String uri = wgPath + WG_URI_RSO_BACK;
        JSONObject jsonObject = new JSONObject(2);
        jsonObject.put("rso_id", soCodes);

        String content = jsonObject.toJSONString();
        logger.info("[POST][URI=" + uri + "] venderId {} request: " + content, venderId);
        StringEntity entity = new StringEntity(content, Consts.UTF_8);
        CommonResult<String> result = HttpClientUtils.post(uri, entity, this.buildHeaders(venderId));
        logger.info("[POST][URI=" + uri + "][Response:" + JSON.toJSONString(result) + "]");

        String json = result.getData();
        if (json == null) {
            return null;
        }
        JSONObject obj = JSON.parseObject(json);
        if (obj == null) {
            return null;
        }
        JSONArray body = obj.getJSONArray("content");
        if (body == null || body.size() == 0) {
            return null;
        }

        HashMap<String, Integer> map = new HashMap<>((int) (body.size() / 0.75) + 1);
        for (int i = 0; i < body.size(); i++) {
            JSONObject rsoJson = body.getJSONObject(i);
            String rso_id = rsoJson.getString("rso_id");
            Boolean success = rsoJson.getBoolean("success");

//            Boolean rfFlag = rsoJson.getBoolean("cloud_rpo");
            if (rso_id == null) {
                continue;
            }
            if (success == null) {
                success = false;
            }
            map.put(rso_id, success ? 1 : 0);
//            if (rfFlag == null) {
//                rfFlag = false;
//            }

//            if (success.equals(false)) {
//                map.put(rso_id, 0);
//
//                return map;
//            }
//
//            if (success.equals(true)) {
//
//                if (rfFlag.equals(true)) {
//                    map.put(rso_id, 2);
//                } else {
//                    map.put(rso_id, 1);
//                }
//            }
        }

        return map;
    }


    public String getVenderIdInfo(String path, Long venderId, Integer providerId) {
        String uri = path + WG_URL_GETVENDERINFO;
        Map<String, String> contentMap = new HashMap<>();
        contentMap.put("vender_id", providerId + "");

        String content = JSON.toJSONString(contentMap);
        StringEntity entity = new StringEntity(content, Consts.UTF_8);
        entity.setContentType(ContentType.APPLICATION_JSON.toString());

        logger.info("[POST][URI=" + uri + "][Request:" + content + "]");
        CommonResult<String> result = HttpClientUtils.post(uri, entity, this.buildHeaders(venderId));
        logger.info("[POST][URI=" + uri + "][Response:" + JSON.toJSONString(result) + "]");

        if (!CommonResult.SUCCESS.equals(result.getCode())) {
            throw new BusinessException(CommonResult.ERROR, result.getMessage());
        }

        String data = result.getData();
        JSONObject json = JSONObject.parseObject(data);
        Integer ret = json.getInteger("ret");
        String ownerId = "";
        if (Integer.valueOf(0).equals(ret)) {
            JSONObject goodsJson = json.getJSONObject("content");

            ownerId = goodsJson.getString("owner_id");
        } else {
            throw new BusinessException(CommonResult.ERROR, json.getString("msg"));
        }

        return ownerId;
    }

//    public Map<String, JSONObject> getGoodsInfo(String path, Set<Long> goodsCodes) {
//        String uri = path + WG_URI_GOOD_WEIGHT;
//        Map<String, String> contentMap = new HashMap<>();
//        contentMap.put("goods_ids", JSON.toJSONString(goodsCodes));
////        contentMap.put("owner_id", ownerId);
//
//        String content = JSON.toJSONString(contentMap);
//        StringEntity entity = new StringEntity(content, Consts.UTF_8);
//        entity.setContentType(ContentType.APPLICATION_JSON.toString());
//
//        logger.info("[POST][URI=" + uri + "][Request:" + content + "]");
//        CommonResult<String> result = HttpClientUtils.post(uri, entity, this.buildHeaders());
//        logger.info("[POST][URI=" + uri + "][Response:" + JSON.toJSONString(result) + "]");
//
//        if (!CommonResult.SUCCESS.equals(result.getCode())) {
//            throw new BusinessException(CommonResult.ERROR, result.getMessage());
//        }
//
//        String data = result.getData();
//        JSONObject json = JSONObject.parseObject(data);
//        Integer ret = json.getInteger("ret");
//        Map<String, JSONObject> resMap = new HashMap<>();
//        if (Integer.valueOf(0).equals(ret)) {
//            JSONArray goodsJson = json.getJSONArray("content");
//
//            for (int i = 0; i < goodsJson.size(); i++) {
//                JSONObject goods = goodsJson.getJSONObject(i);
//
//                resMap.put(goods.getString("goods_id"), goods);
//            }
//        } else {
//            throw new BusinessException(CommonResult.ERROR, json.getString("msg"));
//        }
//
//        return resMap;
//    }


//    public Map<String, BigDecimal> getGoodsWeight(String psiPath, String supplyCodes, String ownerId) {
//        Map<String, BigDecimal> goodsWeight = new HashMap<>();
//        String uri = psiPath + WG_URI_GOOD_WEIGHT;
//        Map<String, String> contentMap = new HashMap<>();
//        contentMap.put("goods_ids", supplyCodes);
//        contentMap.put("owner_id", ownerId);
//
//        String content = JSON.toJSONString(contentMap);
//        StringEntity entity = new StringEntity(content, Consts.UTF_8);
//        entity.setContentType(ContentType.APPLICATION_JSON.toString());
//
//        logger.info("[POST][URI=" + uri + "][Request:" + content + "]");
//        CommonResult<String> result = HttpClientUtils.post(uri, entity, this.buildHeaders());
//        logger.info("[POST][URI=" + uri + "][Response:" + JSON.toJSONString(result) + "]");
//
//        if (!CommonResult.SUCCESS.equals(result.getCode())) {
//            throw new BusinessException(CommonResult.ERROR, result.getMessage());
//        }
//
//        String data = result.getData();
//        JSONObject json = JSONObject.parseObject(data);
//        Integer ret = json.getInteger("ret");
//        //成功
//        if (Integer.valueOf(0).equals(ret)) {
//            JSONObject body = JSONObject.parseObject(json.getString("content"));
//            JSONArray goodsArray = body.getJSONArray("list");
//
//            for (int i = 0; i < goodsArray.size(); i++) {
//                JSONObject good = (JSONObject) goodsArray.get(i);
//                goodsWeight.put(good.getString("goods_id"), good.getBigDecimal("third_pack_weight"));
//            }
//        } else {
//            throw new BusinessException(CommonResult.ERROR, json.getString("msg"));
//        }
//        return goodsWeight;
//    }
    //    /**
//     * 查询文固库存
//     *
//     * @param ownerId
//     * @param warehouseCode
//     * @param goodsIds
//     * @return
//     */
//    public List<QueryWgStockResp> queryStock(String ownerId, String warehouseCode, List<Integer> goodsIds) {
//        String uri = this.wgServerProxyPath + WG_QUERY_STOCK;
//        JSONObject jsonObject = new JSONObject();
//        jsonObject.put("owner_id", ownerId);
//        jsonObject.put("warehouse_code", warehouseCode);
//        jsonObject.put("goods_id", JSONArray.toJSONString(goodsIds));
//        StringEntity entity = new StringEntity(jsonObject.toString(), Consts.UTF_8);
//
//        logger.info("[POST][URI=" + uri + "] params:" + entity);
//        CommonResult<String> result = HttpClientUtils.post(uri, entity, this.buildHeaders());
//        logger.info("[POST][URI=" + uri + "][Response:" + JSON.toJSONString(result) + "]");
//
//        if (!CommonResult.SUCCESS.equals(result.getCode())) {
//            throw new BusinessException(CommonResult.ERROR, result.getMessage());
//        }
//
//        String data = result.getData();
//        JSONObject json = JSONObject.parseObject(data);
//        Integer ret = json.getInteger("ret");
//
//        if (Integer.valueOf(0).equals(ret)) { //成功
//            JSONArray jsonArray = json.getJSONArray("content");
//            if (jsonArray == null || jsonArray.size() == 0) {
//                throw new BusinessException(CommonResult.ERROR, "文固返回数据信息为空");
//            }
//
//            List<QueryWgStockResp> queryWgStockRespList = new ArrayList<>(jsonArray.size());
//            Iterator<Object> iterator = jsonArray.iterator();
//            while (iterator.hasNext()) {
//                JSONObject respContent = (JSONObject) iterator.next();
//                QueryWgStockResp wgStockResp = JSON.parseObject(respContent.toJSONString(), QueryWgStockResp.class);
//                queryWgStockRespList.add(wgStockResp);
//            }
//            return queryWgStockRespList;
//        } else {
//            throw new BusinessException(CommonResult.ERROR, json.getString("msg"));
//        }
//    }

//    /**
//     * 销售单--查询
//     *
//     * @param soIds 销售单id的集合
//     * @return
//     */
//    public List<QueryWgSoRespHead> querySoInfo(List<String> soIds) throws BusinessException {
//        String uri = this.wgServerProxyPath + WG_URI_QUERY_SO_INFO;
//        JSONObject jsonObject = new JSONObject();
//        jsonObject.put("so_id", soIds);
//
//        String content = jsonObject.toString();
//        StringEntity entity = new StringEntity(content, Consts.UTF_8);
//
//        logger.info("[POST][URI=" + uri + "] soId:" + soIds);
//        CommonResult<String> result = HttpClientUtils.post(uri, entity, this.buildHeaders());
//        logger.info("[POST][URI=" + uri + "][Response:" + JSON.toJSONString(result) + "]");
//
//        if (!CommonResult.SUCCESS.equals(result.getCode())) {
//            throw new BusinessException(CommonResult.ERROR, result.getMessage());
//        }
//
//        String data = result.getData();
//        JSONObject json = JSONObject.parseObject(data);
//        Integer ret = json.getInteger("ret");
//
//        if (Integer.valueOf(0).equals(ret)) { //成功
//            JSONArray jsonArray = json.getJSONArray("content");
//            if (jsonArray == null || jsonArray.size() == 0) {
//                throw new BusinessException(CommonResult.ERROR, "文固返回数据信息为空");
//            }
//            List<QueryWgSoRespHead> wgSoRespHeadList = new ArrayList<>(jsonArray.size());
//            Iterator<Object> iterator = jsonArray.iterator();
//            while (iterator.hasNext()) {
//                JSONObject respContent = (JSONObject) iterator.next();
//                QueryWgSoRespHead wgSoRespHead = JSON.parseObject(respContent.toJSONString(), QueryWgSoRespHead.class);
//                wgSoRespHeadList.add(wgSoRespHead);
//            }
//
//            return wgSoRespHeadList;
//        } else {
//            throw new BusinessException(CommonResult.ERROR, json.getString("msg"));
//        }
//    }

//    public boolean updateSoStatus(String content) throws BusinessException {
//        String uri = this.wgServerProxyPath + URI_UPDATE_SO_STATUS;
//        logger.info("[POST][URI=" + uri + "]" + content);
//        StringEntity entity = new StringEntity(content, ContentType.APPLICATION_JSON);//解决中文乱码问题
//        String json = HttpClientUtils.post(uri, entity, this.buildHeaders()).getData();
//        if (json == null) {
//            return false;
//        }
//        logger.info("[POST][URI=" + uri + "]" + json);
//        JSONObject obj = JSON.parseObject(json);
//        if (obj == null) {
//            return false;
//        }
//        JSONObject head = obj.getJSONObject("head");
//        if (head == null) {
//            return false;
//        }
//        return Integer.valueOf(1).equals(head.getInteger("status"));
//    }
}
