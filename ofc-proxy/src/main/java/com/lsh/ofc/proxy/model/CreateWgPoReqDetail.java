package com.lsh.ofc.proxy.model;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;

/**
 * Project Name: CreateWgPoReqDetail
 * 北京链商电子商务有限公司
 * Auth: wangliutao
 * Date: 18/8/10
 * Package Name: com.lsh.ofc.proxy.model
 * Description:
 */
@Setter
@Getter
@NoArgsConstructor
public class CreateWgPoReqDetail {

    @JSONField(name = "goods_id")
    private String supplyCode;

    @JSONField(name = "goods_name")
    private String goodsName;

    private BigDecimal qty;

    private String comment;
}
