package com.lsh.ofc.proxy.model;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.*;

import java.math.BigDecimal;

/**
 * lsh-ofc
 *
 * @author peter
 * @date 16/9/10
 */
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
public class CreateWgSoRespDetail {

    /**
     * 行号
     */
    @JSONField(name = "line_no")
    private String lineNo;

    /**
     * 商品码
     */
    @JSONField(name ="goods_id")
    private String goodsId;

    /**
     * 数量
     */
    private BigDecimal qty;

    /**
     * 订单价格
     */
    @JSONField(name ="sales_price")
    private String salesPrice;

    /**
     * 货主(1:北京物美，2:北京链商，3:天津链商, 4:文固)
     * 注:so单创建,使用的字段
     */
    @JSONField(name ="owner_id")
    private String ownerId;

    /**
     * 异常原因
     * 注:so单创建,使用的字段
     */
    private String error;

    /**
     * 建议修改项
     * 注:so单创建,使用的字段
     */
    private String suggestion;

    /**
     * 实际订单数量
     */
    @JSONField(name ="real_qty")
    private String realQty;

    /**
     * 实际订单价格
     */
    @JSONField(name ="real_price")
    private String realPrice;

    /**
     * 仓库码
     * 注:ro单创建,使用的字段
     */
    @JSONField(name ="warehouse_code")
    private String warehouseCode;

    /**
     * 返仓单id
     * 注:ro单创建,使用的字段
     */
    @JSONField(name ="rso_id")
    private String rsoId;
}
