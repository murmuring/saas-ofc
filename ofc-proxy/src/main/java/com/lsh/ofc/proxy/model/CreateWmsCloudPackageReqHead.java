package com.lsh.ofc.proxy.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;
import java.util.Map;

/**
 * Project Name: CreateWmsCloudObdReqHead
 * 北京链商电子商务有限公司
 * Auth: wangliutao
 * Date: 19/4/17
 * Package Name: com.lsh.ofc.proxy.model
 * Description:
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CreateWmsCloudPackageReqHead {

    /**
     * soBillCode
     */
    private String orderOtherId;

    /**
     * 订单类型(1SO单，2 云仓订单)
     */
    private Integer orderType;

    /**
     * 系统来源
     */
    private String sourceSystem;

    /**
     * 系统来源
     */
    private String warehouseCode;

    /**
     * 包裹详情
     * [{"packageNum":"123-1"},{"packageNum":"123-2"}]
     */
//    private List<Map<String, String>> details;
    private List<CreateWmsCloudPackageReqDetail> details;
}
