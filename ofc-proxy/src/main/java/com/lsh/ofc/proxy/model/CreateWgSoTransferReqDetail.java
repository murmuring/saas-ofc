package com.lsh.ofc.proxy.model;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.*;

import java.math.BigDecimal;

/**
 * Project Name: CreateWgSoTransferReqDetail
 * 北京链商电子商务有限公司
 * Auth: wangliutao
 * Date: 19/3/28
 * Package Name: com.lsh.ofc.proxy.model
 * Description:
 */
@Data
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CreateWgSoTransferReqDetail {
    /**
     * 行号
     */
    @JSONField(name = "line_no")
    private String lineNo;

    /**
     * 商品id
     */
    @JSONField(name = "goods_id")
    private String goodsId;

    /**
     * EA数量
     */
    private BigDecimal qty;

    /**
     * EA价格（含税)
     */
    @JSONField(name = "sales_price")
    private BigDecimal salesPrice;

    /**
     * 扩展字段
     */
    private BigDecimal ext;
}
