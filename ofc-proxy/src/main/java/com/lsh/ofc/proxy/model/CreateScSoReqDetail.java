package com.lsh.ofc.proxy.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;

/**
 * Project Name: CreateCloudObdReqDetail
 * 北京链商电子商务有限公司
 * Auth: wangliutao
 * Date: 19/4/17
 * Package Name: com.lsh.ofc.proxy.model
 * Description:
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CreateScSoReqDetail {
    /**
     * 上游细单Id
     */
    private Integer detailOtherId;

    /**
     * 物美码
     */
    private String skuCode;

    /**
     * 进货数
     */
    private BigDecimal orderQty;

    /**
     * 箱规
     */
    private Integer packUnit;

    /**
     * 箱规名称
     */
    private String packName;


    /**
     * 基本单位名称
     */
    private String unitName;

    /**
     * 基本单位转换后数量
     */
    private BigDecimal unitQty;

    /**
     * barcode
     */
    private String code;

    private String skuName;

}
