package com.lsh.ofc.proxy.model;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.*;

/**
 * lsh-ofc
 *
 * @author peter
 * @date 16/9/10
 */
@NoArgsConstructor
@Getter
@Setter
@ToString
public class BaseReqHead {

    @JSONField(name = "mis_vender_id")
    private Long misVenderId;
}
