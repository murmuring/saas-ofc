package com.lsh.ofc.proxy.service;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.lsh.base.common.exception.BusinessException;
import com.lsh.base.common.model.CommonResult;
import com.lsh.ofc.proxy.util.HttpClientUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.Consts;
import org.apache.http.Header;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.message.BasicHeader;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * Project Name: lsh-ofc
 *
 * @author peter
 * @date 19/4/1
 * 北京链商电子商务有限公司
 * Package
 * desc:
 */
@Service
@Slf4j
public class SupplyInfoServiceProxy {

    private static final String GET_BY_SUPPLY_CODES_URI = "/api/lgort/getList";

    @Value("${supply.server.path}")
    private String supplyServerPath;


    public Map<String, Integer> getSupplyInfoByCodes(Set<String> codes, String dc,Long venderId) throws BusinessException {
        String uri = this.supplyServerPath + GET_BY_SUPPLY_CODES_URI;

        if (CollectionUtils.isEmpty(codes)) {
            return Collections.emptyMap();
        }

        JSONObject jsonObject = new JSONObject(4);
        jsonObject.put("depot", dc);
        jsonObject.put("codes", codes);
        String req = jsonObject.toJSONString();

        StringEntity entity = new StringEntity(req, Consts.UTF_8);
        entity.setContentType(ContentType.APPLICATION_JSON.toString());

        log.info("[POST][URI= " + uri + " ][request:" + req + "]");
        CommonResult<String> result = HttpClientUtils.post(uri, entity, this.buildHeaders(venderId));
        log.info("[POST][URI= " + uri + " ][response:" + JSON.toJSONString(result) + "]");

        Map<String, Integer> splitDetailMap = new HashMap<>();
        //解析 返回数据结果
        String message = result.getData();
        if (StringUtils.isEmpty(message)) {
            return Collections.emptyMap();
        }

        JSONObject content = JSON.parseObject(message);
        Integer ret = content.getInteger("ret");
        if (null == ret || ret != 0) {
            return Collections.emptyMap();
        }

        JSONArray data = content.getJSONArray("content");
        if (null == data || data.size() == 0) {
            return Collections.emptyMap();
        }

        if (data.size() != codes.size()) {
            log.info("返回数据数量与查询数据量不一致");
            return Collections.emptyMap();
        }

        for (int i = 0; i < data.size(); i++) {
            JSONObject codeInfo = data.getJSONObject(i);
            Integer lgortType = codeInfo.getInteger("lgortType");
            String code = codeInfo.getString("code");
            if (null == lgortType) {
                log.info("code = " + code + " :没有在库 直流信息");
                return Collections.emptyMap();
            }

            splitDetailMap.put(code, lgortType);
        }

        return splitDetailMap;
    }


    private Header[] buildHeaders(Long venderId) {
        Header[] headers = {new BasicHeader("VENDERID", venderId + "")};
        return headers;
    }
}
