package com.lsh.ofc.proxy.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;

/**
 * Project Name: CreateWmsCloudPackageReqDetail
 * 北京链商电子商务有限公司
 * Auth: wangliutao
 * Date: 19/5/10
 * Package Name: com.lsh.ofc.proxy.model
 * Description:
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CreateWmsCloudPackageReqDetail {
    /**
     * 数量
     */
    private BigDecimal qty;

    /**
     * 类型：1-包裹、2-按品播种
     */
    private Integer type;

    /**
     * 包裹码
     */
    private String packageNum;
}
