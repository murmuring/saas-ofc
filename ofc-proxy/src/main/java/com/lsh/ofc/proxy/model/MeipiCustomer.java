package com.lsh.ofc.proxy.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * Created by huangdong on 16/8/24.
 */
@Getter
@Setter
@ToString
@NoArgsConstructor
public class MeipiCustomer {

    /**
     * 超市名称
     */
    private String marketName;


    /**
     * 收货人
     */
    private String contactName;


    /**
     * 收货电话
     */
    private String contactPhone;

    /**
     * 省
     */
    private String province;

    /**
     * 市
     */
    private String city;

    /**
     * 区
     */
    private String district;

    /**
     * 详细地址
     */
    private String address;


    /**
     * 地域编号
     */
    private Integer regionCode;

    /**
     * SO用户编号
     */
    private String soUserCode;

    /**
     * SO用户地域
     */
    private String soUserRegion;

    /**
     * 供货商机构 (1:物美；2:北京链商；3:天津链商；4:文固)
     */
    private Integer supplyOrg;

    private String supplierGroup;

    private String supplierCode;

    private String dc;
    private Integer supplierId;

    /**
     * 1、链商北京151001 2、链商天津151002  3、链商餐馆151003 4、文固151004 5、自建
     */
    private Integer cusType;
}
