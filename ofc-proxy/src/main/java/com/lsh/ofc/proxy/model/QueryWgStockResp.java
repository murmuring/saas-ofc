package com.lsh.ofc.proxy.model;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;

/**
 * Project Name: QueryWgStockResp
 * 北京链商电子商务有限公司
 * Auth: wangliutao
 * Date: 18/3/26
 * Package Name: com.lsh.ofc.proxy.model
 * Description:查询WG的库存查询,返回的结果CreateWgStockResp
 */
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class QueryWgStockResp {
    @JSONField(name = "goods_id")
    private String goodsid;

    @JSONField(name = "avail_qty")
    private BigDecimal availQty;
}
