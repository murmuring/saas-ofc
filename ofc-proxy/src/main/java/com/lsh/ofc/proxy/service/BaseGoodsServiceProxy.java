package com.lsh.ofc.proxy.service;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.lsh.base.common.exception.BusinessException;
import com.lsh.base.common.model.CommonResult;
import com.lsh.ofc.proxy.model.BaseGoods;
import com.lsh.ofc.proxy.util.HttpClientUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.Consts;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.message.BasicHeader;
import org.apache.http.message.BasicNameValuePair;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.*;

/**
 * 商品服务代理
 *
 * @author huangdong
 * @date 16/8/28
 */
@Service
@Slf4j
public class BaseGoodsServiceProxy {

    @Value("${base.goods.server.path}")
    private String goodsServerPath;

    private static final String GET_BY_GOODS_CODES_URI = "/v1/Mis/Goods/getList";

    private Header[] buildHeaders(Long venderId) {

        Header[] headers = {new BasicHeader("api-version", "1.1"),
                new BasicHeader("platform", "ofc"),
                new BasicHeader("venderId", venderId + ""),
                new BasicHeader("sign", "md5")};

        log.info("goods header is " + JSON.toJSONString(headers));
        return headers;
    }

    /**
     * 根据商品码集合获取商品信息集合
     *
     * @param goodsCodes
     * @return
     * @throws BusinessException
     */
    public Map<Long, BaseGoods> getGoodsInfoMapByGoodsCodes(Collection<Long> goodsCodes,Long venderId) throws BusinessException {
        String uri = this.goodsServerPath + GET_BY_GOODS_CODES_URI;

        if (CollectionUtils.isEmpty(goodsCodes)) {
            return Collections.emptyMap();
        }
        Map<Long,BaseGoods> resultMap = new HashMap<>();
        Map<String,Collection<Long>> queryParams = new HashMap<>();
        queryParams.put("item_id", goodsCodes);
        List<String> columns = new ArrayList<>();
        columns.add("item_id");
        columns.add("purchase_unit");
        columns.add("purchase_unit_nu");
        columns.add("unit");
        List<BasicNameValuePair> pairs = new ArrayList<>(2);
        pairs.add(new BasicNameValuePair("where", JSON.toJSONString(queryParams)));
        pairs.add(new BasicNameValuePair("columns", JSON.toJSONString(columns)));
        log.info("uri = " + uri + " pairs is " + JSON.toJSONString(pairs));
        HttpEntity entity = new UrlEncodedFormEntity(pairs, Consts.UTF_8);
        String result = HttpClientUtils.post(uri, entity, this.buildHeaders(venderId)).getData();
        System.out.print(result);
        log.info("uri = " + uri + " response" + result);
        JSONArray content = JSON.parseObject(result).getJSONArray("content");
        if (content == null || content.size() < goodsCodes.size()) {
            log.error("商品信息不存在！goodsCodes=" + goodsCodes);
            throw new BusinessException(CommonResult.ERROR, "获取商品信息失败");
        }
        List<BaseGoods> baseGoodses = JSON.parseArray(content.toJSONString(), BaseGoods.class);
        for(BaseGoods baseGoods:baseGoodses){
            resultMap.put(baseGoods.getSkuCode(),baseGoods);
        }

        return resultMap;
    }

    public static void main(String args[]){
        BaseGoodsServiceProxy baseGoodsServiceProxy = new BaseGoodsServiceProxy();
        baseGoodsServiceProxy.goodsServerPath = "http://dev.goods-service.wmdev2.lsh123.com";
        List<Long> codes = new ArrayList<>();
        codes.add(377330L);
        BigDecimal a = new BigDecimal(22);
        BigDecimal b = new BigDecimal(2);
        BigDecimal [] tmp = a.divideAndRemainder(b);

        baseGoodsServiceProxy.getGoodsInfoMapByGoodsCodes(codes, 10099L);
    }
}
