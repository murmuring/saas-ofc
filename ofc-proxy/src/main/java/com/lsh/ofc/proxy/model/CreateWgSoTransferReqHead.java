package com.lsh.ofc.proxy.model;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.*;

import java.util.List;

/**
 * Project Name: CreateWgSoTransferReqHead
 * 北京链商电子商务有限公司
 * Auth: wangliutao
 * Date: 19/3/28
 * Package Name: com.lsh.ofc.proxy.model
 * Description:
 */
@Data
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CreateWgSoTransferReqHead extends BaseReqHead {
    /**
     * 客户码
     */
    @JSONField(name = "customer_id")
    private String customerId;

    /**
     * 仓库
     */
    @JSONField(name = "warehouse_code")
    private String warehouseCode;

    /**
     * 上游订单id
     */
    @JSONField(name = "order_id")
    private String orderId;

    /**
     * 订单类型 1：销售，2：预售，3:前置仓订单
     */
    @JSONField(name = "order_type")
    private String orderType;

    /**
     * 地址信息
     */
    @JSONField(name = "address_info")
    private String addressInfo;

    /**
     * 出库单号
     */
    @JSONField(name = "warehouse_order_no")
    private String warehouseOrderNo;

    /**
     * 订单客户id（上游客户）
     */
    @JSONField(name = "order_customer_id")
    private String orderCustomerId;

    /**
     * 订单客户名称（上游客户）
     */
    @JSONField(name = "order_customer_name")
    private String orderCustomerName;

    /**
     * 货主id,文固3，链商2
     */
    @JSONField(name = "owner_id")
    private Integer ownerId;

    /**
     * 1
     */
    private Integer source;

    /**
     * 货主id
     */
    @JSONField(name = "items")
    private List<CreateWgSoTransferReqDetail> details;

}
