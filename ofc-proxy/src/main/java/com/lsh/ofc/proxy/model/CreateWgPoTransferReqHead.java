package com.lsh.ofc.proxy.model;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.*;

import java.util.List;

/**
 * Project Name: WgPoTransferHead
 * 北京链商电子商务有限公司
 * @author  wangliutao
 * Date: 19/3/28
 * Package Name: com.lsh.ofc.proxy.model
 * Description:
 */
@Data
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CreateWgPoTransferReqHead extends BaseReqHead {

    /**
     * 仓库
     */
    @JSONField(name = "warehouse_code")
    private String warehouseCode;

    /**
     * 供商id
     */
    @JSONField(name = "vender_id")
    private Integer venderId;

    /**
     * 货主id
     */
    @JSONField(name = "owner_id")
    private Integer ownerId;

    /**
     * 记录号
     */
    @JSONField(name = "record_id")
    private String recordId;

    /**
     * 物美po单号
     */
    @JSONField(name = "wm_po_id")
    private String wmPoId;

    /**
     * 物美so单号
     */
    @JSONField(name = "wm_so_id")
    private String wmSoId;

    /**
     * 收货时间('2018-05-14 02:24:27’)
     */
    @JSONField(name = "receive_date")
    private String receiveDate;

    @JSONField(name = "is_sync")
    private Integer isSyncLsh;

    @JSONField(name = "supplier_name")
    private String supplierName;

    /**
     * 订单详情
     */
    @JSONField(name = "po_goods_list")
    private List<CreateWgPoTransferReqDetail> details;
}
