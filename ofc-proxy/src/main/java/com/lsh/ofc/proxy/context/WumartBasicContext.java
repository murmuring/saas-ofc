package com.lsh.ofc.proxy.context;

import org.springframework.util.StringUtils;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * Project Name: lsh-ofc
 * @author peter
 * 北京链商电子商务有限公司
 * Desc: 类功能描述
 * Package Name: com.lsh.ofc.proxy.context
 * Time: 2017-07-27 下午2:43

 */
public class WumartBasicContext {

    public static final String USR = "usr";
    public static final String CMP = "cmp";
    public static final String VSB = "vsb";
    public static final String MKT = "mkt";
    public static final String ZONE = "zone";
    public static final String ORDERSOURCE = "orderSource";
    public static final String ISFORCECANCEL = "isForceCancel";
    public static final String TUPREFIX = "tuPrefix";
    public static final String WMSPATH = "wmsPath";
    public static final String CUSTOMERID = "customerId";
    public static final String OWNERID = "ownerId";
    public static final String SC_OWNER_ID = "scOwnerId";

    public static final String REGION = "region";
    public static final String CUSTYPE = "cusType";
    public static final String SUPPLIERORG = "supplierOrg";
    public static final String SUPPLIERID = "supplierId";

    public static final String WUMART_FILL = "wumartFill";
    public static final String WG_FILL = "wgFill";
    public static final String OPEN_API_FILL = "openApiFill";

    public static final String SOURCEDC = "sourceDC";

    public static final String SALE_MODEL = "saleModel";

    public static final String DELIVERY_TYPE = "deliveryType";
    /**
     * 1 表示非预售  2 表示预售
     */
    public static final String SO_ORDER_TYPE = "soOrderType";
    public static final String SO_DISTRIBUTION_TYPE = "soDistributionType";
    public static final String DC = "dc";
    public static final String PARAM = "param";
    private static final Set<String> paramSet = new HashSet<>();

    static {
        paramSet.add(USR);
        paramSet.add(CMP);
        paramSet.add(VSB);
        paramSet.add(MKT);
        paramSet.add(ZONE);
        paramSet.add(ORDERSOURCE);
        paramSet.add(ISFORCECANCEL);
        paramSet.add(TUPREFIX);
        paramSet.add(WMSPATH);
        paramSet.add(CUSTOMERID);
        paramSet.add(OWNERID);
    }

    private Map<String, Object> context;
    private SupplierDto supplierDto;

    public WumartBasicContext() {

        context = new HashMap<>();
        supplierDto = new SupplierDto();

    }

    public WumartBasicContext(Map<String, Object> context) {
        this.context = context;
    }

    public SupplierDto getSupplierDto(){
        return supplierDto;
    }

    /**
     * 根绝客户号类型获取信息
     * @param region
     * @param cusType
     * @return
     */
    public static WumartBasicContext buildContext(Integer region,Integer cusType) {
        WumartBasicContext context = new WumartBasicContext();
        context.setProperty(REGION, region);
        context.setProperty(CUSTYPE, cusType);
        return context;
    }

//    public static WumartBasicContext buildContext(Integer region, String dc,Integer supplierOrg,Integer supplierId) {
//        WumartBasicContext context = new WumartBasicContext();
//        context.setProperty(REGION, region);
//        context.setProperty(DC, dc);
//        context.setProperty(SUPPLIERORG,supplierOrg);
//        context.setProperty(SUPPLIERID,supplierId);
//        return context;
//    }

    public static WumartBasicContext buildContext(Integer region, String code,Integer supplierOrg,Integer supplierId,String supplierGroup) {
        WumartBasicContext context = new WumartBasicContext();
        context.supplierDto.setRegionCode(region);
        context.supplierDto.setSupplierCode(code);
        context.supplierDto.setSupplierId(supplierId);
        context.supplierDto.setSupplierOrg(supplierOrg);
        context.supplierDto.setSupplierGroup(supplierGroup);

        return context;
    }

    public static boolean isContain(WumartBasicContext context) {
        Object paramObj = context.getProperty(PARAM);
        if (paramObj == null) {
            return false;
        }
        String param = String.valueOf(paramObj);
        if (!StringUtils.hasText(param)) {
            return false;
        }
        if (!paramSet.contains(param)) {
            return false;
        }
        return true;
    }

    public int size() {
        return this.context.size();
    }

    public void setProperty(String key, Object value) {
        this.context.put(key, value);
    }

    public Object getProperty(String key) {
        return this.context.get(key);
    }

    public String logInfo() {
        StringBuilder sb = new StringBuilder();
        for(Map.Entry<String, Object> entry :context.entrySet()){
            sb.append(entry.getKey()).append(":").append(entry.getValue()).append(";");
        }
        return sb.toString();
    }

}
