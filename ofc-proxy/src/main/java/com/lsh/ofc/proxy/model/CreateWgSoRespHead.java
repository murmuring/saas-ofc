package com.lsh.ofc.proxy.model;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.*;

import java.util.List;

/**
 * lsh-ofc
 *
 * @author peter
 * @date 16/9/10
 */
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
public class CreateWgSoRespHead {


    /**
     * 客户id
     */
    @JSONField(name ="customer_id")
    private String customerId;

    /**
     * 仓库id
     */
    @JSONField(name ="warehouse_code")
    private String warehouseCode;

    /**
     * 上游订单id
     */
    @JSONField(name ="order_id")
    private String orderId;

    /**
     * 批次号
     */
    @JSONField(name ="batch_no")
    private String batchNo;

    /**
     * 订单类型(1SO单，2供商退货单，3调拨出库单)
     */
    @JSONField(name ="order_type")
    private String orderType;

    /**
     * 地址信息(json)
     */
    @JSONField(name = "address_info")
    private String adressInfo;

    /**
     * 扩展信息
     */
    private String ext;

    /**
     * so单id
     */
    @JSONField(name ="so_id")
    private String soId;

    /**
     * 返仓单id
     * 注:ro单创建,使用的字段
     */
    @JSONField(name ="rso_id")
    private String rsoId;

    /**
     * 行项目信息
     */
    private List<CreateWgSoRespDetail> items;

}
