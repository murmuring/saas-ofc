package com.lsh.ofc.proxy.model;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

/**
 * Project Name: CreateWgPoReqHead
 * 北京链商电子商务有限公司
 * Auth: wangliutao
 * Date: 18/8/10
 * Package Name: com.lsh.ofc.proxy.model
 * Description:
 */
@Setter
@Getter
@NoArgsConstructor
public class CreateWgPoReqHead extends BaseReqHead {
    @JSONField(name = "warehouse_code")
    private String warehouseCode;

    @JSONField(name = "vender_id")
    private String venderId2;

    @JSONField(name = "vender_name")
    private String venderName;

    @JSONField(name = "purchaser_uid")
    private Integer purchaserUid;

    @JSONField(name = "is_gift")
    private Integer isGift;

    @JSONField(name = "is_prepay")
    private Integer isPrepay;

    private Integer status;

    private String comment;

    @JSONField(name = "po_goods_list")
    private List<CreateWgPoReqDetail> details;
}
