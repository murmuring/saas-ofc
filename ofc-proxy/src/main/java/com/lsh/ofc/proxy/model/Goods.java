package com.lsh.ofc.proxy.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Created by huangdong on 16/10/8.
 */
@Setter
@Getter
@NoArgsConstructor
public class Goods {

    /**
     * 商品编号
     */
    private Long goodsCode;

    /**
     * SKU编号
     */
    private Long skuCode;

    /**
     * SKU定义
     */
    private Integer skuDefine;

    /**
     * 供货SKU编号
     */
    private String supplySkuCode;

    private String purchaseUnit;

    private String purchaseUnitNu;

}
