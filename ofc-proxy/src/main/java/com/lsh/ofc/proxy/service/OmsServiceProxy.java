package com.lsh.ofc.proxy.service;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.lsh.base.common.model.CommonResult;
import com.lsh.oms.api.model.order.delivery.OrderSendHeadDTO;
import com.lsh.oms.api.model.status.UpdateSoInfoDto;
import com.lsh.oms.api.model.status.UpdateStatusDto;
import com.lsh.oms.api.service.order.IOrderDeliveryRpcService;
import com.lsh.oms.api.service.order.IOrderPoRpcService;
import com.lsh.oms.api.service.status.IUpdateStatusRpcService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

/**
 * OMS服务代理
 *
 * @author huangdong
 * @date 16/8/28
 */
@Service
public class OmsServiceProxy {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private IUpdateStatusRpcService updateStatusRpcService;

    @Autowired
    private IOrderDeliveryRpcService orderDeliveryRpcService;

    @Autowired
    private IOrderPoRpcService orderPoRpcService;


    /**
     * 更新订单状态
     *
     * @param orderCode
     * @param status
     * @param lack
     * @param soCodes
     * @return
     */
    public boolean updateOrderStatus(Long orderCode, int status, int lack, List<String> soCodes, Set<String> warehouseCodes, String mpCustCode, Integer deliveryType, Integer saleModel) {
        UpdateStatusDto dto = new UpdateStatusDto();
        dto.setErp("OFC");
        dto.setOrderNo(orderCode);
        dto.setStatus(status);
        dto.setLackDeliveryType(lack);
        dto.setSoCode(StringUtils.collectionToDelimitedString(soCodes, ","));
        dto.setWarehouseCode(StringUtils.collectionToDelimitedString(warehouseCodes, ","));
        dto.setSaleModel(saleModel);
        dto.setDeliveryType(deliveryType);
        dto.setMisExt(mpCustCode);

        long start = System.currentTimeMillis();
        logger.info("IUpdateStatusRpcService-IUpdateStatusRpcService start... params:" + JSON.toJSONString(dto));
        CommonResult<?> result = updateStatusRpcService.updateStatusOFC(dto);
        long end = System.currentTimeMillis();
        logger.info("IUpdateStatusRpcService-IUpdateStatusRpcService end, cost[" + (end - start) + "]... result:" + JSON.toJSONString(result));
        Object obj = result.getData();
        return (obj != null && "100000".equals(result.getCode()));
    }

    /**
     * 更新订单soCode
     *
     * @param orderCode
     * @param soCodesMap
     * @return
     */
    public boolean updateOrderInfoOFC(Long orderCode, Map<String, Set<String>> soCodesMap) {
        UpdateSoInfoDto dto = new UpdateSoInfoDto(orderCode, soCodesMap);
        long start = System.currentTimeMillis();
        logger.info("IUpdateStatusRpcService-updateOrderInfoOFC start... params:" + JSON.toJSONString(dto));
        CommonResult<Boolean> result = updateStatusRpcService.updateOrderInfoOFC(dto);
        long end = System.currentTimeMillis();
        logger.info("IUpdateStatusRpcService-updateOrderInfoOFC end, cost[" + (end - start) + "]... result:" + JSON.toJSONString(result));
        Object obj = result.getData();
        return (obj != null && "100000".equals(result.getCode()));
    }

    /**
     * @param requestJson
     * @return
     */
    public boolean createReceipt(JSONObject requestJson) {
        int time = (int) TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis());
        //兼容处理
        String deliveryTime = requestJson.getString("delivery_time");
        if (StringUtils.isEmpty(deliveryTime) || deliveryTime.startsWith("202")) {
            requestJson.put("delivery_time", time);
        }

        OrderSendHeadDTO orderSendHeadDTO = JSON.toJavaObject(requestJson, OrderSendHeadDTO.class);
        long start = System.currentTimeMillis();
        logger.info("ICreateReceiptRpcService-IUpdateStatusRpcService start... params: " + JSON.toJSONString(orderSendHeadDTO));
        CommonResult<Object> result = orderDeliveryRpcService.create(orderSendHeadDTO);
        long end = System.currentTimeMillis();
        logger.info("ICreateReceiptRpcService-IUpdateStatusRpcService end, cost[" + (end - start) + "]... result: " + JSON.toJSONString(result));
        Object obj = result.getData();
        return (obj != null && "100000".equals(result.getCode()));
    }

    /**
     * @param orderCode
     * @param venderId
     * @return
     */
    public boolean addPoTask(Long orderCode, Long venderId) {
        CommonResult<String> commonResult = orderPoRpcService.addPoTask(orderCode, venderId);

        commonResult.getCode();

        return true;
    }

}
