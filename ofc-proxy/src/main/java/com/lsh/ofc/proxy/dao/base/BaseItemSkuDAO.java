package com.lsh.ofc.proxy.dao.base;

/**
 *
 * @author huangdong
 * @date 16/8/28
 */
//@Repository
public class BaseItemSkuDAO {

    private static final String SELECT_TAX = "select wmcode as sku_id,tax from tax_info where market_id=:market_id and wmcode in (:sku_ids)";

    private static final String ATTR_SKU_ID = "sku_id";

    private static final String ATTR_TAX = "tax";

//    @Resource(name = "baseNamedParameterJdbcTemplate")
//    private NamedParameterJdbcTemplate jdbcTemplate;

//    public Map<String, BigDecimal> selectSkuTaxMap(Integer market, Collection<String> wmSkuCodes) {
//        if (CollectionUtils.isEmpty(wmSkuCodes)) {
//            return Collections.emptyMap();
//        }
//        Map<String, Object> params = new HashMap<>(4);
//        params.put("market_id", market);
//        params.put("sku_ids", wmSkuCodes);
//        List<Map<String, String>> list = this.jdbcTemplate.query(SELECT_TAX, params, new RowMapper<Map<String, String>>() {
//            @Override
//            public Map<String, String> mapRow(ResultSet rs, int i) throws SQLException {
//                Map<String, String> map = new HashMap<>(4);
//                map.put(ATTR_SKU_ID, rs.getString(ATTR_SKU_ID));
//                map.put(ATTR_TAX, rs.getString(ATTR_TAX));
//                return map;
//            }
//        });
//
//        Map<String, BigDecimal> map = new HashMap<>();
//        for (Map<String, String> item : list) {
//            BigDecimal tax = new BigDecimal(item.get(ATTR_TAX));
//            String wmCode = item.get(ATTR_SKU_ID);
//            if (tax.compareTo(BigDecimal.ZERO) <= 0) {
//                map.put(wmCode, BigDecimal.ZERO);
//            } else {
//                map.put(wmCode, tax.divide(new BigDecimal("100"),2,BigDecimal.ROUND_HALF_UP));
//            }
//        }
//        return map;
//    }

}
