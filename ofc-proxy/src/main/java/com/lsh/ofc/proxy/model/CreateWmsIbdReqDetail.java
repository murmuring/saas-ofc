package com.lsh.ofc.proxy.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;

/**
 * Project Name: CreateWmsCloudPackageReqDetail
 * 北京链商电子商务有限公司
 * Auth: wangliutao
 * Date: 19/5/10
 * Package Name: com.lsh.ofc.proxy.model
 * Description:
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CreateWmsIbdReqDetail {

    private BigDecimal unitQty;

    private BigDecimal oriQty;

    private String unitName ;

    private BigDecimal orderQty;

    private BigDecimal packUnit;

    private String packName;

    private String skuCode;

    private String detailOtherId ;

}
