package com.lsh.ofc.proxy.enums;

/**
 * Project Name: lsh-ofc
 * Created by peter
 * Date: 18/6/23
 * Time: 18/6/23.
 * 北京链商电子商务有限公司
 * Package name:com.lsh.ofc.core.enums.
 * desc:类功能描述
 */
public enum DistributionType {

    ORDER_SO("so", "SO"),
    SEED_STO("sto", "STO");

    private final String code;

    private final String desc;

    DistributionType(String code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public String getCode() {
        return code;
    }

    public String getDesc() {
        return desc;
    }

}
