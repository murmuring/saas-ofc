package com.lsh.ofc.proxy.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

/**
 * Project Name: CreateCloudObdReqHead
 * 北京链商电子商务有限公司
 * Auth: wangliutao
 * Date: 19/4/17
 * Package Name: com.lsh.ofc.proxy.model
 * Description:
 * @author peter
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CreateScSoReqHead {
    /**
     * soBillCode
     */
    private String orderOtherId;

    /**
     * 参考订单id(用户订单号)
     */
    private String orderOtherRefId;

    /**
     * 货主(1 物美,2链商)
     */
    private Long ownerId;

    /**
     * 送达方名称(ofcConsumer->name1)
     */
    private String deliveryName;

    /**
     * 送达方编码(电话号码)(ofcConsumer->customerNumber)
     */
    private String deliveryCode;

    /**
     * 订单类型(1SO单，2 云仓订单)
     */
    private Integer orderType;

    /**
     * 系统来源
     */
    private String sourceSystem;

    /**
     * 仓库id (黑狗:A001)
     */
    private String warehouseCode;

    /**
     * 供商码
     */
    private String supplierCode;

    /**
     * 供商名称
     */
    private String supplierName;

    /**
     * 行项目信息
     */
    private List<CreateScSoReqDetail> details;
}
