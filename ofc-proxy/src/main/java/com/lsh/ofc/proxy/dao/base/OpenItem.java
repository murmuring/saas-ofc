package com.lsh.ofc.proxy.dao.base;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * Project Name: lsh-atp
 *
 * @author peter
 * @date 16/7/6
 * 北京链商电子商务有限公司
 * Package
 * desc:
 */
@ToString
@Getter
@Setter
public class OpenItem implements Serializable {
    /**
     * 序列化ID
     */
    private static final long serialVersionUID = -8338882831901476666L;
    /**
     *  商品id
     */
    private Long item_id;

    /**
     * 供商id
     */
    private Integer supplier_id;

    /**
     * 商品数量
     */
    private BigDecimal qty;

    /**
     *
     */
    private String item_type;

    @JSONField(name = "sku_id")
    private String skuId;
}
