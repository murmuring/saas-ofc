package com.lsh.ofc.proxy.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

/**
 * Project Name: CreateWmsCloudObdReqHead
 * 北京链商电子商务有限公司
 * Auth: wangliutao
 * Date: 19/4/17
 * Package Name: com.lsh.ofc.proxy.model
 * Description:
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CreateWmsIbdReqHead {

    /**
     *
     */
    private String orderOtherId;

    private String orderOtherRefId = "";

    /**
     * 订单类型(1)
     */
    private Integer orderType=1;

    /**
     *  货主
     */
    private String ownerId = "2";
    /**
     * 系统来源
     */
    private String sourceSystem ="2";

    /**
     * 系统来源
     */
    private String warehouseCode;


    private String supplierName;

    /**
     * 商品详情
     */
    private List<CreateWmsIbdReqDetail> details;
}
