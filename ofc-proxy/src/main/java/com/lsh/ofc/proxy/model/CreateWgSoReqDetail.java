package com.lsh.ofc.proxy.model;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.*;

import java.math.BigDecimal;

/**
 * lsh-ofc
 *
 * @author peter
 * @date 16/9/10
 */
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
public class CreateWgSoReqDetail {

    /**
     * 商品码
     */
    @JSONField(name = "goods_id")
    private String goodsId;

    /**
     * 单位
     */
    private String unit;

    /**
     * 数量
     */
    private BigDecimal qty;

    /**
     * 销售价
     */
    @JSONField(name = "sales_price")
    private String salesPrice;

    /**
     * 货主(1:北京物美，2:北京链商，3:天津链商, 4:文固)
     */
    @JSONField(name = "owner_id")
    private String ownerId;

    /**
     * 行号
     */
    @JSONField(name = "line_no")
    private String lineNo;

    @JSONField(name = "is_weighing_goods")
    private Integer isWeighingGoods;

    /**
     * 扩展信息
     */
    @JSONField(name = "ext")
    private String ext;

    @JSONField(name = "pre_sale")
    private Integer preSale;
}
