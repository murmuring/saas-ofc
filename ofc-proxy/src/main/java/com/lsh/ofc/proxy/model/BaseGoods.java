package com.lsh.ofc.proxy.model;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;

/**
 * Created by huangdong on 16/10/8.
 */
@Setter
@Getter
@NoArgsConstructor
public class BaseGoods {
    @JSONField(name = "item_id")
    Long skuCode;

    @JSONField(name = "purchase_unit")
    String packName;

    @JSONField(name = "purchase_unit_nu")
    BigDecimal packUnit;

    @JSONField(name = "unit")
    String unit;


    @Override
    public String toString() {
        return "BaseGoods{" +
                "skuCode=" + skuCode +
                ", packName='" + packName + '\'' +
                ", packUnit='" + packUnit + '\'' +
                '}';
    }
}
