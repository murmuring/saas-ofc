package com.lsh.ofc.proxy.model;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;

/**
 * Project Name: lsh-ofc
 * Created by fuhao
 * Date: 18/6/20
 * Time: 18/6/20.
 * 北京链商电子商务有限公司
 * Package name:com.lsh.ofc.proxy.model.
 * desc:类功能描述
 * @author peter
 */
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class CreateWgStoReqDetail {
    /**
     * 行号
     */
    @JSONField(name = "line_no")
    private String lineNo;

    /**
     * 商品码
     */
    @JSONField(name = "goods_id")
    private String goodsId;

    /**
     * 单位
     */
    private String unit;

    /**
     * 数量
     */
    private BigDecimal qty;

    /**
     * 扩展信息
     */
    private String ext;

    private Long skuCode;
}
