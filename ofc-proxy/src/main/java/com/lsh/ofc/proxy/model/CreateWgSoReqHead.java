package com.lsh.ofc.proxy.model;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.*;

import java.util.List;

/**
 * lsh-ofc
 *
 * @author peter
 * @date 16/9/10
 */
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
public class CreateWgSoReqHead extends BaseReqHead {

    /**
     * 客户id
     */
    @JSONField(name = "customer_id")
    private String customerId;

    /**
     * 仓库id
     */
    @JSONField(name = "warehouse_code")
    private String warehouseCode;

    /**
     * 上游订单id
     */
    @JSONField(name = "order_id")
    private String orderId;
    /**
     * mis 订单号
     */
    @JSONField(name = "order_other_id")
    private String orderOtherRefId;

    /**
     * 批次号
     */
    @JSONField(name = "batch_no")
    private String batchNo;

    /**
     * 订单类型 1：销售，2：预售，3:前置仓订单
     */
    @JSONField(name = "order_type")
    private String orderType;

    /**
     * 交货时间(时间戳)
     */
    @JSONField(name = "trans_time")
    private String transTime;

    /**
     * 扩展信息
     */
    private String ext;

    /**
     * 地址信息
     */
    @JSONField(name = "address_info")
    private String addressInfo;

    /**
     * 订单客户id（上游客户）
     */
    @JSONField(name = "order_customer_id")
    private String orderCustomerId;

    /**
     * 订单客户名称（上游客户）
     */
    @JSONField(name = "order_customer_name")
    private String orderCustomerName;

    /**
     * 货主(1:北京物美，2:北京链商，3:天津链商, 4:文固)
     */
    @JSONField(name = "owner_id")
    private String ownerId;

    @JSONField(name = "po_id")
    private String poId;

    @JSONField(name = "supplier_code")
    private String supplierCode;

    /**
     * 行项目
     */
    private List<CreateWgSoReqDetail> items;

}
