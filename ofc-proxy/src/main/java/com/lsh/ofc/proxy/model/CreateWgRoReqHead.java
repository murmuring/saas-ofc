package com.lsh.ofc.proxy.model;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.List;

/**
 * lsh-ofc
 *
 * @author peter
 * @date 16/9/10
 */
@Setter
@Getter
@NoArgsConstructor
public class CreateWgRoReqHead extends BaseReqHead {
    /**
     * so单id
     */
    @JSONField(name = "so_id")
    private String soId;
    /**
     * 仓库id
     */
    @JSONField(name = "warehouse_code")
    private String warehouseCode;
    /**
     * 客户id
     */
    @JSONField(name = "customer_id")
    private String customerId;

    /**
     * 上游订单id
     */
    @JSONField(name = "order_id")
    private String orderId;

    @JSONField(name = "order_customer_id")
    private String orderCustomerId;

    @JSONField(name = "order_customer_name")
    private String orderCustomerName;

    @JSONField(name = "is_pre")
    private Integer isPre;

    @JSONField(name = "vender_id")
    private String venderId;

    @JSONField(name = "owner_id")
    private String ownerId;

    @JSONField(name = "order_amount")
    private BigDecimal orderAmount;

    @JSONField(name = "ext")
    private String rsoExt;
    /**
     *
     */
    private List<CreateWgRoReqDetail> items;
}
