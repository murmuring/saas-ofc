package com.lsh.ofc.proxy.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

/**
 *
 * @author huangdong
 * @date 16/8/28
 */
@Setter
@Getter
@NoArgsConstructor
@ToString
public class CreateSoReqHead {

    /**
     * 原SO单号(仅返仓时设置,下单时为空)
     */
    private String soCode;

    /**
     * 订单号
     */
    private String orderCode;

    /**
     * 仓库
     */
    private String warehouse;

    /**
     * 仓库
     */
    private String supplierCode;

    private String supplierGroup;

    /**
     * 地域编号
     */
    private Integer regionCode;

    /**
     * SO用户编号
     */
    private String soUserCode;

    /**
     * 供货商机构
     */
    private Integer supplierOrg;

    /**
     * 货主
     */
    private Integer supplierId;
    /**
     * 履约WMS
     */
    private Integer fulfillWms;

    private Integer lgortType;

    private List<CreateSoReqDetail> details;
}
