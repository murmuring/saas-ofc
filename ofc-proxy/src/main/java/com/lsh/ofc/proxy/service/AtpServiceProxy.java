package com.lsh.ofc.proxy.service;

import com.alibaba.fastjson.JSON;
import com.lsh.atp.api.model.config.ItemPreSellQueryRpcRequest;
import com.lsh.atp.api.model.hold.HoldDetailQueryRequest;
import com.lsh.atp.api.model.hold.HoldDetailQueryResponse;
import com.lsh.atp.api.model.base.ItemDc;
import com.lsh.atp.api.service.config.IItemQtyConfigRpcService;
import com.lsh.atp.api.service.hold.IHoldDatailQueryRpcService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.*;

/**
 * ATP服务代理
 *
 * @author huangdong
 * @date 16/8/28
 */
@Service
public class AtpServiceProxy {

    private final Logger logger = Logger.getLogger(this.getClass());

    @Autowired
    private IHoldDatailQueryRpcService holdDatailQueryRpcService;

    @Autowired
    private IItemQtyConfigRpcService itemQtyConfigRpcService;

    /**
     * 获取订单商品库存扣减信息{SKU编号,{DC,数量}}
     *
     * @param orderCode  订单号
     * @return   库存扣减信息
     */
    public Map<Long, Map<String, BigDecimal>> querySkuDcQtyMap(Long orderCode,String orderClass) {
        HoldDetailQueryRequest req = new HoldDetailQueryRequest();
        req.setSequence(orderCode.toString());
        req.setSequenceType(orderClass);
        //OMS请求扣减库存设置值
        req.setChannel("1");
        long start = System.currentTimeMillis();
        logger.info("IHoldDatailQueryRPCService-queryHoldDetail start... params:" + JSON.toJSONString(req));
        HoldDetailQueryResponse resp = holdDatailQueryRpcService.queryHoldDetail(req);
        long end = System.currentTimeMillis();
        logger.info("IHoldDatailQueryRPCService-queryHoldDetail end, cost[" + (end - start) + "]... result:" + JSON.toJSONString(resp));
        List<ItemDc> items = resp.getItems();
        if (CollectionUtils.isEmpty(items)) {
            return Collections.emptyMap();
        }
        Map<Long, Map<String, BigDecimal>> map = new HashMap<>(4);
        for (ItemDc item : items) {
            Long skuCode = item.getItemId();
            Map<String, BigDecimal> dcQty = map.get(skuCode);
            if (dcQty == null) {
                dcQty = new HashMap<>();
                map.put(skuCode, dcQty);
            }
            String dc = item.getDc();
            Integer supply = item.getSupplyId();
            Integer market = item.getSupplyMarket();
            dc = dc + ":" + market + ":" + supply;
            BigDecimal qty = dcQty.get(dc);
            if (qty == null) {
                qty = item.getQty();
            } else {
                qty = qty.add(item.getQty());
            }
            dcQty.put(dc, qty);
        }
        return map;
    }

    /**
     * 获取订单商品预售信息{SKU编号,{DC,数量}}
     *
     * @return   商品预售信息
     */
    public Map<Long, Integer> querySkuPreSale(String zoneCode,Long venderId,Integer supplyId,Integer supplyMarket,String dc,List<Long>itemIds) {
        //zoneCode,venderId,supplyId,supplyMarket,dcs,itemIds
        ItemPreSellQueryRpcRequest req = new ItemPreSellQueryRpcRequest();
        req.setDcs(Arrays.asList(dc));
        req.setItemIds(itemIds);
        req.setSupplyId(supplyId);
        req.setSupplyMarket(supplyMarket);
        req.setVenderId(venderId);
        req.setZoneCode(zoneCode);

        long start = System.currentTimeMillis();
        logger.info("itemQtyConfigRpcService-judgeItemType start... params:" + JSON.toJSONString(req));
        List<Long> preSaleItems = itemQtyConfigRpcService.judgeItemType(req);
        long end = System.currentTimeMillis();
        logger.info("itemQtyConfigRpcService-judgeItemType end, cost[" + (end - start) + "]... result:" + JSON.toJSONString(preSaleItems));
        if (CollectionUtils.isEmpty(preSaleItems)) {
            return Collections.emptyMap();
        }
        Map<Long,Integer> map = new HashMap<>(4);
        for (Long itemId : preSaleItems) {
            map.put(itemId,1);
        }
        return map;
    }
}
