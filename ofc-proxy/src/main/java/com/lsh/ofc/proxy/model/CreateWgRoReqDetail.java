package com.lsh.ofc.proxy.model;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.*;

import java.math.BigDecimal;

/**
 * lsh-ofc
 *
 * @author peter
 * @date 16/9/10
 */
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
public class CreateWgRoReqDetail {

    /**
     * 商品码
     */
    @JSONField(name = "goods_id")
    private String goodsId;
    /**
     * 单位
     */
    private String unit;
    /**
     * 数量
     */
    private BigDecimal qty;
    /**
     * 行号
     */
    @JSONField(name = "line_no")
    private String lineNo;

    @JSONField(name = "goods_amount")
    private BigDecimal goodsAmount;
}
