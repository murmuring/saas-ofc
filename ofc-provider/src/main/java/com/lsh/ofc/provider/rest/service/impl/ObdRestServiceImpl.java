package com.lsh.ofc.provider.rest.service.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.alibaba.dubbo.rpc.protocol.rest.support.ContentType;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.lsh.base.common.exception.BusinessException;
import com.lsh.base.common.model.CommonResult;
import com.lsh.ofc.core.constant.Constants;
import com.lsh.ofc.core.entity.*;
import com.lsh.ofc.core.enums.*;
import com.lsh.ofc.core.exception.EC;
import com.lsh.ofc.core.model.PackageCodeBo;
import com.lsh.ofc.core.redis.RedisTemplate;
import com.lsh.ofc.core.service.OfcObdService;
import com.lsh.ofc.core.service.OfcSoService;
import com.lsh.ofc.core.service.OfcTaskService;
import com.lsh.ofc.core.util.DateUtil;
import com.lsh.ofc.provider.rest.dto.*;
import com.lsh.ofc.provider.rest.service.ObdRestService;
import com.lsh.ofc.proxy.util.MethodCallLogCollector;
import org.apache.commons.lang.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.math.BigDecimal;
import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * @author peter
 */
@Service(protocol = "rest", validation = "true")
@Path("/obd")
@Consumes({MediaType.APPLICATION_JSON, MediaType.APPLICATION_FORM_URLENCODED, MediaType.TEXT_XML})
@Produces({ContentType.APPLICATION_JSON_UTF_8, ContentType.TEXT_XML_UTF_8})
public class ObdRestServiceImpl implements ObdRestService {

    private static Logger logger = LoggerFactory.getLogger(ObdRestServiceImpl.class);

    @Autowired
    private OfcObdService ofcObdService;

    @Autowired
    private OfcSoService ofcSoService;

    @Autowired
    private OfcTaskService ofcTaskService;

    @Autowired
    private RedisTemplate redisTemplate;

    @POST
    @Path("/push")
    @Override
    public CommonResult<Boolean> push(ObdHeadDTO obdDto) throws BusinessException {
        long start = System.currentTimeMillis();
        try {
            logger.info("[push obd info] {}", JSON.toJSONString(obdDto));
            boolean addTask = true;
            boolean updateQty = true;
            if (obdDto.getMis() != null && obdDto.getMis() == 1) {
                addTask = false;
                updateQty = false;
            }

            MethodCallLogCollector.init();
            MethodCallLogCollector.params(JSON.toJSONString(obdDto));
            List<ObdDetailDTO> list = obdDto.getDetails();
            List<OfcObdDetail> details = new ArrayList<>(list.size());
            BigDecimal totalSkuQty = BigDecimal.ZERO;
            for (ObdDetailDTO item : list) {
                JSONObject detailExtJson = new JSONObject();
                detailExtJson.put(Constants.OBD_D_PACK_NUM, ObjectUtils.toString(item.getPackNum()));
                detailExtJson.put(Constants.OBD_D_BOX_NUM, ObjectUtils.toString(item.getBoxNum()));
                detailExtJson.put(Constants.OBD_D_LEFT_EA_NUM, ObjectUtils.toString(item.getLeftEaNum()));
                detailExtJson.put(Constants.OBD_D_OBD_DETAIL_EXT, ObjectUtils.toString(item.getObdDetailExt()));
                detailExtJson.put(Constants.OBD_D_OBD_DETAIL_DEFINE, ObjectUtils.toString(item.getSkuDefine()));
                // TODO 2018-06-26
                detailExtJson.put(Constants.OBD_D_OBD_BARCODE, ObjectUtils.toString(item.getBarcode()));
                detailExtJson.put(Constants.OBD_D_OBD_PACK_CODE, ObjectUtils.toString(item.getPackCode()));
                detailExtJson.put(Constants.OBD_D_OBD_SKU_NAME, ObjectUtils.toString(item.getSkuName()));
                detailExtJson.put(Constants.OBD_D_OBD_ITEM_NO, ObjectUtils.toString(item.getItemNo()));
                // TODO 2018-09-29
                detailExtJson.put(Constants.OBD_D_OBD_SKU_WEIGHT, ObjectUtils.toString(item.getWeight(), "0.0000"));
                // TODO 2019-05-06 作业模式
                detailExtJson.put(Constants.OBD_D_OBD_TASK_MODEL, ObjectUtils.toString(item.getTaskModel(), "1"));
                detailExtJson.put(Constants.OBD_D_OBD_GOODS_CODE, ObjectUtils.toString(item.getGoodsCode(), ""));
                detailExtJson.put(Constants.OBD_D_OBD_PRE_INFO, ObjectUtils.toString(item.getPreInfo(), ""));

                BigDecimal skuQty = item.getSkuQty();
                OfcObdDetail detail = new OfcObdDetail();
                detail.setSkuSupplyCode(item.getSupplySkuCode());
                if (!addTask) {
                    detailExtJson.put("unconfirmed_deliver_qty", skuQty.toString());
                }
                detail.setSkuDeliverQty(skuQty);
                detail.setExt(detailExtJson.toJSONString());
                details.add(detail);
                totalSkuQty = totalSkuQty.add(skuQty);
            }

            JSONObject headExtJson = new JSONObject();
            int deliveryTime = (int) TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis());
            headExtJson.put(Constants.OBD_H_WAYBILL_CODE, ObjectUtils.toString(obdDto.getWaybillCode()));
            headExtJson.put(Constants.OBD_H_TURNOVER_BOX_NUM, ObjectUtils.toString(obdDto.getTurnoverBoxNum()));
            headExtJson.put(Constants.OBD_H_BOX_NUM, ObjectUtils.toString(obdDto.getBoxNum()));
            headExtJson.put(Constants.OBD_H_DRIVER_INFO, ObjectUtils.toString(obdDto.getDriverInfo()));
            headExtJson.put(Constants.OBD_H_VEHICLE_TYPE, ObjectUtils.toString(obdDto.getVehicleType()));
            headExtJson.put(Constants.OBD_H_VEHICLE_TYPE_DESC, ObjectUtils.toString(obdDto.getVehicleTypeDesc()));
            headExtJson.put(Constants.OBD_H_CARRIER_CODE, ObjectUtils.toString(obdDto.getCarrierCode()));
            headExtJson.put(Constants.OBD_H_CARRIER_NAME, ObjectUtils.toString(obdDto.getCarrierName()));
            headExtJson.put(Constants.OBD_H_CREATE_TIME, ObjectUtils.toString(obdDto.getCreateTime()));
            headExtJson.put(Constants.OBD_H_PICK_TIME, ObjectUtils.toString(obdDto.getPickTime()));
            headExtJson.put(Constants.OBD_H_DELIVERY_TIME, deliveryTime);
            headExtJson.put(Constants.OBD_H_SCATTERED_BOX_NUM, ObjectUtils.toString(obdDto.getScatteredBoxNum()));
            headExtJson.put(Constants.OBD_TOTAL_VOLUME, ObjectUtils.toString(obdDto.getTotalVolume(), "0"));

            headExtJson.put("collect_code", ObjectUtils.toString(obdDto.getCollectCode(), "0"));
            if (obdDto.getPackageCodes() != null) {
                // 包裹码（目前只有云仓有）
                headExtJson.put(Constants.OBD_D_OBD_PACKAGE_CODES, obdDto.getPackageCodes());
            }
            String misExt = obdDto.getMisExt();
            if (!addTask) {
                if (StringUtils.isNotBlank(misExt)) {
                    JSONObject misExtJson = JSON.parseObject(misExt);
                    headExtJson.put(Constants.OBD_D_OBD_PACKAGE_NUM, ObjectUtils.toString(misExtJson.getString(Constants.OBD_D_OBD_PACKAGE_NUM), "0"));
                } else {
                    headExtJson.put(Constants.OBD_D_OBD_PACKAGE_NUM, "0");
                }
            }

            if (StringUtils.isNotBlank(misExt)) {
                JSONObject misExtJson = JSON.parseObject(misExt);

                headExtJson.put(Constants.OBD_D_OBD_RECEIPT_FLAG, ObjectUtils.toString(misExtJson.getString(Constants.OBD_D_OBD_RECEIPT_FLAG), "0"));
            }

            OfcObdHead head = new OfcObdHead();
            head.setFulfillWms(obdDto.getWms());
            /*
             *  TODO: 18/3/30 按照ofc订单号发货，不能按照wms的号发货，多wms时候so可能重复。
             */
            head.setSoBillCode(obdDto.getSoCode());
            head.setObdCode(obdDto.getObdCode());
            head.setWarehouseCode(obdDto.getWarehouseCode());
            head.setTotalSkuDeliverQty(totalSkuQty);
            head.setExt(headExtJson.toJSONString());
            head.setDetails(details);

            this.redisTemplate.set(Constants.OBD_HEAD_EXT_PREFIX + obdDto.getSoCode(), ObjectUtils.toString(obdDto.getObdOrderExt()), 50000000);

            this.ofcObdService.create(head, updateQty, addTask);
            MethodCallLogCollector.result(Boolean.TRUE.toString(), (int) (System.currentTimeMillis() - start));
            return CommonResult.success(true);
        } catch (BusinessException t) {
            logger.error("push obd info" + t.getCode() + ":" + t.getMessage(), t);
            MethodCallLogCollector.except(t, (int) (System.currentTimeMillis() - start));
            return new CommonResult(t.getCode(), "业务处理错误:" + t.getMessage(), false);
        } catch (Exception t) {
            logger.error("push obd info", t);
            MethodCallLogCollector.except(t, (int) (System.currentTimeMillis() - start));
            return new CommonResult(EC.SO_OBD_DEAL_ERROR.getCode(), "业务处理错误:数据处理异常或稍后再试", false);
        } finally {
            MethodCallLogCollector.upload();
            MethodCallLogCollector.clear();
        }
    }

    /**
     * 预发货
     *
     * @param preDto
     * @return
     * @throws BusinessException
     */
    @POST
    @Path("/prePush1")
    @Override
    public CommonResult<Boolean> preparePush(PreObdHeadDTO preDto) throws BusinessException {

        logger.info("[prepare push obd info] {}", JSON.toJSONString(preDto));
        CommonResult<Boolean> commonResult;
        try {
            String orderCode = preDto.getOrderCode();
            OfcSoHead filter = new OfcSoHead();
            //云仓 so bill code 和 orderCode 一样
            filter.setSoBillCode(orderCode);
            OfcSoHead soHead = ofcSoService.findOne(filter, true);
            if (soHead == null) {
                throw EC.ERROR.exception("so 信息不存在 请稍后再试");
            }

            if (soHead.getSoStatus().compareTo(SoStatus.DELIVERED.getValue()) >= 0) {
                throw EC.ERROR.exception("so订单 已发货或已取消 请稍后再试");
            }

            Set<String> aggregateCodes = new HashSet<>();
            ObdHeadDTO obdHeadDTO = this.initObdHeadDTO(soHead, preDto, aggregateCodes);
            // 生成包裹码
//            boolean isScOrder = FulfillChannel.SAAS_SC.getValue().equals(soHead.getFulfillChannel());
            this.setPackageCodes(obdHeadDTO, preDto, aggregateCodes, true);

            commonResult = this.push(obdHeadDTO);

            if (CommonResult.SUCCESS.equals(commonResult.getCode())) {
//                if (isScOrder) {
                this.addTask(soHead.getSoBillCode(), soHead.getVenderId());
//                }
            }
        } catch (BusinessException e) {
            logger.error("prePush obd info " + e.getCode() + ":" + e.getMessage(), e);
            commonResult = CommonResult.error("prePush obd info 业务处理错误 ：" + e.getCode() + ":" + e.getMessage(), false);
        } catch (Exception t) {
            logger.error("prePush obd info", t);
            commonResult = CommonResult.error("prePush obd info 业务处理错误", false);
        }

        return commonResult;
    }

    /**
     * 预发货list
     *
     * @param obdHeads
     * @return
     * @throws BusinessException
     */
    @POST
    @Path("/prePushList1")
    @Override
    public CommonResult<JSONArray> preparePushList(PreObdHeadsDTO obdHeads) throws BusinessException {

        logger.info("[request id {} batch prepare push obd info start] {}", obdHeads.getBatchObdCode(), JSON.toJSONString(obdHeads));
        CommonResult<JSONArray> commonResult;
        JSONArray result = new JSONArray();
        try {
            if (!this.batchLock(obdHeads.getBatchObdCode())) {
                throw EC.OBD_BATCH_DEALING.exception("batchObdCode = " + obdHeads.getBatchObdCode());
            }
            // 请求数据加入redis
            this.process4redis(obdHeads);

            for (PreObdHeadDTO preDto : obdHeads.getObdHeadList()) {
                String orderCode = preDto.getOrderCode();
                OfcSoHead filter = new OfcSoHead();
                //云仓 so bill code 和 orderCode 一样
                filter.setSoBillCode(preDto.getOrderCode());
                OfcSoHead soHead = ofcSoService.findOne(filter, true);
                if (soHead == null) {
                    result.add(this.initOrderResult(orderCode, false, "so 信息不存在 请稍后再试"));
                    continue;
                }
                if (soHead.getSoStatus().compareTo(SoStatus.DELIVERED.getValue()) == 0) {
                    result.add(this.initOrderResult(orderCode, true, "so订单已发货"));
                    continue;
                }
                if (soHead.getSoStatus().compareTo(SoStatus.DELIVERED.getValue()) > 0) {
                    result.add(this.initOrderResult(orderCode, false, "so订单已取消"));
                    continue;
                }
                OfcObdHead obdFilter = new OfcObdHead();
                //云仓 so bill code 和 orderCode 一样
                obdFilter.setSoBillCode(preDto.getOrderCode());
                OfcObdHead obdHead = ofcObdService.findOne(obdFilter, false);
                if (obdHead != null) {
                    result.add(this.initOrderResult(orderCode, true, "so订单已发货"));
                    continue;
                }

                Set<String> aggregateCodes = new HashSet<>();
                ObdHeadDTO obdHeadDTO = this.initObdHeadDTO(soHead, preDto, aggregateCodes);
                // 生成包裹码
                boolean isScOrder = FulfillChannel.SAAS_SC.getValue().equals(soHead.getFulfillChannel());
                this.setPackageCodes(obdHeadDTO, preDto, aggregateCodes, isScOrder);

                CommonResult<Boolean> pushResult = this.push(obdHeadDTO);
                if (CommonResult.SUCCESS.equals(pushResult.getCode())) {
                    result.add(this.initOrderResult(orderCode, true, "ok"));
                    if (isScOrder) {
                        this.addTask(soHead.getSoBillCode(), soHead.getVenderId());
                    }
                } else {
                    result.add(this.initOrderResult(orderCode, false, pushResult.getCode() + ":" + pushResult.getMessage()));
                }
            }
            commonResult = CommonResult.success(result);
        } catch (BusinessException e) {
            logger.error("prePush obd info " + e.getCode() + ":" + e.getMessage(), e);
            commonResult = CommonResult.error("prePush obd info 业务处理错误 ：" + e.getCode() + ":" + e.getMessage(), result);
        } catch (Exception t) {
            logger.error("prePush obd info", t);
            commonResult = CommonResult.error("prePush obd info 业务处理错误", result);
        } finally {
            this.batchUnLock(obdHeads.getBatchObdCode());
        }

        logger.info("[request id {} batch prepare push obd info end] {}", obdHeads.getBatchObdCode(), JSON.toJSONString(commonResult));
        return commonResult;
    }

    /**
     * @param obdHeads
     */
    private void process4redis(PreObdHeadsDTO obdHeads) {
        redisTemplate.set("ofc:mis:batch:obd:" + DateUtil.getDayStr() + ":" + obdHeads.getBatchObdCode(), JSON.toJSONString(obdHeads), 20000000L);
    }

    /**
     * @param batchObdCode
     * @return
     */
    private boolean batchLock(String batchObdCode) {
        String key = "ofc:mis:batch:obd:" + batchObdCode;
        if (StringUtils.isBlank(redisTemplate.get(key))) {
            redisTemplate.set(key, batchObdCode, 100L);
            return true;
        }
        return false;
    }

    /**
     * @param batchObdCode
     */
    private void batchUnLock(String batchObdCode) {
        String key = "ofc:mis:batch:obd:" + batchObdCode;
        redisTemplate.del(key);
    }

    /**
     * @param obdHeadDTO
     * @param preDto
     * @param aggregateCodes
     * @param isScOrder
     */
    private void setPackageCodes(ObdHeadDTO obdHeadDTO, PreObdHeadDTO preDto, Set<String> aggregateCodes, boolean isScOrder) {
//        if (isScOrder) {
        List<String> packageCodes = this.ofcObdService.generatePackageCodes(preDto.getOrderCode(), preDto.getPackageNum() == null ? 0 : preDto.getPackageNum().intValue());
        packageCodes.addAll(aggregateCodes);
        obdHeadDTO.setPackageCodes(packageCodes);
//        }
    }

    /**
     * @param soHead
     * @param preDto
     * @param aggregateCodes
     * @return
     */
    private ObdHeadDTO initObdHeadDTO(OfcSoHead soHead, PreObdHeadDTO preDto, Set<String> aggregateCodes) {

        Map<String, OfcSoDetail> soDetailMap = new HashMap<>((int) (soHead.getDetails().size() / 0.75 + 1));
        Map<Long, OfcSoDetail> goodsCodeMap = new HashMap<>((int) (soHead.getDetails().size() / 0.75 + 1));
        for (OfcSoDetail detail : soHead.getDetails()) {
            goodsCodeMap.put(detail.getGoodsCode(), detail);
            soDetailMap.put(detail.getSkuCode() + ":" + detail.getSkuSupplyCode(), detail);
        }

        ObdHeadDTO obdHeadDTO = new ObdHeadDTO();
        obdHeadDTO.setMis(1);
        obdHeadDTO.setWms(soHead.getFulfillWms());
        obdHeadDTO.setWarehouseCode(soHead.getWarehouseCode());
        obdHeadDTO.setObdCode(preDto.getMisOrderCode());
        obdHeadDTO.setSoCode(preDto.getOrderCode());
        obdHeadDTO.setWaybillCode("0");
        obdHeadDTO.setCarrierCode("0");
        obdHeadDTO.setCarrierName("");
        obdHeadDTO.setBoxNum(1);
        obdHeadDTO.setTurnoverBoxNum(0);
        obdHeadDTO.setScatteredBoxNum(0);
        JSONObject misExt = new JSONObject();
        misExt.put(Constants.OBD_D_OBD_PACKAGE_NUM, preDto.getPackageNum().toString());
        obdHeadDTO.setMisExt(misExt.toJSONString());

        int time = (int) TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis());
        obdHeadDTO.setDeliveryTime(time + "");

        List<ObdDetailDTO> obdDetailDTOS = new ArrayList<>(preDto.getDetails().size());
        for (PreObdDetailDTO misDetailDTO : preDto.getDetails()) {
            ObdDetailDTO obdDetailDTO = new ObdDetailDTO();
            BeanUtils.copyProperties(misDetailDTO, obdDetailDTO);

            obdDetailDTO.setBarcode(misDetailDTO.getBarcode());
            obdDetailDTO.setGoodsCode(misDetailDTO.getGoodsCode());
            Integer taskModel = misDetailDTO.getTaskModel();

            OfcSoDetail soDetail = soDetailMap.get(misDetailDTO.getGoodsCode() + ":" + misDetailDTO.getSupplySkuCode());
            if (soDetail != null && StringUtils.isBlank(obdDetailDTO.getSkuDefine())) {
                JSONObject extJson = JSON.parseObject(soDetail.getExt());
                obdDetailDTO.setSkuDefine(extJson.getString("sku_define"));
            }

            if (null == taskModel) {
                taskModel = TaskModel.TASK_PART.getCode();
            } else if (taskModel.equals(TaskModel.TASK_BOX.getCode())) {
                // 提总商品，生成码
                OfcSoDetail detail = goodsCodeMap.get(misDetailDTO.getGoodsCode());
                if (detail == null) {
                    throw EC.ERROR_PARAMS.exception("该商品不存在，销售码：" + misDetailDTO.getGoodsCode());
                }
                aggregateCodes.add(detail.getSkuCode() + "-" + detail.getGoodsSaleUnit().setScale(2, BigDecimal.ROUND_HALF_UP) + "B");
            }

            obdDetailDTO.setTaskModel(taskModel);
            obdDetailDTO.setPackNum(1);
            JSONObject detailExt = new JSONObject();
            detailExt.put("pre_sku_qty", misDetailDTO.getSkuQty());
            detailExt.put("sale_unit", misDetailDTO.getSaleUnit());
            obdDetailDTO.setObdDetailExt("");
            obdDetailDTO.setPreInfo(detailExt.toJSONString());
            obdDetailDTOS.add(obdDetailDTO);
        }
        obdHeadDTO.setDetails(obdDetailDTOS);

        return obdHeadDTO;
    }

    /**
     * @param orderCode
     * @param flag
     * @param message
     * @return
     */
    private JSONObject initOrderResult(String orderCode, boolean flag, String message) {
        JSONObject order = new JSONObject();
        order.put("orderCode", orderCode);
        order.put("sendFlag", flag);
        order.put("message", message);
        return order;
    }


    @POST
    @Path("/confirm")
    @Override
    public CommonResult<Boolean> confirm(ConfirmObdHeadDTO dto) throws BusinessException {

        CommonResult<Boolean> commonResult;
        try {
            logger.info("[confirm obd info] {}", JSON.toJSONString(dto));

            OfcTask taskFilter = new OfcTask();
            taskFilter.setType(OfcTaskType.OBD_MERGE.getValue());
            taskFilter.setRefId(dto.getOrderCode());

            if (this.ofcTaskService.countTask(taskFilter) > 0) {
                throw EC.ERROR.exception("订单已完成收发货，请勿重复操作");
            }

            OfcSoHead soFilter = new OfcSoHead();
            //云仓 so bill code 和 orderCode 一样
            soFilter.setSoBillCode(String.valueOf(dto.getOrderCode()));
            OfcSoHead soHead = ofcSoService.findOne(soFilter, false);

            if (soHead == null) {
                throw EC.ERROR.exception("so 信息不存在 请稍后再试");
            }

            if (soHead.getSoStatus().compareTo(SoStatus.DELIVERED.getValue()) >= 0) {
                throw EC.ERROR.exception("so订单 已发货或已取消 请稍后再试");
            }

            OfcObdDetail filter = new OfcObdDetail();
            filter.setSoBillCode(String.valueOf(dto.getOrderCode()));
            List<OfcObdDetail> ofcObdDetails = ofcObdService.findDetails(String.valueOf(dto.getOrderCode()));
            Map<Long, OfcObdDetail> detailMap = new HashMap<>();
            //用于发货数量校验
            for (OfcObdDetail ofcObdDetail : ofcObdDetails) {
                detailMap.put(ofcObdDetail.getId(), ofcObdDetail);
            }

            List<ConfirmObdDetailDTO> list = dto.getDetails();
            List<OfcObdDetail> details = new ArrayList<>(list.size());
            BigDecimal totalSkuQty = BigDecimal.ZERO;
            for (ConfirmObdDetailDTO item : list) {

                BigDecimal skuConfirmQty = item.getSkuDeliverQty();
                OfcObdDetail ofcObdDetail = detailMap.get(item.getItemNo());
                BigDecimal skuDeliverQty = ofcObdDetail.getSkuDeliverQty();
                //发货校验
                if (skuDeliverQty == null || skuConfirmQty.compareTo(skuDeliverQty) > 0) {
                    throw EC.ERROR.exception("确认发货数量有误");
                }
                JSONObject extJson = JSON.parseObject(ofcObdDetail.getExt());
                extJson.put("unconfirmed_deliver_qty", skuDeliverQty.toString());

                OfcObdDetail detail = new OfcObdDetail();
                detail.setId(item.getItemNo());
                detail.setSkuDeliverQty(skuConfirmQty);
                detail.setExt(extJson.toJSONString());
                details.add(detail);
                totalSkuQty = totalSkuQty.add(skuConfirmQty);
            }
            OfcObdHead headFilter = new OfcObdHead();
            headFilter.setSoBillCode(String.valueOf(dto.getOrderCode()));
            OfcObdHead ofcObdHead = ofcObdService.findOne(headFilter, false);
            JSONObject headExtJson = JSON.parseObject(ofcObdHead.getExt());
            headExtJson.put(Constants.OBD_H_SCATTERED_BOX_NUM, ObjectUtils.toString(dto.getScatteredBoxNum(), "0"));
//            headExtJson.put(Constants.OBD_D_OBD_SCAN_PACKAGE_NUM, ObjectUtils.toString(dto.getScanPackageNum(), "0"));
            headExtJson.put(Constants.OBD_COLLECTION_LOCATION, ObjectUtils.toString(dto.getCollectionLocation(), ""));
            // 添加扫描确认包裹编码
            headExtJson.put(Constants.OBD_D_OBD_SCAN_PACKAGE_CODES, dto.getScanPackageCodes() == null ? "" : dto.getScanPackageCodes());
            headExtJson.put(Constants.OBD_H_WAYBILL_CODE, dto.getWaybillCode() == null ? "" : dto.getWaybillCode());

            OfcObdHead head = new OfcObdHead();
            head.setOrderCode(dto.getOrderCode());
            head.setExt(headExtJson.toJSONString());
            head.setSoBillCode(String.valueOf(dto.getOrderCode()));
            head.setTotalSkuDeliverQty(totalSkuQty);
            head.setDetails(details);

            this.ofcObdService.confirm(head, false);

            commonResult = CommonResult.success(true);
        } catch (BusinessException ex) {
            logger.error("confirm obd info message : " + ex.getMessage(), ex);

            commonResult = CommonResult.error(ex.getMessage(), false);
        } catch (Throwable t) {
            logger.error("confirm obd info", t);
            commonResult = CommonResult.error("confirm obd info 业务处理错误", false);
        }

        return commonResult;
    }

    /**
     * 推送OBD ConfirmObdHeadDTO
     *
     * @param dto ObdBoxDTO
     * @return
     * @throws BusinessException
     */
    @POST
    @Path("/box/push")
    @Override
    public CommonResult<Boolean> obdBox(ObdBoxDTO dto) throws BusinessException {
        CommonResult<Boolean> commonResult;
        try {
            String dtoJsonString = JSON.toJSONString(dto);
            logger.info("[box obd info] {}", dtoJsonString);

            if (StringUtils.isEmpty(dto.getSoCode())) {
                throw EC.ERROR_PARAMS.exception("soCode不能为空");
            }

            OfcTask taskFilter = new OfcTask();
            taskFilter.setType(OfcTaskType.OBD_MERGE.getValue());
            //云仓 so bill code 和 orderCode 一样
            taskFilter.setRefId(Long.valueOf(dto.getSoCode()));

            if (this.ofcTaskService.countTask(taskFilter) > 0) {
                throw EC.ERROR.exception("订单已完成收发货");
            }

            // 将box push参数，保存到redis
            this.saveObdBoxInfo2Redis(dto.getSoCode(), dtoJsonString);
            OfcObdHead obdFilter = new OfcObdHead();
            obdFilter.setSoBillCode(dto.getSoCode());
            OfcObdHead obdHead = ofcObdService.findOne(obdFilter, true);

            // todo:如果扫描包裹码为空，或者总发货数量为0，则直接生成发货为0的发货单
            if (obdHead == null) {
                boolean isEmpty = false;
                if (CollectionUtils.isEmpty(dto.getDetails())) {
                    isEmpty = true;
                } else {
                    BigDecimal totalQty = BigDecimal.ZERO;
                    for (ObdBoxDetailDTO detail : dto.getDetails()) {
                        totalQty = totalQty.add(detail.getQty());
                    }

                    if (totalQty.compareTo(BigDecimal.ZERO) <= 0) {
                        isEmpty = true;
                    }
                }

                if (isEmpty) {
                    return this.pushEmptyObd(dto);
                } else {
                    throw EC.ERROR.exception("obd 信息不存在 请稍后再试");
                }
            }

            ConfirmObdHeadDTO confirmObdHead = this.buildObdConfirmInfo(dto, obdHead);

            commonResult = this.confirm(confirmObdHead);
        } catch (BusinessException ex) {
            logger.error("[box obd info] : " + ex.getMessage(), ex);
            commonResult = CommonResult.error(ex.getMessage(), false);
        } catch (Throwable t) {
            logger.error("[box obd info]", t);
            commonResult = CommonResult.error("[box obd info] 业务处理错误", false);
        }

        return commonResult;
    }

    /**
     * 将sc confirm的参数保存到redis中
     *
     * @param soBillCode
     * @param content
     */
    private void saveObdBoxInfo2Redis(String soBillCode, String content) {
        try {
            redisTemplate.set(Constants.OBD_BOX_INFO_FROM_SC + soBillCode, content, 604800);
        } catch (Exception e) {
            logger.error("[box obd info] 操作redis异常", e);
        }
    }

    /**
     * 生成发货为0的发货单数据
     *
     * @param dto
     * @return
     */
    private CommonResult pushEmptyObd(ObdBoxDTO dto) {
        logger.info("该订单缺货，没有预发货单，默认取消，订单号：{}", dto.getSoCode());
        OfcSoHead soFilter = new OfcSoHead();
        //云仓 so bill code 和 orderCode 一样
        soFilter.setSoBillCode(String.valueOf(dto.getSoCode()));
        OfcSoHead soHead = ofcSoService.findOne(soFilter, true);

        if (soHead == null) {
            throw EC.ERROR.exception("so 信息不存在 请稍后再试");
        }

        if (soHead.getSoStatus().compareTo(SoStatus.CANCELED.getValue()) == 0) {
            return CommonResult.success(true, "so订单 已取消");
        } else if (soHead.getSoStatus().compareTo(SoStatus.DELIVERED.getValue()) >= 0) {
            throw EC.ERROR.exception("so订单 已发货或已取消 请稍后再试");
        }

        ObdHeadDTO obdHeadDTO = new ObdHeadDTO();
        obdHeadDTO.setMis(0);
        obdHeadDTO.setWms(soHead.getFulfillWms());
        obdHeadDTO.setWarehouseCode(soHead.getWarehouseCode());
        obdHeadDTO.setObdCode(dto.getSoCode());
        obdHeadDTO.setSoCode(dto.getSoCode());
        obdHeadDTO.setWaybillCode("0");
        obdHeadDTO.setCarrierCode("0");
        obdHeadDTO.setCarrierName("");
        obdHeadDTO.setBoxNum(0);
        obdHeadDTO.setTurnoverBoxNum(0);
        obdHeadDTO.setScatteredBoxNum(0);
        JSONObject misExt = new JSONObject();
        misExt.put(Constants.OBD_D_OBD_PACKAGE_NUM, "0");
        obdHeadDTO.setMisExt(misExt.toJSONString());

        int time = (int) TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis());
        obdHeadDTO.setDeliveryTime(time + "");

        List<ObdDetailDTO> obdDetailDTOS = new ArrayList<>();
        for (OfcSoDetail detail : soHead.getDetails()) {
            ObdDetailDTO obdDetailDTO = new ObdDetailDTO();
            obdDetailDTO.setItemNo(String.valueOf(detail.getItemNo()));
            obdDetailDTO.setObdDetailExt("");
            obdDetailDTO.setPackNum(0);
            obdDetailDTO.setSkuDefine("");
            obdDetailDTO.setSkuName(detail.getGoodsName());
            obdDetailDTO.setSkuQty(BigDecimal.ZERO);
            obdDetailDTO.setSupplySkuCode(detail.getSkuSupplyCode());
            obdDetailDTOS.add(obdDetailDTO);
        }
        obdHeadDTO.setDetails(obdDetailDTOS);

        return this.push(obdHeadDTO);
    }

    /**
     * 构建请求confirm()接口的参数
     *
     * @param dto
     * @param ofcObdHead
     * @return
     */
    private ConfirmObdHeadDTO buildObdConfirmInfo(ObdBoxDTO dto, OfcObdHead ofcObdHead) {
        Integer supplierId = ofcObdHead.getSupplierId();
        boolean isEmpty = true;
        List<String> scanPackageCodes = new ArrayList<>();
        Map<String, BigDecimal> aggregateCodeQtyMap = new HashMap<>(); // 提总商品-qty的map
        if (!CollectionUtils.isEmpty(dto.getDetails())) {
            for (ObdBoxDetailDTO detail : dto.getDetails()) {
                if (detail == null || detail.getQty() == null || StringUtils.isEmpty(detail.getScanPackageCode())) {
                    throw EC.ERROR_PARAMS.exception("包裹信息不能为空");
                }

                String scanPackageCode = detail.getScanPackageCode();
                // "B"结尾属于提总的商品，"A"结尾属于包裹
                if (scanPackageCode.endsWith("B")) {
                    String[] split = detail.getScanPackageCode().split("-");
                    if (split.length != 3) {
                        throw EC.ERROR_PARAMS.exception("包裹码不正确，包裹码：" + scanPackageCode);
                    }
                    String skuCode = split[1];
                    BigDecimal saleUnit = new BigDecimal(split[2].replace("B", ""));
                    BigDecimal qty = aggregateCodeQtyMap.get(skuCode);
                    if (qty == null) {
                        qty = detail.getQty().multiply(saleUnit);
                    } else {
                        qty = qty.add(detail.getQty().multiply(saleUnit));
                    }
                    aggregateCodeQtyMap.put(skuCode, qty);
                } else {
                    if (isEmpty && detail.getQty().compareTo(BigDecimal.ZERO) > 0) {
                        isEmpty = false;
                    }
                }

                if (detail.getQty().compareTo(BigDecimal.ZERO) > 0) {
                    // todo：此处将包裹码的前缀去掉，只保留包裹编号
                    scanPackageCodes.add(scanPackageCode.substring(scanPackageCode.indexOf(Constants.MIDDLE_LINE) + 1));
                }
            }
        }

        // 构建 id - OfcObdDetail 的关系； 以及，提总商品：id - OfcObdDetail 的关系
        Map<Long, OfcObdDetail> aggregateIdObdDetailsMap = new HashMap<>(); // 提总商品 id-obdDetail的map
        Map<Long, OfcObdDetail> idObdDetailMap = new HashMap<>();
        for (OfcObdDetail obdDetail : ofcObdHead.getDetails()) {
            JSONObject detailExt = JSON.parseObject(obdDetail.getExt());
            Integer taskModel = detailExt.getInteger("task_model");

            // taskModel = 1；提总作业
            if (taskModel != null && TaskModel.TASK_BOX.getCode().equals(taskModel)) {
                aggregateIdObdDetailsMap.put(obdDetail.getId(), obdDetail);
            }

            idObdDetailMap.put(obdDetail.getId(), obdDetail);
        }

        List<ConfirmObdDetailDTO> detailDTOS = new ArrayList<>(ofcObdHead.getDetails().size());
        // 构建提总到SC01的商品发货行项目信息
        if (!aggregateIdObdDetailsMap.isEmpty()) {
            for (OfcObdDetail detail : aggregateIdObdDetailsMap.values()) {
                // 分摊qty
                String skuCode = String.valueOf(detail.getSkuCode());
                BigDecimal qty = aggregateCodeQtyMap.get(skuCode);
                qty = qty == null ? BigDecimal.ZERO : qty;
                BigDecimal deliveryQty;
                if (qty.compareTo(detail.getSkuDeliverQty()) > 0) {
                    deliveryQty = detail.getSkuDeliverQty();
                    aggregateCodeQtyMap.put(skuCode, qty.subtract(detail.getSkuDeliverQty()));
                } else {
                    deliveryQty = qty;
                    aggregateCodeQtyMap.put(skuCode, BigDecimal.ZERO);
                }

                // 构造detail
                ConfirmObdDetailDTO detailDTO = new ConfirmObdDetailDTO();
                detailDTO.setItemNo(detail.getId());
                detailDTO.setSkuCode(detail.getSkuCode());
                detailDTO.setSkuSupplyCode(detail.getSkuSupplyCode());
                detailDTO.setSkuDeliverQty(deliveryQty);
                detailDTO.setExt(null);

                detailDTOS.add(detailDTO);

                // 将提总商品，从map中移除
                idObdDetailMap.remove(detail.getId());
            }
        }

        // 构建按包裹发货的发货行项目信息
        for (OfcObdDetail detail : idObdDetailMap.values()) {
            ConfirmObdDetailDTO detailDTO = new ConfirmObdDetailDTO();
            detailDTO.setItemNo(detail.getId());
            detailDTO.setSkuCode(detail.getSkuCode());
            detailDTO.setSkuSupplyCode(detail.getSkuSupplyCode());
            if (isEmpty) {
                detailDTO.setSkuDeliverQty(BigDecimal.ZERO);
            } else {
                detailDTO.setSkuDeliverQty(detail.getSkuDeliverQty());
            }
            detailDTO.setExt(null);

            detailDTOS.add(detailDTO);
        }

        ConfirmObdHeadDTO confirmObdHead = new ConfirmObdHeadDTO();
        // 来自ObdBoxDTO的参数
        confirmObdHead.setScanPackageCodes(scanPackageCodes);
        confirmObdHead.setCollectionLocation(dto.getCollectionLocation());
        confirmObdHead.setWaybillCode(dto.getWaybillCode());

        // 来自OfcObdHead的数据
        confirmObdHead.setOrderCode(ofcObdHead.getOrderCode());
        confirmObdHead.setSoBillCode(ofcObdHead.getSoBillCode());
        confirmObdHead.setDetails(detailDTOS);

        return confirmObdHead;
    }

    /**
     * 根据SO单据号
     *
     * @param orderCode
     * @return
     * @throws BusinessException
     */
    @GET
    @Path("/obdInfo")
    @Override
    public CommonResult<ObdHeadVo> obdHeadInfo(@QueryParam("orderCode") String orderCode) throws BusinessException {

        logger.info("obdInfo orderCode is {}", orderCode);

        CommonResult<ObdHeadVo> commonResult;
        ObdHeadVo obdHeadVo = new ObdHeadVo();
        try {
            OfcObdHead filter = new OfcObdHead();
            // TODO
            filter.setSoBillCode(orderCode);
            OfcObdHead ofcObdHead = ofcObdService.findOne(filter, true);
            if (ofcObdHead == null) {
                throw new BusinessException("10010005", "ofcObdHead 不存在");
            }

            BeanUtils.copyProperties(ofcObdHead, obdHeadVo);
            this.buildObdDetailInfo(ofcObdHead, obdHeadVo);

            // 分拣中心上线，兼容老流程；（如果obd中，不包含签，则返回云仓的单号 * package_num）
            JSONObject obdExt = JSON.parseObject(obdHeadVo.getExt());
            List<String> packageCodes = this.rebuildPackageCode(ofcObdHead.getSoBillCode(), obdExt);
            obdExt.put(Constants.OBD_D_OBD_PACKAGE_CODES, packageCodes);
            obdHeadVo.setExt(obdExt.toJSONString());

            commonResult = CommonResult.success(obdHeadVo);
        } catch (BusinessException e) {
            logger.error("业务异常 " + e.getCode(), e);
            commonResult = CommonResult.error(e.getCode() + ":" + e.getMessage(), obdHeadVo);
        } catch (Exception e) {
            logger.error("1001004:系统异常", e);
            commonResult = CommonResult.error("1001004:系统异常", obdHeadVo);
        }

        return commonResult;
    }

    /**
     * 根据SO单据号
     *
     * @param content
     * @return
     * @throws BusinessException
     */
    @POST
    @Path("/obdInfo/list")
    @Override
    public CommonResult<List<ObdHeadVo>> obdHeadListInfo(String content) throws BusinessException {
        logger.info("obdInfo_list orderCodes is {}", content);

        CommonResult<List<ObdHeadVo>> commonResult;
        List<ObdHeadVo> obdHeadVos = new ArrayList<>();
        try {
            List<Long> orderCodes = JSON.parseArray(content, Long.class);
            if (CollectionUtils.isEmpty(orderCodes)) {
                return CommonResult.success(obdHeadVos);
            }

            List<OfcObdHead> ofcObdHeads = ofcObdService.findListByOrderCodes(orderCodes, true);

            if (CollectionUtils.isEmpty(orderCodes)) {
                throw new BusinessException("查询不到任何OBD信息，orderCodes：{}", content);
            }

            for (OfcObdHead ofcObdHead : ofcObdHeads) {
                try {
                    ObdHeadVo obdHeadVo = new ObdHeadVo();
                    BeanUtils.copyProperties(ofcObdHead, obdHeadVo);
                    this.buildObdDetailInfo(ofcObdHead, obdHeadVo);

                    // 分拣中心上线，兼容老流程；（如果obd中，不包含签，则返回云仓的单号 * package_num）
                    JSONObject obdExt = JSON.parseObject(obdHeadVo.getExt());
                    List<String> packageCodes = this.rebuildPackageCode(ofcObdHead.getSoBillCode(), obdExt);
                    obdExt.put(Constants.OBD_D_OBD_PACKAGE_CODES, packageCodes);
                    obdHeadVo.setExt(obdExt.toJSONString());

                    obdHeadVos.add(obdHeadVo);
                } catch (Exception e) {
                    logger.error("系统异常：构建ObdHeadVo异常，orderCode：" + ofcObdHead.getOrderCode(), e);
                }
            }

            commonResult = CommonResult.success(obdHeadVos);
        } catch (BusinessException e) {
            logger.error("业务异常 " + e.getCode(), e);
            commonResult = CommonResult.error(e.getCode() + ":" + e.getMessage(), obdHeadVos);
        } catch (Exception e) {
            logger.error("1001004:系统异常", e);
            commonResult = CommonResult.error("1001004:系统异常", obdHeadVos);
        }

        return commonResult;
    }


    /**
     * 根据订单号，查询包裹码
     *
     * @param content
     * @return
     * @throws BusinessException
     */
    @POST
    @Path("/box/packages")
    @Override
    public CommonResult obdPackageInfos(String content) throws BusinessException {
        logger.info("obdInfo package is {}", content);

        CommonResult commonResult;
        try {
            JSONObject json = JSON.parseObject(content);
            List<Long> orderCodes = JSON.parseArray(json.getString("orderCodes"), Long.class);
            if (CollectionUtils.isEmpty(orderCodes)) {
                throw EC.ERROR_PARAMS.exception("参数不能为空，请检查");
            }

            List<OfcObdHead> ofcObdHeads = ofcObdService.findListByOrderCodes(orderCodes, false);
            if (CollectionUtils.isEmpty(orderCodes)) {
                throw EC.SO_OBD_IS_ERROR.exception("查询不到任何的OBD信息，请确认是否已经预发货");
            }

            Map<Long, OfcObdHead> orderCodeObdMap = new HashMap<>((int) (orderCodes.size() / 0.75) + 1);
            for (OfcObdHead ofcObdHead : ofcObdHeads) {
                orderCodeObdMap.put(ofcObdHead.getOrderCode(), ofcObdHead);
            }

            Map<String, List<String>> data = new HashMap<>((int) (orderCodes.size() / 0.75) + 1);
            for (Long orderCode : orderCodes) {
                OfcObdHead ofcObdHead = orderCodeObdMap.get(orderCode);
                if (ofcObdHead == null) {
                    throw EC.SO_OBD_IS_ERROR.exception("查询不到OBD信息，请确认是否已经预发货！订单号：" + orderCode);
                }

                // 分拣中心上线，兼容老流程；（如果obd中，不包含签，则返回云仓的单号 * package_num）
                JSONObject obdExt = JSON.parseObject(ofcObdHead.getExt());
                List<String> packageCodes = this.rebuildPackageCode(ofcObdHead.getSoBillCode(), obdExt);

                data.put(ofcObdHead.getSoBillCode(), packageCodes);
            }

            commonResult = CommonResult.success(data);
        } catch (BusinessException e) {
            logger.error("业务异常 " + e.getCode(), e);
            commonResult = CommonResult.error(e.getCode() + ":" + e.getMessage(), Collections.emptyList());
        } catch (Exception e) {
            logger.error("1001004:系统异常", e);
            commonResult = CommonResult.error("1001004:系统异常", Collections.emptyList());
        }

        return commonResult;
    }


    /**
     * 根据SO单据号，从redis中查询下发分拣中心的参数和分拣中心回传的参数
     *
     * @param soBillCode
     * @return
     * @throws BusinessException
     */
    @GET
    @Path("/boxObd/param")
    @Override
    public CommonResult queryObdBoxParam(@QueryParam("orderCode") String soBillCode) throws BusinessException {
        try {
            String ofc2sc = redisTemplate.get(Constants.OBD_BOX_INFO_TO_SC + soBillCode);
            String sc2ofc = redisTemplate.get(Constants.OBD_BOX_INFO_FROM_SC + soBillCode);

            Map<String, String> data = new HashMap<>(4);
            data.put("下发SC的参数", ofc2sc);
            data.put("SC回传确认的参数", sc2ofc);
            return CommonResult.success(data);
        } catch (Exception e) {
            logger.info("从redis中，查询box obd参数异常", e);
        }
        return CommonResult.error();
    }

    /**
     * 构建返回的obd detail信息
     *
     * @param ofcObdHead
     * @param obdHeadVo
     * @return
     */
    private void buildObdDetailInfo(OfcObdHead ofcObdHead, ObdHeadVo obdHeadVo) {
        List<ObdDetailVo> details = new ArrayList<>();
        for (OfcObdDetail ofcObdDetail : ofcObdHead.getDetails()) {
            ObdDetailVo obdDetailVo = new ObdDetailVo();
            BeanUtils.copyProperties(ofcObdDetail, obdDetailVo);

            JSONObject ext = JSON.parseObject(ofcObdDetail.getExt());

            Integer taskModel = ext.getInteger("task_model");

            if (null == taskModel || taskModel == 0) {
                taskModel = TaskModel.TASK_PART.getCode();
            }

            obdDetailVo.setTaskModel(taskModel);
            obdDetailVo.setItemNo(ofcObdDetail.getId());

            details.add(obdDetailVo);
        }

        JSONObject ext = JSON.parseObject(ofcObdHead.getExt());

        Integer package_num = ext.getInteger("package_num");
        Integer scanPackageNum = 0;
        if (null == package_num) {
            package_num = 0;
        }
        JSONArray array = ext.getJSONArray("scan_package_codes");

        if (null == array || array.size() == 0) {
            scanPackageNum = 0;
        } else {
            for (int i = 0; i < array.size(); i++) {
                String code = array.getString(i);
                if (code.endsWith("A")) {
                    scanPackageNum++;
                }
            }
        }

        obdHeadVo.setPrePackageNum(package_num);
        obdHeadVo.setScanPackageNum(scanPackageNum);
        obdHeadVo.setDetails(details);
    }

    /**
     * 重新组合包裹码
     * 1、拆零发货：soBillCode-包裹编号A
     * 2、按箱发货：供商-链商商品码-箱规B
     *
     * @param soBillCode
     * @param obdExt
     */
    private List<String> rebuildPackageCode(String soBillCode, JSONObject obdExt) {
        Integer packageNum = obdExt.getInteger(Constants.OBD_D_OBD_PACKAGE_NUM);
        List<String> packageCodes = new ArrayList<>();
        if (packageNum != null && packageNum.intValue() > 0) {
            JSONArray jsonArray = obdExt.getJSONArray(Constants.OBD_D_OBD_PACKAGE_CODES);
            if (CollectionUtils.isEmpty(jsonArray)) {
                for (int i = 1; i <= packageNum; i++) {
                    packageCodes.add(this.ofcObdService.buildPackageCodes(PackageCodeBo.normalTask(soBillCode, i + "A")));
                }
            } else {
                for (Object obj : jsonArray) {
                    String packageCode = (String) obj;
                    if (StringUtils.isEmpty(packageCode)) {
                        continue;
                    }

                    // 此处只返回拆零发货的包裹码，不返回按箱发货的条码
                    if (!packageCode.endsWith("B")) {
                        if (packageCode.contains("-")) {
                            packageCodes.add(packageCode);
                        } else {
                            packageCodes.add(this.ofcObdService.buildPackageCodes(PackageCodeBo.normalTask(soBillCode, packageCode)));
                        }
                    }
                }
            }
        }

        return packageCodes;
    }

    @GET
    @Path("/details")
    @Override
    public CommonResult<List<OfcObdDetail>> details(@QueryParam("soBillCode") String soBillCode) throws
            BusinessException {
        return CommonResult.success(this.ofcObdService.findDetails(soBillCode));
    }

    /**
     * @param orderCode
     */
//    private void addOrUpdateTask(Long orderCode) {
//
//        try {
//
//            List<OfcTask> taskList = ofcTaskService.selectByRefIdAndType(orderCode, OfcTaskType.OBD_BOX_CONFERM_PUSH.getValue());
//
//            if (CollectionUtils.isEmpty(taskList)) {
//                OfcTask task = new OfcTask();
//
//                task.setRefId(orderCode);
//                task.setContent(orderCode + "");
//                task.setValid(Valid.enable.getValue());
//                task.setExecCount(0);
//                task.setStatus(OfcTaskStatus.NEW.getValue());
//                task.setType(OfcTaskType.OBD_BOX_CONFERM_PUSH.getValue());
//
//                ofcTaskService.addTask(task);
//            } else {
//                OfcTask task = taskList.get(0);
//                task.setExecCount(0);
//                task.setStatus(OfcTaskStatus.NEW.getValue());
//
//                ofcTaskService.updateTask(task, false);
//            }
//
//        } catch (Exception e) {
//            logger.error("", e);
//        }
//    }

    /**
     * @param orderCode
     */
    private void addTask(String orderCode, Long venderId) {

        try {
            OfcTask task = new OfcTask();
            task.setRefId(Long.valueOf(orderCode));
            task.setType(OfcTaskType.OBD_BOX_PUSH.getValue());
            task.setValid(Valid.enable.getValue());
            if (ofcTaskService.countTask(task) <= 0) {
                task.setContent(orderCode + "");
                task.setExecCount(0);
                task.setStatus(OfcTaskStatus.NEW.getValue());
                task.setVenderId(venderId);
                ofcTaskService.addTask(task);
            }

        } catch (Exception e) {
            logger.error("插入OFC任务【" + OfcTaskType.OBD_BOX_PUSH.getValue() + "】失败", e);
        }
    }

}
