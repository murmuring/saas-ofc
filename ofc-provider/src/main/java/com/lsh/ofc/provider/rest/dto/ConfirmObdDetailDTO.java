package com.lsh.ofc.provider.rest.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * OBD明细
 *
 * @author peter
 * @date 16/9/5
 */
@Setter
@Getter
@NoArgsConstructor
@ToString
public class ConfirmObdDetailDTO implements Serializable {

    private static final long serialVersionUID = -3380475657461957950L;

    private Long itemNo;

    /**
     * SKU编号
     */
    private Long skuCode;

    /**
     * SKU供货码
     */
    private String skuSupplyCode;

    /**
     * SKU发货总数
     */
    private BigDecimal skuDeliverQty;

    /**
     * 其它信息
     */
    private String ext;
}
