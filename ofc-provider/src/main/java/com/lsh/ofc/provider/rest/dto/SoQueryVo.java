package com.lsh.ofc.provider.rest.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;

/**
 * OBD头DTO
 *
 * @author huangdong
 * @date 16/9/5
 */
@Setter
@Getter
@NoArgsConstructor
public class SoQueryVo implements Serializable {

    private static final long serialVersionUID = -6505409680331950216L;
    /**
     * SO单据号
     */
    private List<String> soBillCodes;
}
