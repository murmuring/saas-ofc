package com.lsh.ofc.provider.rest.service.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.alibaba.dubbo.rpc.protocol.rest.support.ContentType;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.lsh.base.common.exception.BusinessException;
import com.lsh.base.common.model.CommonResult;
import com.lsh.ofc.core.entity.OfcOrderHead;
import com.lsh.ofc.core.exception.EC;
import com.lsh.ofc.core.service.OfcStoCreateService;
import com.lsh.ofc.core.service.Order4PCService;
import com.lsh.ofc.provider.rest.service.Order4PCRestService;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 * Project Name: Order4PCRestServiceImpl
 * 北京链商电子商务有限公司
 * Auth: wangliutao
 * Date: 18/8/8
 * Package Name: com.lsh.ofc.provider.rest.service.impl
 * Description:
 */
@Service(protocol = "rest", validation = "true")
@Path("order")
@Consumes({MediaType.APPLICATION_JSON, MediaType.TEXT_XML})
@Produces({ContentType.APPLICATION_JSON_UTF_8, ContentType.TEXT_XML_UTF_8})
public class Order4PCRestServiceImpl implements Order4PCRestService {
    private static final Logger logger = LoggerFactory.getLogger(Order4PCRestServiceImpl.class);

    @Autowired
    private Order4PCService order4PCService;

    @POST
    @Path("pc/create")
    @Override
    public CommonResult<Boolean> createOrder(String content) throws BusinessException {
        logger.info("PC供给请求参数：{}", content);

        try {
            if (StringUtils.isEmpty(content)) {
                throw  EC.ERROR_PARAMS.exception("param is null, please check");
            }

            order4PCService.createOrder(content);
        } catch (BusinessException e) {
            logger.error("PC供给，业务异常", e);
            CommonResult<Boolean> result = new CommonResult<>(e.getCode(), e.getMessage());
            result.setData(false);
            return result;
        } catch (Exception e) {
            logger.error("PC供给，系统异常", e);
            return CommonResult.error("系统异常", false);
        }

        return CommonResult.success(true);
    }

}
