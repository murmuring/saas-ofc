package com.lsh.ofc.provider.rest.service;

import com.alibaba.fastjson.JSONArray;
import com.lsh.base.common.exception.BusinessException;
import com.lsh.base.common.model.CommonResult;
import com.lsh.ofc.core.entity.OfcObdDetail;
import com.lsh.ofc.provider.rest.dto.*;

import java.util.List;
import java.util.Map;

/**
 * OBD REST服务
 *
 * @author huangdong
 * @date 16/9/13
 */
public interface ObdRestService {

    /**
     * 推送OBD
     *
     * @param dto
     * @return
     * @throws BusinessException
     */
    CommonResult<Boolean> push(ObdHeadDTO dto) throws BusinessException;

    /**
     * 推送mis OBD
     *
     * @param dto
     * @return
     * @throws BusinessException
     */
    CommonResult<Boolean> preparePush(PreObdHeadDTO dto) throws BusinessException;

    /**
     * 推送mis OBD
     *
     * @param dto
     * @return
     * @throws BusinessException
     */
    CommonResult<JSONArray> preparePushList(PreObdHeadsDTO dto) throws BusinessException;

    /**
     * 推送OBD ConfirmObdHeadDTO
     *
     * @param dto ObdBoxDTO
     * @return
     * @throws BusinessException
     */
    CommonResult<Boolean> confirm(ConfirmObdHeadDTO dto) throws BusinessException;

    /**
     * 推送OBD ObdBoxDTO
     *
     * @param dto ObdBoxDTO
     * @return
     * @throws BusinessException
     */
    CommonResult<Boolean> obdBox(ObdBoxDTO dto) throws BusinessException;

    /**
     * 根据SO单据号查询OBD明细列表
     *
     * @param soBillCode
     * @return
     * @throws BusinessException
     */
    CommonResult<List<OfcObdDetail>> details(String soBillCode) throws BusinessException;

    /**
     * 根据SO单据号
     *
     * @param soBillCode
     * @return
     * @throws BusinessException
     */
    CommonResult<ObdHeadVo> obdHeadInfo(String soBillCode) throws BusinessException;

    /**
     * 批量查询OBD明细列表
     *
     * @param content
     * @return
     * @throws BusinessException
     */
    CommonResult<List<ObdHeadVo>> obdHeadListInfo(String content) throws BusinessException;

    /**
     * 根据订单号，查询包裹码
     *
     * @param content
     * @return
     * @throws BusinessException
     */
    CommonResult obdPackageInfos(String content) throws BusinessException;

    /**
     * 根据SO单据号，从redis中查询下发分拣中心的参数和分拣中心回传的参数
     *
     * @param soBillCode
     * @return
     * @throws BusinessException
     */
    CommonResult queryObdBoxParam(String soBillCode) throws BusinessException;
}
