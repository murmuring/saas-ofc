package com.lsh.ofc.provider.rest.dto;

import com.lsh.ofc.api.validation.ValidationMessage;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

/**
 * @author peter
 */
@Setter
@Getter
@NoArgsConstructor
@ToString
public class ConfirmObdHeadDTO implements Serializable {

    private static final long serialVersionUID = 8408982792283114220L;
    /**
     * SO单据号
     */
    private String soBillCode;

    /**
     * 订单号
     */
    private Long orderCode;

    /**
     * 预估箱数
     */
    @Min(value = 0, message = ValidationMessage.ERROR)
    @NotNull(message = ValidationMessage.NOT_NULL)
    private BigDecimal scatteredBoxNum;

    /**
     * 扫描包裹唯一标识
     */
    private List<String> scanPackageCodes;

    /**
     * 集货道
     */
    private String collectionLocation;

    /**
     * 运单号
     */
    private String waybillCode;

    /**
     * OBD单据明细列表
     */
    private List<ConfirmObdDetailDTO> details;
}
