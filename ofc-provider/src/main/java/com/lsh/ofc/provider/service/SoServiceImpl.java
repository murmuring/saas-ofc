package com.lsh.ofc.provider.service;

import com.alibaba.dubbo.config.annotation.Service;
import com.lsh.base.common.exception.BusinessException;
import com.lsh.ofc.api.dto.SoHeadDTO;
import com.lsh.ofc.api.service.SoService;
import com.lsh.ofc.core.entity.OfcSoHead;
import com.lsh.ofc.core.service.OfcSoService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Project Name: lsh-ofc
 *
 * @author peter
 * @date 18/5/17
 * 北京链商电子商务有限公司
 * Package
 * desc:
 */
@Service(protocol = "dubbo", validation = "true", timeout = 10000)
@Slf4j
public class SoServiceImpl implements SoService {

    @Autowired
    private OfcSoService ofcSoService;

    /**
     * 查询SO单信息
     * @param orderCode
     * @return
     * @throws BusinessException
     */
    @Override
    public List<SoHeadDTO> querySignOrders(Long orderCode) throws BusinessException {
        List<SoHeadDTO> soHeadDTOList = new ArrayList<>();
        try {
            log.info("[rpc so query] orderCode " + orderCode);

            if (orderCode == null) {
                return Collections.emptyList();
            }

            OfcSoHead filter = new OfcSoHead();
            filter.setOrderCode(orderCode);
            List<OfcSoHead> list = this.ofcSoService.findList(filter, false);
            if (list == null || list.isEmpty()) {
                return Collections.emptyList();
            }

            SoHeadDTO soHeadDTO;
            for (OfcSoHead ofcSoHead : list) {
                soHeadDTO = new SoHeadDTO();
                BeanUtils.copyProperties(ofcSoHead,soHeadDTO);
                soHeadDTOList.add(soHeadDTO);
            }

        } catch (Exception e) {
            log.error("查询so异常",e);
        }

        return soHeadDTOList;
    }

    /**
     * 查询SO单信息
     *
     * @param soBillCode
     * @return
     * @throws BusinessException
     */
    @Override
    public SoHeadDTO querySoHeadInfo(String soBillCode) throws BusinessException {
        SoHeadDTO soHead = new SoHeadDTO();
        try {
            log.info("[rpc so query] soBillCode " + soBillCode);

            if (StringUtils.isBlank(soBillCode)) {
                return soHead;
            }

            OfcSoHead filter = new OfcSoHead();
            filter.setSoBillCode(soBillCode);
            List<OfcSoHead> list = this.ofcSoService.findList(filter, false);
            if (list == null || list.isEmpty()) {
                return soHead;
            }
            OfcSoHead ofcSoHead = list.get(0);

            BeanUtils.copyProperties(ofcSoHead,soHead);

        } catch (Exception e) {
            log.error("查询so异常",e);
        }

        return soHead;
    }
}
