package com.lsh.ofc.provider.rest.service;

import com.lsh.base.common.model.CommonResult;
import com.lsh.ofc.core.entity.OfcSoHead;
import com.lsh.ofc.provider.common.util.Pageable;

import javax.ws.rs.core.UriInfo;

public interface SaleOrderRestService {

    CommonResult<Pageable<OfcSoHead>> query(UriInfo uriInfo,
                                            long page,
                                            long limit);
}
