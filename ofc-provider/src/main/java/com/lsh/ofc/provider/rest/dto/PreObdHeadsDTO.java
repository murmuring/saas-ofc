package com.lsh.ofc.provider.rest.dto;

import com.lsh.ofc.api.validation.ValidationMessage;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.Valid;
import java.io.Serializable;
import java.util.List;

/**
 * mis OBD头DTO
 *
 * @author miaozhuang
 * @date 18/08/24
 */
@Setter
@Getter
@NoArgsConstructor
@ToString
public class PreObdHeadsDTO implements Serializable {

    private static final long serialVersionUID = -367589046377634659L;

    /**
     * 批量发货唯一标识
     */
    @NotBlank(message = ValidationMessage.NOT_BLANK)
    private String batchObdCode;
    /**
     * 汇总信息
     */
    private List<BatchObdDetailDTO> batchObdDetailList;
    /**
     * 订单发货详情
     */
    @Valid
    @NotEmpty(message = ValidationMessage.NOT_EMPTY)
    private List<PreObdHeadDTO> obdHeadList;
}
