package com.lsh.ofc.provider.rest.service.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.alibaba.dubbo.rpc.protocol.rest.support.ContentType;
import com.alibaba.fastjson.JSON;
import com.lsh.base.common.exception.BusinessException;
import com.lsh.base.common.model.CommonResult;
import com.lsh.ofc.core.entity.OfcSoDetail;
import com.lsh.ofc.core.entity.OfcSoHead;
import com.lsh.ofc.core.service.OfcSoService;
import com.lsh.ofc.provider.rest.dto.SoQueryVo;
import com.lsh.ofc.provider.rest.service.SoRestService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author peter
 */
@Service(protocol = "rest", validation = "true")
@Path("/so")
@Consumes({MediaType.APPLICATION_JSON, MediaType.TEXT_XML})
@Produces({ContentType.APPLICATION_JSON_UTF_8, ContentType.TEXT_XML_UTF_8})
public class SoRestServiceImpl implements SoRestService {

    private static Logger logger = LoggerFactory.getLogger(SoRestServiceImpl.class);

    @Autowired
    private OfcSoService ofcSoService;

    @GET
    @Path("/query")
    @Override
    public CommonResult<List<OfcSoHead>> query(@QueryParam("orderCode") Long orderCode) throws BusinessException {

        logger.info("[so query] orderCode " + orderCode);
        List<OfcSoHead> list;
        if (orderCode == null) {
            list = Collections.emptyList();
        } else {
            OfcSoHead filter = new OfcSoHead();
            filter.setOrderCode(orderCode);
            list = this.ofcSoService.findList(filter, true);
        }
        return CommonResult.success(list);
    }

    @GET
    @Path("/details")
    @Override
    public CommonResult<List<OfcSoDetail>> details(@QueryParam("soBillCode") String soBillCode) throws BusinessException {
        return CommonResult.success(this.ofcSoService.findDtails(soBillCode));
    }

    /**
     * 查询SO单信息
     *
     * @param soQueryVo
     * @return
     * @throws BusinessException
     */
    @POST
    @Path("/querySo")
    @Override
    public CommonResult<List<OfcSoHead>> querySo(SoQueryVo soQueryVo) throws BusinessException {
        logger.info("[so query] soQueryVo " + JSON.toJSONString(soQueryVo));
        List<OfcSoHead> list = new ArrayList<>();
        if (soQueryVo == null || CollectionUtils.isEmpty(soQueryVo.getSoBillCodes())) {
            return CommonResult.success(list);
        }

        for (String soBillCode : soQueryVo.getSoBillCodes()) {
            OfcSoHead filter = new OfcSoHead();
            filter.setSoBillCode(soBillCode);
            OfcSoHead soHead = this.ofcSoService.findOne(filter, false);

            list.add(soHead);
        }

        return CommonResult.success(list);
    }
}
