package com.lsh.ofc.provider.rest.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

/**
 * OBD头DTO
 *
 * @author huangdong
 * @date 16/9/5
 */
@Setter
@Getter
@NoArgsConstructor
public class ObdHeadVo implements Serializable {

    private static final long serialVersionUID = -6505409680331950216L;
    /**
     * ID
     */
    private Long id;

    /**
     * SO单据号
     */
    private String soBillCode;

    /**
     * 订单号
     */
    private Long orderCode;

    /**
     * 地域编号
     */
    private Integer regionCode;

    /**
     * 仓库编号
     */
    private String warehouseCode;

    /**
     * 仓库名称
     */
    private String warehouseName;

    /**
     * 供货商ID
     */
    private Integer supplierId;

    /**
     * 供货商DC
     */
    private String supplierDc;

    /**
     * 供货商机构
     */
    private Integer supplierOrg;

    /**
     * 履约WMS
     */
    private Integer fulfillWms;

    /**
     * 履约渠道
     */
    private Integer fulfillChannel;

    /**
     * 履约SO单号
     */
    private String soCode;

    /**
     * OBD单号
     */
    private String obdCode;

    /**
     * SKU下单总数
     */
    private BigDecimal totalSkuOrderQty;

    /**
     * SKU供货总数
     */
    private BigDecimal totalSkuSupplyQty;

    /**
     * SKU发货总数
     */
    private BigDecimal totalSkuDeliverQty;

    /**
     * SKU退货总数
     */
    private BigDecimal totalSkuReturnQty;

    /**
     * 成本金额
     */
    private BigDecimal costAmount;

    /**
     * 未税成本金额
     */
    private BigDecimal costNtAmount;

    /**
     * 其它信息
     */
    private String ext;

    /**
     * 创建时间
     */
    private Integer createTime;

    /**
     * 更新时间
     */
    private Integer updateTime;

    /**
     * 是否有效（0:无效；1:有效）
     */
    private Integer valid;

    /**
     * 预发货包裹数
     */
    private Integer prePackageNum;

    /**
     * 分拣中心拣货包裹数
     */
    private Integer scanPackageNum;

    /**
     * OBD单据明细列表
     */
    private List<ObdDetailVo> details;
}
