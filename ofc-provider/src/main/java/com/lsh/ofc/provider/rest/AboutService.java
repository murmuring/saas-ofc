package com.lsh.ofc.provider.rest;

import java.util.Map;

public interface AboutService {

    Map<String, Object> about();
}
