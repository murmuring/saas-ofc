package com.lsh.ofc.provider.common.util;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Getter;

import java.io.Serializable;
import java.util.List;

@Getter
public class Pageable<T> implements Serializable {

    private static final long serialVersionUID = 1596448441568743910L;

    private List<T> entries;

    private Long page;

    @JSONField(name = "total_page")
    private Long totalPage;

    @JSONField(name = "total_count")
    private Long totalCount;

    public Pageable(List<T> entries, Long page) {
        this.entries = entries;
        this.page = page;
        // TODO: 2019-12-11
        this.totalPage = 1L;
        this.totalCount = (long) entries.size();
    }
}
