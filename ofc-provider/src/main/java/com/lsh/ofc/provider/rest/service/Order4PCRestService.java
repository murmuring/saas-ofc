package com.lsh.ofc.provider.rest.service;

import com.lsh.base.common.model.CommonResult;

public interface Order4PCRestService {
    CommonResult<Boolean> createOrder(String content);
}
