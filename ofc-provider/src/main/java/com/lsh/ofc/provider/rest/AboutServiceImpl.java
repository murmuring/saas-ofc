package com.lsh.ofc.provider.rest;

import com.alibaba.dubbo.config.annotation.Service;
import com.google.common.collect.ImmutableMap;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.Map;

@Path("about")
@Service(protocol = "rest", timeout = 30000)
public class AboutServiceImpl implements AboutService {

    @GET
    @Produces({MediaType.APPLICATION_JSON})
    @Override
    public Map<String, Object> about() {
        return ImmutableMap.<String, Object>builder()
                .put("name", "ofc-provider")
                .build();
    }
}
