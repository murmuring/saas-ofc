package com.lsh.ofc.provider.rest.dto;

import com.alibaba.fastjson.annotation.JSONField;
import com.lsh.ofc.api.validation.ValidationMessage;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * mis OBD头DTO
 *
 * @author miaozhuang
 * @date 18/08/24
 */
@Setter
@Getter
@NoArgsConstructor
@ToString
public class PreObdDetailDTO implements Serializable {

    private static final long serialVersionUID = -3380475657461957950L;
    /**
     * 供货SKU码
     */
    @NotBlank(message = ValidationMessage.NOT_BLANK)
    private String supplySkuCode;
    /**
     * 国条码
     */
//    @NotBlank(message = ValidationMessage.NOT_BLANK)
    private String barcode;
    /**
     * 销售码
     */
    @NotNull(message = ValidationMessage.NOT_BLANK)
    @JSONField(name = "skuId")
    private Long goodsCode;
    /**
     * SKU数量 EA数
     */
    @NotNull(message = ValidationMessage.NOT_NULL)
    @DecimalMin(value = "0", message = ValidationMessage.ERROR)
    private BigDecimal skuQty;
    /**
     * 销售名称  商品名称
     */
    @NotBlank(message = ValidationMessage.NOT_BLANK)
    private String skuName;
    /**
     * 仓库作业模式
     */
//    @NotNull(message = ValidationMessage.NOT_BLANK)
    private Integer taskModel;
    /**
     * 售卖单位
     */
    private String skuDefine;
    /**
     *
     */
//    @NotNull(message = ValidationMessage.NOT_NULL)
//    @DecimalMin(value = "0", message = ValidationMessage.ERROR)
    private BigDecimal saleUnit;
}
