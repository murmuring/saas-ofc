package com.lsh.ofc.provider.rest.service;

import com.lsh.base.common.exception.BusinessException;
import com.lsh.base.common.model.CommonResult;
import com.lsh.ofc.api.dto.OrderHeadShipDTO;
import com.lsh.ofc.core.entity.OfcObdHead;

/**
 * OBD REST服务
 *
 * @author huangdong
 * @date 16/9/13
 */
public interface OrderObdRestService {

    /**
     * 推送OBD
     *
     * @param orderHeadShipDTO
     * @return
     * @throws BusinessException
     */
    CommonResult<Boolean> orderPush(OrderHeadShipDTO orderHeadShipDTO) throws BusinessException;


    /**
     * 推送OBD
     *
     * @param obd
     * @return
     * @throws BusinessException
     */
    CommonResult<Boolean> wumartObdPush(OfcObdHead obd) throws BusinessException;

}
