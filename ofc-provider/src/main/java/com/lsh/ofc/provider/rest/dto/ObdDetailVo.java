package com.lsh.ofc.provider.rest.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * OBD明细
 *
 * @author huangdong
 * @date 16/9/5
 */
@Setter
@Getter
@NoArgsConstructor
public class ObdDetailVo implements Serializable {

    private static final long serialVersionUID = -3380475657461957950L;

    /**
     * ID
     */
    private Long id;

    private Long itemNo;
    /**
     * SO单据号
     */
    private String soBillCode;

    /**
     * SKU编号
     */
    private Long skuCode;

    /**
     * SKU编号
     */
    private String skuName;

    /**
     * SKU供货码
     */
    private String skuSupplyCode;

    /**
     * SKU供货价
     */
    private BigDecimal skuSupplyPrice;

    /**
     * SKU发货总数
     */
    private BigDecimal skuDeliverQty;

    /**
     * SKU退货总数
     */
    private BigDecimal skuReturnQty;

    /**
     * 税率
     */
    private BigDecimal taxRate;
    /**
     * 仓库作业模式
     */
    private Integer taskModel;

    /**
     * 其它信息
     */
    private String ext;
}
