package com.lsh.ofc.provider.rest.dto;

import com.lsh.ofc.api.validation.ValidationMessage;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

/**
 * OBD头DTO
 *
 * @author huangdong
 * @date 16/9/5
 */
@Setter
@Getter
@NoArgsConstructor
@ToString
public class ObdHeadDTO implements Serializable {

    private static final long serialVersionUID = -367589046322334659L;

    /**
     * WMS
     */
    @Min(value = 1, message = ValidationMessage.ERROR)
    @NotNull(message = ValidationMessage.NOT_NULL)
    private Integer wms;

    /**
     * 仓库编号
     */
    @NotBlank(message = ValidationMessage.NOT_BLANK)
    private String warehouseCode;

    private String preWarehouseCode;

    /**
     * SO单号
     */
    @NotBlank(message = ValidationMessage.NOT_BLANK)
    private String soCode;

    /**
     * OBD单号
     */
    @NotBlank(message = ValidationMessage.NOT_BLANK)
    private String obdCode;

    /**
     * 运单号
     */
    @NotBlank(message = ValidationMessage.NOT_BLANK)
    private String waybillCode;

    /**
     * 箱数
     */
    @Min(value = 0, message = ValidationMessage.ERROR)
    @NotNull(message = ValidationMessage.NOT_NULL)
    private Integer boxNum;

    /**
     * 周转箱数
     */
    @Min(value = 0, message = ValidationMessage.ERROR)
    @NotNull(message = ValidationMessage.NOT_NULL)
    private Integer turnoverBoxNum;

    /**
     * 散箱数
     */
    @Min(value = 0, message = ValidationMessage.ERROR)
    @NotNull(message = ValidationMessage.NOT_NULL)
    private Integer scatteredBoxNum;

    /**
     * 包裹唯一标识（云仓订单使用）
     */
    private List<String> packageCodes;

    /**
     * 司机信息
     */
    private String driverInfo;

    /**
     * 车辆类型
     */
    private String vehicleType;

    /**
     * 车辆类型描述
     */
    private String vehicleTypeDesc;

    /**
     * 承运商标号
     */
    private String carrierCode;

    /**
     * 承运商名称
     */
    private String carrierName;

    /**
     * 创建时间
     */
    private String createTime;

    /**
     * 拣货时间
     */
    private String pickTime;

    /**
     * 发货时间
     */
    private String deliveryTime;

    /**
     * 发货信息
     */
    private String obdOrderExt;

    /**
     * mis 调用标志
     */
    private Integer mis;

    private String misExt;

    /**
     * obd 体积 单位（立方厘米）
     */
    private BigDecimal totalVolume;

    private String collectCode;

    /**
     * OBD明细
     */
    @Valid
    @NotEmpty(message = ValidationMessage.NOT_EMPTY)
    private List<ObdDetailDTO> details;
}
