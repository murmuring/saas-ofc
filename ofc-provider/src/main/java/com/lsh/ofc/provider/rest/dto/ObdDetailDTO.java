package com.lsh.ofc.provider.rest.dto;

import com.lsh.ofc.api.validation.ValidationMessage;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * OBD明细
 *
 * @author huangdong
 * @date 16/9/5
 */
@Setter
@Getter
@NoArgsConstructor
@ToString
public class ObdDetailDTO implements Serializable {

    private static final long serialVersionUID = -3380475657461957950L;

    private String itemNo;
    /**
     * 供货SKU码
     */
    @NotBlank(message = ValidationMessage.NOT_BLANK)
    private String supplySkuCode;

    /**
     * SKU数量 （EA数）
     */
    @NotNull(message = ValidationMessage.NOT_NULL)
    @DecimalMin(value = "0", message = ValidationMessage.ERROR)
    private BigDecimal skuQty;

    @NotNull(message = ValidationMessage.NOT_NULL)
    @Min(value = 0, message = ValidationMessage.ERROR)
    private Integer packNum;

    /**
     * 箱数
     */
    private Integer boxNum;

    /**
     * LeftEA数
     */
    private Integer leftEaNum;

    private String skuDefine;

    private String obdDetailExt;

    private String barcode;

    private String packCode;

    private String skuName;

    private BigDecimal weight;

    private Integer taskModel;

    private Long goodsCode;

    private String preInfo;
}
