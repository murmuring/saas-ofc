package com.lsh.ofc.provider.rest.service;

import com.lsh.base.common.exception.BusinessException;
import com.lsh.base.common.model.CommonResult;
import com.lsh.ofc.api.dto.KafkaConsumerRecordDTO;

/**
 * Project Name: lsh-ofc
 *
 * @author peter
 * @date 19/3/29
 * 北京链商电子商务有限公司
 * Package
 * desc: so汇总
 */
public interface CollectOrderSoObdRestService {

    /**
     * 创建返仓RO
     *
     * @param recordDTO
     * @return
     * @throws BusinessException
     */
    CommonResult<Boolean> createOrderSoObd(KafkaConsumerRecordDTO recordDTO) throws BusinessException;

}
