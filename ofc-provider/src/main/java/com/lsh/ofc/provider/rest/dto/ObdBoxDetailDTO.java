package com.lsh.ofc.provider.rest.dto;

import com.lsh.ofc.api.validation.ValidationMessage;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.validator.constraints.NotBlank;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author peter
 */
@Setter
@Getter
@NoArgsConstructor
@ToString
public class ObdBoxDetailDTO implements Serializable {
    private static final long serialVersionUID = -2907477091656868040L;

    /**
     * 销售码
     */
    @NotBlank(message = ValidationMessage.NOT_BLANK)
    private String scanPackageCode;

    /**
     * 数量
     */
    @NotBlank(message = ValidationMessage.NOT_BLANK)
    private BigDecimal qty;
}
