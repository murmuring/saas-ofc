package com.lsh.ofc.provider.rest.service.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.alibaba.dubbo.rpc.protocol.rest.support.ContentType;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.lsh.base.common.exception.BusinessException;
import com.lsh.base.common.model.CommonResult;
import com.lsh.ofc.api.dto.RealDCQueryDTO;
import com.lsh.ofc.api.dto.ReturnHeadDTO;
import com.lsh.ofc.api.dto.RoQueryDTO;
import com.lsh.ofc.api.dto.WumartReturnHeadDTO;
import com.lsh.ofc.core.entity.OfcRoHead;
import com.lsh.ofc.core.enums.BillType;
import com.lsh.ofc.core.service.OfcRoService;
import com.lsh.ofc.provider.rest.BaseService;
import com.lsh.ofc.provider.rest.service.ReturnRestService;
import com.lsh.ofc.provider.service.ReturnRpcServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.ArrayList;
import java.util.List;

/**
 * @author peter
 */
@Service(protocol = "rest", validation = "true")
@Path("return")
@Consumes({MediaType.APPLICATION_JSON, MediaType.TEXT_XML})
@Produces({ContentType.APPLICATION_JSON_UTF_8, ContentType.TEXT_XML_UTF_8})
@Slf4j
public class ReturnRestServiceImpl extends BaseService implements ReturnRestService {

    @Autowired
    private ReturnRpcServiceImpl returnRpcService;

    @Autowired
    private OfcRoService ofcRoService;

    @POST
    @Path("/ro/create")
    @Override
    public CommonResult<List<String>> createRo(ReturnHeadDTO dto) throws BusinessException {

        Long venderId = this.getVenderId();
        if (venderId == null) {
            CommonResult.error("venderId 不存在");
        }
        dto.setVenderId(venderId);
        log.info("ofc ro create venderId is {}", venderId);

        return returnRpcService.createRo(dto);
    }

    @GET
    @Path("/ro/status/query")
    @Override
    public CommonResult<Object> queryRoStatus(@QueryParam("returnCode") Long returnCode) throws BusinessException {

        return returnRpcService.queryRoStatus(returnCode);
    }

    /**
     * 物美返仓的创建
     *
     * @param dto
     * @return
     * @throws BusinessException
     */
    @POST
    @Path("/ro/wm/create")
    @Override
    public CommonResult<List<String>> createWumartRo(WumartReturnHeadDTO dto) throws BusinessException {

        return returnRpcService.createWumartRo(dto);
    }

    /**
     * 查询返仓状态
     *
     * @param realDCQueryDTO
     * @return
     * @throws BusinessException
     */
    @POST
    @Path("/ro/dc/query")
    @Override
    public CommonResult<JSONArray> queryRo(RealDCQueryDTO realDCQueryDTO) throws BusinessException {
        JSONArray data = new JSONArray();

        List<String> billCodes = realDCQueryDTO.getBillCodes();

        if (StringUtils.isEmpty(realDCQueryDTO.getBillType()) || !BillType.compareValue(realDCQueryDTO.getBillType())) {
            return CommonResult.error("billType 参数不能为空，切必须为RO或SO", data);
        }

        if (CollectionUtils.isEmpty(realDCQueryDTO.getBillCodes())) {
            return CommonResult.error("billCodes 参数不能为空", data);
        }

        for (String billCode : billCodes) {
            String realSupplierDc;
            if (realDCQueryDTO.getBillType().equals(BillType.RO.name())) {
                realSupplierDc = returnRpcService.querySoDcFromRo(billCode);
            } else {
                realSupplierDc = returnRpcService.querySoDcFromSo(billCode);
            }

            JSONObject subData = new JSONObject();
            subData.put("billCode", billCode);
            subData.put("supplierDc", realSupplierDc);

            data.add(subData);
        }

        return CommonResult.success(data);
    }

    /**
     * 查询返
     *
     * @param roQueryDTO
     * @return
     * @throws BusinessException
     */
    @POST
    @Path("/rso/query")
    @Override
    public CommonResult<List<OfcRoHead>> queryRoInfo(RoQueryDTO roQueryDTO) throws BusinessException {
        List<String> roBillCodes = roQueryDTO.getRoBillCodes();
        List<OfcRoHead> data = new ArrayList<>();
        if (CollectionUtils.isEmpty(roBillCodes)) {
            return CommonResult.error("billCodes 参数不能为空", data);
        }
        OfcRoHead roHead;
        for (String billCode : roBillCodes) {
            OfcRoHead filter = new OfcRoHead();
            filter.setRoBillCode(billCode);
            roHead = ofcRoService.findOne(filter,true);

            data.add(roHead);
        }

        return CommonResult.success(data);
    }
}
