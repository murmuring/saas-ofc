package com.lsh.ofc.provider.rest.dto;

import com.lsh.ofc.api.validation.ValidationMessage;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.Valid;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

/**
 * mis OBD头DTO
 *
 * @author miaozhuang
 * @date 18/08/24
 */
@Setter
@Getter
@NoArgsConstructor
@ToString
public class PreObdHeadDTO implements Serializable {

    private static final long serialVersionUID = -367589046322334659L;

    /**
     * 订单号
     */
    @NotBlank(message = ValidationMessage.NOT_BLANK)
    private String orderCode;

    /**
     * mis 发货唯一标识
     */
    @NotBlank(message = ValidationMessage.NOT_BLANK)
    private String misOrderCode;
    /**
     * 发货时间
     */
    private String deliveryTime;
    /**
     * 包裹数
     */
    @NotNull(message = ValidationMessage.NOT_NULL)
    @DecimalMin(value = "0", message = ValidationMessage.ERROR)
    private BigDecimal packageNum;

    /**
     * OBD明细
     */
    @Valid
    @NotEmpty(message = ValidationMessage.NOT_EMPTY)
    private List<PreObdDetailDTO> details;
}
