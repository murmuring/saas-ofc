package com.lsh.ofc.provider.rest;

import com.alibaba.dubbo.rpc.RpcContext;
import com.lsh.base.common.filter.TraceContext;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;

import javax.servlet.http.HttpServletRequest;

/**
 * @author peter
 */
@Slf4j
public class BaseService {

    /**
     * 获取request数据
     * @param
     * @return
     */
    public Long getVenderId(){

        String venderId = RpcContext.getContext().getAttachment("venderId");
        if (StringUtils.isBlank(venderId)) {

            HttpServletRequest request = (HttpServletRequest) RpcContext.getContext().getRequest();
            if(request != null){
                venderId = request.getHeader(TraceContext.VENDER_ID);
            }
            if (StringUtils.isBlank(venderId)) {
                return 0L;
            }
        }

        log.info("baseService venderId is " + venderId);

        return Long.parseLong(venderId);
    }



}
