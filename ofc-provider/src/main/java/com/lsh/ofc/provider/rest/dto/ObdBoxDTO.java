package com.lsh.ofc.provider.rest.dto;

import com.lsh.ofc.api.validation.ValidationMessage;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.validator.constraints.NotBlank;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * @author peter
 */
@Setter
@Getter
@NoArgsConstructor
@ToString
public class ObdBoxDTO implements Serializable {

    private static final long serialVersionUID = 8408982792283114220L;

    /**
     * SO单号
     */
    @NotBlank(message = ValidationMessage.NOT_BLANK)
    private String soCode;

    /**
     * 扫描包裹唯一标识
     */
//    private List<String> scanPackageCodes;

    /**
     * 集货道
     */
    private String collectionLocation;

    /**
     * 运单号
     */
    @NotBlank(message = ValidationMessage.NOT_BLANK)
    private String waybillCode;

    /**
     * 提总商品发货信息
     */
    private List<ObdBoxDetailDTO> details;

    //    /**
//     * SO单据号
//     */
//    private String soBillCode;
}
