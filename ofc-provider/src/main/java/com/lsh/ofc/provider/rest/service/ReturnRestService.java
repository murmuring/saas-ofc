package com.lsh.ofc.provider.rest.service;

import com.alibaba.fastjson.JSONArray;
import com.lsh.base.common.exception.BusinessException;
import com.lsh.base.common.model.CommonResult;
import com.lsh.ofc.api.dto.RealDCQueryDTO;
import com.lsh.ofc.api.dto.ReturnHeadDTO;
import com.lsh.ofc.api.dto.RoQueryDTO;
import com.lsh.ofc.api.dto.WumartReturnHeadDTO;
import com.lsh.ofc.core.entity.OfcRoHead;

import java.util.List;

/**
 * 返仓单REST服务
 *
 * @author huangdong
 * @date 16/8/28
 */

public interface ReturnRestService {

    /**
     * 创建返仓RO
     *
     * @param dto
     * @return
     * @throws BusinessException
     */
    CommonResult<List<String>> createRo(ReturnHeadDTO dto) throws BusinessException;

    /**
     * 查询返仓状态
     *
     * @param returnCode
     * @return
     * @throws BusinessException
     */
    CommonResult<Object> queryRoStatus(Long returnCode) throws BusinessException;

    /**
     * 物美返仓的创建
     *
     * @param dto
     * @return
     * @throws BusinessException
     */
    CommonResult<List<String>> createWumartRo(WumartReturnHeadDTO dto) throws BusinessException;

    /**
     * 查询返
     *
     * @param realDCQueryDTO
     * @return
     * @throws BusinessException
     */
    CommonResult<JSONArray> queryRo(RealDCQueryDTO realDCQueryDTO) throws BusinessException;


    /**
     * 查询返
     *
     * @param roQueryDTO
     * @return
     * @throws BusinessException
     */
    CommonResult<List<OfcRoHead>> queryRoInfo(RoQueryDTO roQueryDTO) throws BusinessException;
}
